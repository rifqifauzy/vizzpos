-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2018 at 02:38 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vizzpos`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_company`
--

CREATE TABLE IF NOT EXISTS `m_company` (
  `m_company_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedUser` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `event` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `npwp` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `postalcode` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`m_company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `m_company`
--

INSERT INTO `m_company` (`m_company_id`, `created`, `createdUser`, `updated`, `updatedUser`, `address`, `city`, `email`, `event`, `facebook`, `fax`, `instagram`, `logo`, `name`, `npwp`, `phone`, `postalcode`, `website`) VALUES
(1, '2018-05-12 11:39:54', 'rifqi', NULL, NULL, 'Jakarta Fair', 'Jakarta', 'cs@vizz.co.id', '', 'vizzcomindonesia', '', 'vizz_official', NULL, 'VIZZ INDONESIA', '', '12345', '', 'www.vizz.co.id');

-- --------------------------------------------------------

--
-- Table structure for table `m_configuration`
--

CREATE TABLE IF NOT EXISTS `m_configuration` (
  `m_configuration_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedUser` varchar(255) DEFAULT NULL,
  `directprint` bit(1) DEFAULT NULL,
  `prefix` varchar(5) DEFAULT NULL,
  `sequence` int(11) NOT NULL,
  `m_locator_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`m_configuration_id`),
  KEY `FK_9e99ihbhnnblf9af9s21949e8` (`m_locator_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `m_configuration`
--

INSERT INTO `m_configuration` (`m_configuration_id`, `created`, `createdUser`, `updated`, `updatedUser`, `directprint`, `prefix`, `sequence`, `m_locator_id`) VALUES
(1, '2018-05-15 21:53:46', 'rifqi', NULL, NULL, b'1', 'PRJ', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_edc`
--

CREATE TABLE IF NOT EXISTS `m_edc` (
  `m_edc_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedUser` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `minamount` double NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`m_edc_id`),
  UNIQUE KEY `idx_name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `m_edc`
--

INSERT INTO `m_edc` (`m_edc_id`, `created`, `createdUser`, `updated`, `updatedUser`, `active`, `code`, `description`, `minamount`, `name`) VALUES
(1, '2018-05-15 21:53:03', 'rifqi', NULL, NULL, b'1', NULL, '', 20000, 'BCA');

-- --------------------------------------------------------

--
-- Table structure for table `m_locator`
--

CREATE TABLE IF NOT EXISTS `m_locator` (
  `m_locator_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedUser` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `code` varchar(7) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`m_locator_id`),
  UNIQUE KEY `idx_code` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `m_locator`
--

INSERT INTO `m_locator` (`m_locator_id`, `created`, `createdUser`, `updated`, `updatedUser`, `active`, `address`, `code`, `description`, `name`) VALUES
(1, '2018-05-15 21:53:27', 'rifqi', NULL, NULL, b'1', '', 'PRJ', '', 'Jakarta Fair');

-- --------------------------------------------------------

--
-- Table structure for table `m_menu`
--

CREATE TABLE IF NOT EXISTS `m_menu` (
  `m_menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedUser` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `code` varchar(255) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `menubar` bit(1) NOT NULL,
  `menuitem` bit(1) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parentCode` varchar(255) DEFAULT NULL,
  `zulpath` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`m_menu_id`),
  KEY `idx_name` (`name`),
  KEY `idx_code` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `m_menu`
--

INSERT INTO `m_menu` (`m_menu_id`, `created`, `createdUser`, `updated`, `updatedUser`, `active`, `code`, `icon`, `level`, `menubar`, `menuitem`, `name`, `parentCode`, `zulpath`) VALUES
(1, '2018-05-12 11:34:59', '', NULL, NULL, b'1', '01', NULL, 0, b'1', b'0', 'Master', NULL, NULL),
(2, '2018-05-12 11:34:59', '', NULL, NULL, b'1', '0101', NULL, 1, b'1', b'1', 'Satuan', '01', 'pages/master/unit.zul'),
(3, '2018-05-12 11:34:59', '', NULL, NULL, b'1', '0102', NULL, 1, b'1', b'1', 'Kategori Produk', '01', 'pages/master/productCategory.zul'),
(4, '2018-05-12 11:35:00', '', NULL, NULL, b'1', '0103', NULL, 1, b'1', b'1', 'Produk', '01', 'pages/master/product.zul'),
(5, '2018-05-12 11:35:00', '', NULL, NULL, b'1', '0104', NULL, 1, b'1', b'1', 'Locator', '01', 'pages/master/locator.zul'),
(6, '2018-05-12 11:35:00', '', NULL, NULL, b'1', '0105', NULL, 1, b'1', b'1', 'EDC', '01', 'pages/master/edc.zul'),
(7, '2018-05-12 11:35:00', '', NULL, NULL, b'1', '0106', NULL, 1, b'1', b'1', 'Promo', '01', 'pages/master/promo.zul'),
(8, '2018-05-12 11:35:00', '', NULL, NULL, b'1', '0107', NULL, 1, b'1', b'1', 'Atur Promo', '01', 'pages/master/promoManage.zul'),
(9, '2018-05-12 11:35:00', '', NULL, NULL, b'1', '02', NULL, 0, b'1', b'0', 'Utilitas', NULL, NULL),
(10, '2018-05-12 11:35:00', '', NULL, NULL, b'1', '0201', NULL, 1, b'1', b'1', 'Konfigurasi', '02', 'pages/utility/configuration.zul'),
(11, '2018-05-12 11:35:00', '', NULL, NULL, b'1', '03', NULL, 0, b'1', b'0', 'Import', NULL, NULL),
(12, '2018-05-12 11:35:00', '', NULL, NULL, b'1', '0301', NULL, 1, b'1', b'1', 'Product', '03', 'pages/imports/importProduct.zul');

-- --------------------------------------------------------

--
-- Table structure for table `m_order`
--

CREATE TABLE IF NOT EXISTS `m_order` (
  `m_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedUser` varchar(255) DEFAULT NULL,
  `changes` double DEFAULT NULL,
  `dateordered` datetime DEFAULT NULL,
  `discountline` double DEFAULT NULL,
  `documentno` varchar(40) NOT NULL,
  `grandtotal` double DEFAULT NULL,
  `hold` bit(1) NOT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `paid` bit(1) NOT NULL,
  `specialdiscount` double DEFAULT NULL,
  `subtotal` double DEFAULT NULL,
  `totalpayment` double DEFAULT NULL,
  `voided` bit(1) NOT NULL,
  `m_cashier_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`m_order_id`),
  KEY `idx_documentno` (`documentno`),
  KEY `FK_47cumci2tuxm3lb366jpe735n` (`m_cashier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_orderline`
--

CREATE TABLE IF NOT EXISTS `m_orderline` (
  `m_orderline_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedUser` varchar(255) DEFAULT NULL,
  `discountamount` double NOT NULL,
  `discountpercent` double NOT NULL,
  `guarantee` int(11) DEFAULT NULL,
  `locked` bit(1) NOT NULL,
  `price` double DEFAULT NULL,
  `productpromo` bit(1) DEFAULT NULL,
  `qty` double DEFAULT NULL,
  `m_order_id` int(11) DEFAULT NULL,
  `m_product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`m_orderline_id`),
  KEY `FK_fu8lu6lfkblxk6305avq93fgr` (`m_order_id`),
  KEY `FK_bi92smsyqshivduk3fneuxycy` (`m_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_orderpayment`
--

CREATE TABLE IF NOT EXISTS `m_orderpayment` (
  `m_orderpayment_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedUser` varchar(255) DEFAULT NULL,
  `cardname` varchar(255) DEFAULT NULL,
  `cardno` varchar(255) DEFAULT NULL,
  `paymentamount` double NOT NULL,
  `paymenttype` varchar(1) NOT NULL,
  `m_edc_id` int(11) DEFAULT NULL,
  `m_order_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`m_orderpayment_id`),
  KEY `FK_2f5nnmvw6vqvvyu17s50b3upj` (`m_edc_id`),
  KEY `FK_qg2spuy9m2aj0kuuyybbmppok` (`m_order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_privilege`
--

CREATE TABLE IF NOT EXISTS `m_privilege` (
  `m_privilege_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedUser` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `m_menu_id` int(11) DEFAULT NULL,
  `m_role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`m_privilege_id`),
  KEY `FK_n7w6enhghdkkcc7p5h6pgq899` (`m_menu_id`),
  KEY `FK_s7xi3rx60u51tqpia7045lmnx` (`m_role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `m_privilege`
--

INSERT INTO `m_privilege` (`m_privilege_id`, `created`, `createdUser`, `updated`, `updatedUser`, `active`, `m_menu_id`, `m_role_id`) VALUES
(1, '2018-05-12 11:37:32', 'rifqi', NULL, NULL, b'1', 1, 1),
(2, '2018-05-12 11:37:32', 'rifqi', NULL, NULL, b'1', 2, 1),
(3, '2018-05-12 11:37:32', 'rifqi', NULL, NULL, b'1', 3, 1),
(4, '2018-05-12 11:37:32', 'rifqi', NULL, NULL, b'1', 4, 1),
(5, '2018-05-12 11:37:32', 'rifqi', NULL, NULL, b'1', 5, 1),
(6, '2018-05-12 11:37:32', 'rifqi', NULL, NULL, b'1', 6, 1),
(7, '2018-05-12 11:37:32', 'rifqi', NULL, NULL, b'1', 7, 1),
(8, '2018-05-12 11:37:33', 'rifqi', NULL, NULL, b'1', 8, 1),
(9, '2018-05-12 11:37:33', 'rifqi', NULL, NULL, b'1', 9, 1),
(10, '2018-05-12 11:37:33', 'rifqi', NULL, NULL, b'1', 10, 1),
(11, '2018-05-12 11:37:33', 'rifqi', NULL, NULL, b'1', 11, 1),
(12, '2018-05-12 11:37:33', 'rifqi', NULL, NULL, b'1', 12, 1),
(13, '2018-05-12 11:37:47', 'rifqi', NULL, NULL, b'1', 1, 2),
(14, '2018-05-12 11:37:47', 'rifqi', NULL, NULL, b'1', 2, 2),
(15, '2018-05-12 11:37:47', 'rifqi', NULL, NULL, b'1', 3, 2),
(16, '2018-05-12 11:37:47', 'rifqi', NULL, NULL, b'1', 4, 2),
(17, '2018-05-12 11:37:47', 'rifqi', NULL, NULL, b'1', 5, 2),
(18, '2018-05-12 11:37:47', 'rifqi', NULL, NULL, b'1', 6, 2),
(19, '2018-05-12 11:37:47', 'rifqi', NULL, NULL, b'1', 7, 2),
(20, '2018-05-12 11:37:47', 'rifqi', NULL, NULL, b'1', 8, 2),
(21, '2018-05-12 11:37:47', 'rifqi', NULL, NULL, b'1', 9, 2),
(22, '2018-05-12 11:37:47', 'rifqi', NULL, NULL, b'1', 10, 2),
(23, '2018-05-12 11:37:47', 'rifqi', NULL, NULL, b'1', 11, 2),
(24, '2018-05-12 11:37:47', 'rifqi', NULL, NULL, b'1', 12, 2);

-- --------------------------------------------------------

--
-- Table structure for table `m_product`
--

CREATE TABLE IF NOT EXISTS `m_product` (
  `m_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedUser` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `code` varchar(255) NOT NULL,
  `guarantee` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `price` double DEFAULT NULL,
  `m_product_category_id` int(11) DEFAULT NULL,
  `m_unit_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`m_product_id`),
  UNIQUE KEY `idx_name` (`name`),
  UNIQUE KEY `idx_code` (`code`),
  KEY `FK_6jjiwejficd3ql5na6w41wyov` (`m_product_category_id`),
  KEY `FK_jajtk333ol6ruuv0ngl7aknnn` (`m_unit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=421 ;

--
-- Dumping data for table `m_product`
--

INSERT INTO `m_product` (`m_product_id`, `created`, `createdUser`, `updated`, `updatedUser`, `active`, `code`, `guarantee`, `name`, `price`, `m_product_category_id`, `m_unit_id`) VALUES
(1, '2018-05-12 13:03:34', 'rifqi', NULL, NULL, b'1', 'M220 VIZZ', 0, 'BT ACR M220 VIZZ', 88000, NULL, 1),
(2, '2018-05-12 13:03:34', 'rifqi', NULL, NULL, b'1', 'Z320 VIZZ', 0, 'BT ACR Z320 VIZZ', 114000, NULL, 1),
(3, '2018-05-12 13:03:34', 'rifqi', NULL, NULL, b'1', 'Z520 VIZZ', 0, 'BT ACR Z520 VIZZ', 106000, NULL, 1),
(4, '2018-05-12 13:03:35', 'rifqi', NULL, NULL, b'1', 'i4C VIZZ', 0, 'BT ADV i4C VIZZ', 85000, NULL, 1),
(5, '2018-05-12 13:03:35', 'rifqi', NULL, NULL, b'1', 'i4D VIZZ', 0, 'BT ADV i4D VIZZ', 75000, NULL, 1),
(6, '2018-05-12 13:03:35', 'rifqi', NULL, NULL, b'1', 'i5C VIZZ', 0, 'BT ADV i5C VIZZ', 94500, NULL, 1),
(7, '2018-05-12 13:03:35', 'rifqi', NULL, NULL, b'1', 'M4 ADV VIZZ', 0, 'BT ADV M4 VIZZ', 70000, NULL, 1),
(8, '2018-05-12 13:03:35', 'rifqi', NULL, NULL, b'1', 'R3D VIZZ', 0, 'BT ADV R3D VIZZ', 60000, NULL, 1),
(9, '2018-05-12 13:03:35', 'rifqi', NULL, NULL, b'1', 'R3E VIZZ', 0, 'BT ADV R3E VIZZ', 51000, NULL, 1),
(10, '2018-05-12 13:03:35', 'rifqi', NULL, NULL, b'1', 'S3 LITE VIZZ', 0, 'BT ADV S3 LITE VIZZ', 68500, NULL, 1),
(11, '2018-05-12 13:03:35', 'rifqi', NULL, NULL, b'1', '3501 VIZZ', 0, 'BT ADV S3(3501) VIZZ', 73000, NULL, 1),
(12, '2018-05-12 13:03:35', 'rifqi', NULL, NULL, b'1', 'S35D VIZZ', 0, 'BT ADV S35D VIZZ', 70000, NULL, 1),
(13, '2018-05-12 13:03:35', 'rifqi', NULL, NULL, b'1', 'S35E VIZZ', 0, 'BT ADV S35E VIZZ', 79000, NULL, 1),
(14, '2018-05-12 13:03:35', 'rifqi', NULL, NULL, b'1', 'S3A VIZZ', 0, 'BT ADV S3A VIZZ', 79000, NULL, 1),
(15, '2018-05-12 13:03:35', 'rifqi', NULL, NULL, b'1', 'S3C VIZZ', 0, 'BT ADV S3C VIZZ', 79000, NULL, 1),
(16, '2018-05-12 13:03:35', 'rifqi', NULL, NULL, b'1', 'S4+ VIZZ', 0, 'BT ADV S4+ VIZZ', 68500, NULL, 1),
(17, '2018-05-12 13:03:35', 'rifqi', NULL, NULL, b'1', 'S45C VIZZ', 0, 'BT ADV S45C VIZZ', 85000, NULL, 1),
(18, '2018-05-12 13:03:35', 'rifqi', NULL, NULL, b'1', 'S4A VIZZ', 0, 'BT ADV S4A VIZZ', 68500, NULL, 1),
(19, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S4A+ VIZZ', 0, 'BT ADV S4A+ VIZZ', 73000, NULL, 1),
(20, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S4C VIZZ', 0, 'BT ADV S4C VIZZ', 68500, NULL, 1),
(21, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S4E VIZZ', 0, 'BT ADV S4E VIZZ', 85000, NULL, 1),
(22, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S4G VIZZ', 0, 'BT ADV S4G VIZZ', 65500, NULL, 1),
(23, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S4H VIZZ', 0, 'BT ADV S4H VIZZ', 70000, NULL, 1),
(24, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S4i VIZZ', 0, 'BT ADV S4i VIZZ', 70000, NULL, 1),
(25, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S4K VIZZ', 0, 'BT ADV S4K VIZZ', 85000, NULL, 1),
(26, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S4P VIZZ', 0, 'BT ADV S4P VIZZ', 67000, NULL, 1),
(27, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S50 VIZZ', 0, 'BT ADV S50 VIZZ', 97000, NULL, 1),
(28, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S50D VIZZ', 0, 'BT ADV S50D VIZZ', 105000, NULL, 1),
(29, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S50K VIZZ', 0, 'BT ADV S50K VIZZ', 90000, NULL, 1),
(30, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S5D VIZZ', 0, 'BT ADV S5D VIZZ', 93000, NULL, 1),
(31, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S5E NXT VIZZ', 0, 'BT ADV S5E NXT VIZZ', 112500, NULL, 1),
(32, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S5E VIZZ', 0, 'BT ADV S5E VIZZ', 85000, NULL, 1),
(33, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S5E+ VIZZ', 0, 'BT ADV S5E+ VIZZ', 90000, NULL, 1),
(34, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S5H VIZZ', 0, 'BT ADV S5H VIZZ', 96000, NULL, 1),
(35, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S5J VIZZ', 0, 'BT ADV S5J VIZZ', 90000, NULL, 1),
(36, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S5J+ VIZZ', 0, 'BT ADV S5J+ VIZZ', 71500, NULL, 1),
(37, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'S5P VIZZ', 0, 'BT ADV S5P VIZZ', 71500, NULL, 1),
(38, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'ZB452KG VIZZ', 0, 'BT ASS ZB452KG VIZZ', 120000, NULL, 1),
(39, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'ZE500KG VIZZ', 0, 'BT ASS ZE500KG VIZZ', 120000, NULL, 1),
(40, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'ZE500KL VIZZ', 0, 'BT ASS ZE500KL VIZZ', 124500, NULL, 1),
(41, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'ZE550ML VIZZ', 0, 'BT ASS ZE550ML VIZZ', 180000, NULL, 1),
(42, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'ZEN 2 LASER VIZZ', 0, 'BT ASS ZENFONE 2 LASER VIZZ', 142500, NULL, 1),
(43, '2018-05-12 13:03:36', 'rifqi', NULL, NULL, b'1', 'ZEN 4 VIZZ', 0, 'BT ASS ZENFONE 4 VIZZ', 88000, NULL, 1),
(44, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'ZEN 4S VIZZ', 0, 'BT ASS ZENFONE 4S VIZZ', 92500, NULL, 1),
(45, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'ZEN 5 VIZZ', 0, 'BT ASS ZENFONE 5 VIZZ', 127500, NULL, 1),
(46, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'ZEN C VIZZ', 0, 'BT ASS ZENFONE C VIZZ', 123000, NULL, 1),
(47, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'ZEN GO 5 VIZZ', 0, 'BT ASS ZENFONE GO 5 VIZZ', 120000, NULL, 1),
(48, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'CM2 VIZZ', 0, 'BT BB CM2 VIZZ', 65500, NULL, 1),
(49, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'CS2 VIZZ', 0, 'BT BB CS2 VIZZ', 65500, NULL, 1),
(50, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'DAKOTA VIZZ', 0, 'BT BB DAKOTA VIZZ', 92500, NULL, 1),
(51, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'DX1 VIZZ', 0, 'BT BB DX1 VIZZ', 70000, NULL, 1),
(52, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'EM1 VIZZ', 0, 'BT BB EM1 VIZZ', 92500, NULL, 1),
(53, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'FM1 VIZZ', 0, 'BT BB FM1 VIZZ', 92500, NULL, 1),
(54, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'FS1 VIZZ', 0, 'BT BB FS1 VIZZ', 92500, NULL, 1),
(55, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'JM1 VIZZ', 0, 'BT BB JM1 VIZZ', 92500, NULL, 1),
(56, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'JS1 VIZZ', 0, 'BT BB JS1 VIZZ', 92500, NULL, 1),
(57, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'LS1 VIZZ', 0, 'BT BB LS1 VIZZ', 180000, NULL, 1),
(58, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'MS1 VIZZ', 0, 'BT BB MS1 VIZZ', 79000, NULL, 1),
(59, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'NX1 VIZZ', 0, 'BT BB NX1 VIZZ', 180000, NULL, 1),
(60, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'Q5 VIZZ', 0, 'BT BB Q5 VIZZ', 202500, NULL, 1),
(61, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'Z30 VIZZ', 0, 'BT BB Z30 VIZZ', 225000, NULL, 1),
(62, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'WIFI AQUILLA VIZZ', 0, 'BT BOLT AQUILLA VIZZ', 79000, NULL, 1),
(63, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'E5577 VIZZ', 0, 'BT BOLT E5577 SLIM 2 VIZZ', 99000, NULL, 1),
(64, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'WIFI MF90 VIZZ', 0, 'BT BOLT MF90 VIZZ', 103000, NULL, 1),
(65, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', 'WIFI ORION VIZZ', 0, 'BT BOLT ORION VIZZ', 80500, NULL, 1),
(66, '2018-05-12 13:03:37', 'rifqi', NULL, NULL, b'1', '4LC VIZZ', 0, 'BT EVCOSS 4LC VIZZ', 73000, NULL, 1),
(67, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'A12 VIZZ', 0, 'BT EVCOSS A12 VIZZ', 79000, NULL, 1),
(68, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'A26C VIZZ', 0, 'BT EVCOSS A26C VIZZ', 68500, NULL, 1),
(69, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'A28A VIZZ', 0, 'BT EVCOSS A28A VIZZ', 85000, NULL, 1),
(70, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'A33A VIZZ', 0, 'BT EVCOSS A33A VIZZ', 82000, NULL, 1),
(71, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'A35B VIZZ', 0, 'BT EVCOSS A35B VIZZ', 72000, NULL, 1),
(72, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'A53C VIZZ', 0, 'BT EVCOSS A53C VIZZ', 70000, NULL, 1),
(73, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'A54 VIZZ', 0, 'BT EVCOSS A54 VIZZ', 64000, NULL, 1),
(74, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'A65 VIZZ', 0, 'BT EVCOSS A65 VIZZ', 114000, NULL, 1),
(75, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'A66A VIZZ', 0, 'BT EVCOSS A66A VIZZ', 114000, NULL, 1),
(76, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'A74E VIZZ', 0, 'BT EVCOSS A74E VIZZ', 68500, NULL, 1),
(77, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'A74M VIZZ', 0, 'BT EVCOSS A74M VIZZ', 64000, NULL, 1),
(78, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'A75 MAX VIZZ', 0, 'BT EVCOSS A75 MAX VIZZ', 124500, NULL, 1),
(79, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'A75 VIZZ', 0, 'BT EVCOSS A75 VIZZ', 68500, NULL, 1),
(80, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'A7A VIZZ', 0, 'BT EVCOSS A7A VIZZ', 70500, NULL, 1),
(81, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'A7E VIZZ', 0, 'BT EVCOSS A7E VIZZ', 85000, NULL, 1),
(82, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'A7K VIZZ', 0, 'BT EVCOSS A7K VIZZ', 85000, NULL, 1),
(83, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'A7S VIZZ', 0, 'BT EVCOSS A7S VIZZ', 89500, NULL, 1),
(84, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'A7T VIZZ', 0, 'BT EVCOSS A7T VIZZ', 71500, NULL, 1),
(85, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'C5 VIZZ', 0, 'BT EVCOSS C5 VIZZ', 56500, NULL, 1),
(86, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'EG971 VIZZ', 0, 'BT HIS EG971 VIZZ', 107500, NULL, 1),
(87, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'G10 VIZZ', 0, 'BT HTC G10 VIZZ', 77500, NULL, 1),
(88, '2018-05-12 13:03:38', 'rifqi', NULL, NULL, b'1', 'G13 VIZZ', 0, 'BT HTC G13 VIZZ', 77500, NULL, 1),
(89, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', 'G7 VIZZ', 0, 'BT HTC G7 VIZZ', 77500, NULL, 1),
(90, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', 'HD2 VIZZ', 0, 'BT HTC HD2 VIZZ', 77500, NULL, 1),
(91, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', '3C VIZZ', 0, 'BT HWEI 3C VIZZ', 111000, NULL, 1),
(92, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', '3X VIZZ', 0, 'BT HWEI 3X VIZZ', 106000, NULL, 1),
(93, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', 'P6 VIZZ', 0, 'BT HWEI P6 VIZZ', 114000, NULL, 1),
(94, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', 'P7 VIZZ', 0, 'BT HWEI P7 VIZZ', 127500, NULL, 1),
(95, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', 'Y520 VIZZ', 0, 'BT HWEI Y520 VIZZ', 92500, NULL, 1),
(96, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', 'IP 4G VIZZ', 0, 'BT IPH 4G VIZZ', 107500, NULL, 1),
(97, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', 'IP 4S VIZZ', 0, 'BT IPH 4S VIZZ', 135000, NULL, 1),
(98, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', 'IP 5C VIZZ', 0, 'BT IPH 5C VIZZ', 157500, NULL, 1),
(99, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', 'IP 5G VIZZ', 0, 'BT IPH 5G VIZZ', 135000, NULL, 1),
(100, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', 'IP 5S VIZZ', 0, 'BT IPH 5S VIZZ', 150000, NULL, 1),
(101, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', 'IP 6+ VIZZ', 0, 'BT IPH 6+ VIZZ', 180000, NULL, 1),
(102, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', 'IP 6G VIZZ', 0, 'BT IPH 6G VIZZ', 165000, NULL, 1),
(103, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', 'IP 6S VIZZ', 0, 'BT IPH 6S VIZZ', 172500, NULL, 1),
(104, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', 'A1000 TAB VIZZ', 0, 'BT LEV A1000 TAB VIZZ', 180000, NULL, 1),
(105, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', 'A369 VIZZ', 0, 'BT LEV A369 VIZZ', 83500, NULL, 1),
(106, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', 'A390 VIZZ', 0, 'BT LEV A390 VIZZ', 85000, NULL, 1),
(107, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', 'A690 VIZZ', 0, 'BT LEV A690 VIZZ', 89500, NULL, 1),
(108, '2018-05-12 13:03:39', 'rifqi', NULL, NULL, b'1', 'A706 VIZZ', 0, 'BT LEV A706 VIZZ', 114000, NULL, 1),
(109, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL169 VIZZ', 0, 'BT LEV BL169 VIZZ', 114000, NULL, 1),
(110, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL189 VIZZ', 0, 'BT LEV BL189 VIZZ', 85000, NULL, 1),
(111, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL192 VIZZ', 0, 'BT LEV BL192 VIZZ', 114000, NULL, 1),
(112, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL197 VIZZ', 0, 'BT LEV BL197 VIZZ', 107500, NULL, 1),
(113, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL204 VIZZ', 0, 'BT LEV BL204 VIZZ', 92500, NULL, 1),
(114, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL205 VIZZ', 0, 'BT LEV BL205 VIZZ', 172500, NULL, 1),
(115, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL207 VIZZ', 0, 'BT LEV BL207 VIZZ', 165000, NULL, 1),
(116, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL208 VIZZ', 0, 'BT LEV BL208 VIZZ', 123000, NULL, 1),
(117, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL210 VIZZ', 0, 'BT LEV BL210 VIZZ', 114000, NULL, 1),
(118, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL211 VIZZ', 0, 'BT LEV BL211 VIZZ', 130500, NULL, 1),
(119, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL212 VIZZ', 0, 'BT LEV BL212 VIZZ', 117000, NULL, 1),
(120, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL215 VIZZ', 0, 'BT LEV BL215 VIZZ', 130500, NULL, 1),
(121, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL216 VIZZ', 0, 'BT LEV BL216 VIZZ', 187500, NULL, 1),
(122, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL217 VIZZ', 0, 'BT LEV BL217 VIZZ', 165000, NULL, 1),
(123, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL219 VIZZ', 0, 'BT LEV BL219 VIZZ', 123000, NULL, 1),
(124, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL220 VIZZ', 0, 'BT LEV BL220 VIZZ', 124500, NULL, 1),
(125, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL222 VIZZ', 0, 'BT LEV BL222 VIZZ', 135000, NULL, 1),
(126, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL233 VIZZ', 0, 'BT LEV BL233 VIZZ', 79000, NULL, 1),
(127, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL242 VIZZ', 0, 'BT LEV BL242 VIZZ', 130500, NULL, 1),
(128, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL243 VIZZ', 0, 'BT LEV BL243 VIZZ', 130500, NULL, 1),
(129, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL253 VIZZ', 0, 'BT LEV BL253 VIZZ', 103000, NULL, 1),
(130, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL259 VIZZ', 0, 'BT LEV BL259 VIZZ', 142500, NULL, 1),
(131, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL261 VIZZ', 0, 'BT LEV BL261 VIZZ', 165000, NULL, 1),
(132, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'S880 VIZZ', 0, 'BT LEV S880 VIZZ', 127500, NULL, 1),
(133, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL44JH VIZZ', 0, 'BT LG BL44JH VIZZ', 103000, NULL, 1),
(134, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL44JN VIZZ', 0, 'BT LG BL44JN VIZZ', 82000, NULL, 1),
(135, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL44JR VIZZ', 0, 'BT LG BL44JR VIZZ', 82000, NULL, 1),
(136, '2018-05-12 13:03:40', 'rifqi', NULL, NULL, b'1', 'BL48TH VIZZ', 0, 'BT LG BL48TH VIZZ', 157500, NULL, 1),
(137, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'BL53QH VIZZ', 0, 'BT LG BL53QH VIZZ', 142500, NULL, 1),
(138, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'G3 VIZZ', 0, 'BT LG G3 VIZZ', 172500, NULL, 1),
(139, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'G4 VIZZ', 0, 'BT LG G4 VIZZ', 172500, NULL, 1),
(140, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'G5 VIZZ', 0, 'BT LG G5 VIZZ', 180000, NULL, 1),
(141, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'M2Y VIZZ', 0, 'BT M2Y VIZZ', 71500, NULL, 1),
(142, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'M3Y VIZZ', 0, 'BT M3Y VIZZ', 109000, NULL, 1),
(143, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'MAX-A VIZZ', 0, 'BT MAX-A VIZZ', 98500, NULL, 1),
(144, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'MAX-B VIZZ', 0, 'BT MAX-B VIZZ', 98500, NULL, 1),
(145, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'MAX-C VIZZ', 0, 'BT MAX-C VIZZ', 85000, NULL, 1),
(146, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'MAX-C2 VIZZ', 0, 'BT MAX-C2 VIZZ', 85000, NULL, 1),
(147, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'MAX-C3 VIZZ', 0, 'BT MAX-C3 VIZZ', 85000, NULL, 1),
(148, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'MAX-E2 PLUS VIZZ', 0, 'BT MAX-E2 PLUS VIZZ', 103000, NULL, 1),
(149, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'MAX-E2 VIZZ', 0, 'BT MAX-E2 VIZZ', 95500, NULL, 1),
(150, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'MAX-EC VIZZ', 0, 'BT MAX-EC VIZZ', 88000, NULL, 1),
(151, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'MAX-G VIZZ', 0, 'BT MAX-G VIZZ', 85000, NULL, 1),
(152, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'MAX-G2 VIZZ', 0, 'BT MAX-G2 VIZZ', 88000, NULL, 1),
(153, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'MAX-I VIZZ', 0, 'BT MAX-I VIZZ', 77500, NULL, 1),
(154, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'MAX-I2 VIZZ', 0, 'BT MAX-I2 VIZZ', 114000, NULL, 1),
(155, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'MAX-I3 VIZZ', 0, 'BT MAX-I3 VIZZ', 106000, NULL, 1),
(156, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'MAX-I3S VIZZ', 0, 'BT MAX-I3S VIZZ', 114000, NULL, 1),
(157, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'MAX-L VIZZ', 0, 'BT MAX-L VIZZ', 130500, NULL, 1),
(158, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'MAX-Q VIZZ', 0, 'BT MAX-Q VIZZ', 106000, NULL, 1),
(159, '2018-05-12 13:03:41', 'rifqi', NULL, NULL, b'1', 'MAX-Qi VIZZ', 0, 'BT MAX-Qi VIZZ', 100000, NULL, 1),
(160, '2018-05-12 13:03:42', 'rifqi', NULL, NULL, b'1', 'MAX-R VIZZ', 0, 'BT MAX-R VIZZ', 114000, NULL, 1),
(161, '2018-05-12 13:03:42', 'rifqi', NULL, NULL, b'1', 'MAX-T VIZZ', 0, 'BT MAX-T VIZZ', 114000, NULL, 1),
(162, '2018-05-12 13:03:42', 'rifqi', NULL, NULL, b'1', 'MAX-U PLAT VIZZ', 0, 'BT MAX-U PLATINUM VIZZ', 85000, NULL, 1),
(163, '2018-05-12 13:03:42', 'rifqi', NULL, NULL, b'1', 'MAX-U2 VIZZ', 0, 'BT MAX-U2 VIZZ', 103000, NULL, 1),
(164, '2018-05-12 13:03:42', 'rifqi', NULL, NULL, b'1', 'MAX-U3 VIZZ', 0, 'BT MAX-U3 VIZZ', 130500, NULL, 1),
(165, '2018-05-12 13:03:42', 'rifqi', NULL, NULL, b'1', 'MAX-V VIZZ', 0, 'BT MAX-V VIZZ', 114000, NULL, 1),
(166, '2018-05-12 13:03:42', 'rifqi', NULL, NULL, b'1', 'MAX-Z VIZZ', 0, 'BT MAX-Z VIZZ', 150000, NULL, 1),
(167, '2018-05-12 13:03:42', 'rifqi', NULL, NULL, b'1', 'A120 VIZZ', 0, 'BT MTO A120 VIZZ', 82000, NULL, 1),
(168, '2018-05-12 13:03:42', 'rifqi', NULL, NULL, b'1', 'A230 VIZZ', 0, 'BT MTO A230 VIZZ', 78000, NULL, 1),
(169, '2018-05-12 13:03:42', 'rifqi', NULL, NULL, b'1', 'A260 VIZZ', 0, 'BT MTO A260 VIZZ', 82000, NULL, 1),
(170, '2018-05-12 13:03:42', 'rifqi', NULL, NULL, b'1', 'A750 VIZZ', 0, 'BT MTO A750 VIZZ', 71500, NULL, 1),
(171, '2018-05-12 13:03:42', 'rifqi', NULL, NULL, b'1', 'G900 VIZZ', 0, 'BT NEX G900 VIZZ', 55000, NULL, 1),
(172, '2018-05-12 13:03:42', 'rifqi', NULL, NULL, b'1', 'H888 VIZZ', 0, 'BT NEX H888 VIZZ', 55000, NULL, 1),
(173, '2018-05-12 13:03:42', 'rifqi', NULL, NULL, b'1', '3L VIZZ', 0, 'BT NOK 3L VIZZ', 61000, NULL, 1),
(174, '2018-05-12 13:03:42', 'rifqi', NULL, NULL, b'1', '4B VIZZ', 0, 'BT NOK 4B VIZZ', 53500, NULL, 1),
(175, '2018-05-12 13:03:42', 'rifqi', NULL, NULL, b'1', '4C VIZZ', 0, 'BT NOK 4C VIZZ', 53500, NULL, 1),
(176, '2018-05-12 13:03:42', 'rifqi', NULL, NULL, b'1', '4CT VIZZ', 0, 'BT NOK 4CT VIZZ', 53500, NULL, 1),
(177, '2018-05-12 13:03:42', 'rifqi', NULL, NULL, b'1', '4D VIZZ', 0, 'BT NOK 4D VIZZ', 59500, NULL, 1),
(178, '2018-05-12 13:03:43', 'rifqi', NULL, NULL, b'1', '4J VIZZ', 0, 'BT NOK 4J VIZZ', 53500, NULL, 1),
(179, '2018-05-12 13:03:43', 'rifqi', NULL, NULL, b'1', '4L VIZZ', 0, 'BT NOK 4L VIZZ', 62500, NULL, 1),
(180, '2018-05-12 13:03:43', 'rifqi', NULL, NULL, b'1', '4S VIZZ', 0, 'BT NOK 4S VIZZ', 53500, NULL, 1),
(181, '2018-05-12 13:03:43', 'rifqi', NULL, NULL, b'1', '4U VIZZ', 0, 'BT NOK 4U VIZZ', 58000, NULL, 1),
(182, '2018-05-12 13:03:43', 'rifqi', NULL, NULL, b'1', '4UL VIZZ', 0, 'BT NOK 4UL VIZZ', 68500, NULL, 1),
(183, '2018-05-12 13:03:43', 'rifqi', NULL, NULL, b'1', '4W VIZZ', 0, 'BT NOK 4W VIZZ', 120000, NULL, 1),
(184, '2018-05-12 13:03:43', 'rifqi', NULL, NULL, b'1', '5B VIZZ', 0, 'BT NOK 5B VIZZ', 53500, NULL, 1),
(185, '2018-05-12 13:03:43', 'rifqi', NULL, NULL, b'1', '5BT VIZZ', 0, 'BT NOK 5BT VIZZ', 53500, NULL, 1),
(186, '2018-05-12 13:03:43', 'rifqi', NULL, NULL, b'1', '5C VIZZ', 0, 'BT NOK 5C VIZZ', 53500, NULL, 1),
(187, '2018-05-12 13:03:43', 'rifqi', NULL, NULL, b'1', '5CA VIZZ', 0, 'BT NOK 5CA VIZZ', 50500, NULL, 1),
(188, '2018-05-12 13:03:43', 'rifqi', NULL, NULL, b'1', '5CB VIZZ', 0, 'BT NOK 5CB VIZZ', 50500, NULL, 1),
(189, '2018-05-12 13:03:43', 'rifqi', NULL, NULL, b'1', '5CT VIZZ', 0, 'BT NOK 5CT VIZZ', 53500, NULL, 1),
(190, '2018-05-12 13:03:43', 'rifqi', NULL, NULL, b'1', '5F VIZZ', 0, 'BT NOK 5F VIZZ', 53500, NULL, 1),
(191, '2018-05-12 13:03:43', 'rifqi', NULL, NULL, b'1', '5J VIZZ', 0, 'BT NOK 5J VIZZ', 59500, NULL, 1),
(192, '2018-05-12 13:03:43', 'rifqi', NULL, NULL, b'1', '5K VIZZ', 0, 'BT NOK 5K VIZZ', 59500, NULL, 1),
(193, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', '5M VIZZ', 0, 'BT NOK 5M VIZZ', 53500, NULL, 1),
(194, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', '5T VIZZ', 0, 'BT NOK 5T VIZZ', 92500, NULL, 1),
(195, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', '5U VIZZ', 0, 'BT NOK 5U VIZZ', 59500, NULL, 1),
(196, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', '5Z  VIZZ', 0, 'BT NOK 5Z VIZZ', 82000, NULL, 1),
(197, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', '6C VIZZ', 0, 'BT NOK 6C VIZZ', 53500, NULL, 1),
(198, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', '6F VIZZ', 0, 'BT NOK 6F VIZZ', 53500, NULL, 1),
(199, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', '6M VIZZ', 0, 'BT NOK 6M VIZZ', 56500, NULL, 1),
(200, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', '6MT VIZZ', 0, 'BT NOK 6MT VIZZ', 53500, NULL, 1),
(201, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', '6P VIZZ', 0, 'BT NOK 6P VIZZ', 53500, NULL, 1),
(202, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', '6Q VIZZ', 0, 'BT NOK 6Q VIZZ', 53500, NULL, 1),
(203, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', 'BN01 VIZZ', 0, 'BT NOK BN01 VIZZ', 95500, NULL, 1),
(204, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', 'BN02 VIZZ', 0, 'BT NOK BN02 VIZZ', 106000, NULL, 1),
(205, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', 'BLP519 VIZZ', 0, 'BT OPO BLP519 VIZZ', 88000, NULL, 1),
(206, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', 'BLP537 VIZZ', 0, 'BT OPO BLP537 VIZZ', 106000, NULL, 1),
(207, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', 'BLP553 VIZZ', 0, 'BT OPO BLP553 VIZZ', 165000, NULL, 1),
(208, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', 'BLP573 VIZZ', 0, 'BT OPO BLP573 VIZZ', 79000, NULL, 1),
(209, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', 'BLP579 VIZZ', 0, 'BT OPO BLP579 VIZZ', 139500, NULL, 1),
(210, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', 'BLP599 VIZZ', 0, 'BT OPO BLP599 VIZZ', 142500, NULL, 1),
(211, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', 'BLT029 VIZZ', 0, 'BT OPO BLT029 VIZZ', 97000, NULL, 1),
(212, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', 'OPO JOY 3 VIZZ', 0, 'BT OPO JOY 3 VIZZ', 110500, NULL, 1),
(213, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', 'OPO NEO VIZZ', 0, 'BT OPO NEO VIZZ', 114000, NULL, 1),
(214, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', 'R7S VIZZ', 0, 'BT OPO R7S VIZZ', 114000, NULL, 1),
(215, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', 'A3 VIZZ', 0, 'BT SAM A3 VIZZ', 123000, NULL, 1),
(216, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', 'A5 VIZZ', 0, 'BT SAM A5 VIZZ', 123000, NULL, 1),
(217, '2018-05-12 13:03:44', 'rifqi', NULL, NULL, b'1', 'A8 VIZZ', 0, 'BT SAM A8 VIZZ', 150000, NULL, 1),
(218, '2018-05-12 13:03:45', 'rifqi', NULL, NULL, b'1', 'A9 VIZZ', 0, 'BT SAM A9 VIZZ', 165000, NULL, 1),
(219, '2018-05-12 13:03:45', 'rifqi', NULL, NULL, b'1', 'ACE 3 VIZZ', 0, 'BT SAM ACE 3 VIZZ', 89500, NULL, 1),
(220, '2018-05-12 13:03:45', 'rifqi', NULL, NULL, b'1', 'CORE 2 VIZZ', 0, 'BT SAM CORE 2 VIZZ', 110500, NULL, 1),
(221, '2018-05-12 13:03:45', 'rifqi', NULL, NULL, b'1', 'E7 VIZZ', 0, 'BT SAM E7 VIZZ', 154500, NULL, 1),
(222, '2018-05-12 13:03:45', 'rifqi', NULL, NULL, b'1', 'G130 VIZZ', 0, 'BT SAM G130 VIZZ', 71500, NULL, 1),
(223, '2018-05-12 13:03:45', 'rifqi', NULL, NULL, b'1', 'G530 VIZZ', 0, 'BT SAM G530 VIZZ', 139500, NULL, 1),
(224, '2018-05-12 13:03:45', 'rifqi', NULL, NULL, b'1', 'G600 VIZZ', 0, 'BT SAM G600 VIZZ', 56500, NULL, 1),
(225, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'G750 VIZZ', 0, 'BT SAM G750 VIZZ', 172500, NULL, 1),
(226, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'G8508 VIZZ', 0, 'BT SAM G8508 VIZZ', 114000, NULL, 1),
(227, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'I8160 VIZZ', 0, 'BT SAM I8160 VIZZ', 82000, NULL, 1),
(228, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'I8190 VIZZ', 0, 'BT SAM I8190 VIZZ', 82000, NULL, 1),
(229, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'I8260 VIZZ', 0, 'BT SAM I8260 VIZZ', 106000, NULL, 1),
(230, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'I8530 VIZZ', 0, 'BT SAM I8530 VIZZ', 106000, NULL, 1),
(231, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'I8750 VIZZ', 0, 'BT SAM I8750 VIZZ', 112500, NULL, 1),
(232, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'I9000 VIZZ', 0, 'BT SAM I9000 VIZZ', 73000, NULL, 1),
(233, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'I9070 VIZZ', 0, 'BT SAM I9070 VIZZ', 73000, NULL, 1),
(234, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'I9082 VIZZ', 0, 'BT SAM I9082 VIZZ', 112500, NULL, 1),
(235, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'I9100 VIZZ', 0, 'BT SAM I9100 VIZZ', 79000, NULL, 1),
(236, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'I9152 VIZZ', 0, 'BT SAM I9152 VIZZ', 150000, NULL, 1),
(237, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'I9200 VIZZ', 0, 'BT SAM I9200 VIZZ', 180000, NULL, 1),
(238, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'I9220 VIZZ', 0, 'BT SAM I9220 VIZZ', 150000, NULL, 1),
(239, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'I9250 VIZZ', 0, 'BT SAM I9250 VIZZ', 88000, NULL, 1),
(240, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'J1 2016 VIZZ', 0, 'BT SAM J1 2016 VIZZ', 107500, NULL, 1),
(241, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'J1 ACE VIZZ', 0, 'BT SAM J1 ACE VIZZ', 109000, NULL, 1),
(242, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'J1 MINI VIZZ', 0, 'BT SAM J1 MINI VIZZ', 82000, NULL, 1),
(243, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'J1 VIZZ', 0, 'BT SAM J1 VIZZ', 106000, NULL, 1),
(244, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'J2 VIZZ', 0, 'BT SAM J2 VIZZ', 114000, NULL, 1),
(245, '2018-05-12 13:03:46', 'rifqi', NULL, NULL, b'1', 'J5 VIZZ', 0, 'BT SAM J5 VIZZ', 139500, NULL, 1),
(246, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'J710 VIZZ', 0, 'BT SAM J7 2016 VIZZ', 192000, NULL, 1),
(247, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'J7 VIZZ', 0, 'BT SAM J7 VIZZ', 165000, NULL, 1),
(248, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'KZOOM VIZZ', 0, 'BT SAM KZOOM VIZZ', 120000, NULL, 1),
(249, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'M608/B3210 VIZZ', 0, 'BT SAM M608/B3210 VIZZ', 56500, NULL, 1),
(250, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'N7100 VIZZ', 0, 'BT SAM N7100 VIZZ', 157500, NULL, 1),
(251, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'N9000 VIZZ', 0, 'BT SAM N9000/NOTE3 VIZZ', 180000, NULL, 1),
(252, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'NOTE 4 VIZZ', 0, 'BT SAM NOTE 4 VIZZ', 180000, NULL, 1),
(253, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'NOTE EDGE VIZZ', 0, 'BT SAM NOTE EDGE VIZZ', 172500, NULL, 1),
(254, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'NOTE3 HC VIZZ', 0, 'BT SAM NOTE3 HC VIZZ', 127500, NULL, 1),
(255, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'NOTE3 MINI VIZZ', 0, 'BT SAM NOTE3 MINI VIZZ', 150000, NULL, 1),
(256, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'NOTE4 HC VIZZ', 0, 'BT SAM NOTE4 HC VIZZ', 127500, NULL, 1),
(257, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'S3 I9300 VIZZ', 0, 'BT SAM S3 I9300 VIZZ', 112500, NULL, 1),
(258, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'S3 SLIM VIZZ', 0, 'BT SAM S3 SLIM VIZZ', 112500, NULL, 1),
(259, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'S3850 VIZZ', 0, 'BT SAM S3850 VIZZ', 64000, NULL, 1),
(260, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'S4HC VIZZ', 0, 'BT SAM S4 HIGHCOPY VIZZ', 92500, NULL, 1),
(261, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'S4 I9500 VIZZ', 0, 'BT SAM S4 I9500 VIZZ', 150000, NULL, 1),
(262, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'S4 MINI VIZZ', 0, 'BT SAM S4 MINI VIZZ', 142500, NULL, 1),
(263, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'S5HC VIZZ', 0, 'BT SAM S5 HIGHCOPY VIZZ', 120000, NULL, 1),
(264, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'S5 I9600 VIZZ', 0, 'BT SAM S5 I9600 VIZZ', 172500, NULL, 1),
(265, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'S5 NEO VIZZ', 0, 'BT SAM S5 NEO VIZZ', 150000, NULL, 1),
(266, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'S5360 VIZZ', 0, 'BT SAM S5360 VIZZ', 65500, NULL, 1),
(267, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'S5570 VIZZ', 0, 'BT SAM S5570 VIZZ', 65500, NULL, 1),
(268, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'S5820 VIZZ', 0, 'BT SAM S5820 VIZZ', 73000, NULL, 1),
(269, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'S5830 VIZZ', 0, 'BT SAM S5830 VIZZ', 73000, NULL, 1),
(270, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'S7 EDGE PLUS VIZZ', 0, 'BT SAM S7 EDGE PLUS VIZZ', 157500, NULL, 1),
(271, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'S7260 VIZZ', 0, 'BT SAM S7260 VIZZ', 89500, NULL, 1),
(272, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'Z2 TIZEN VIZZ', 0, 'BT SAM TIZEN Z2 VIZZ', 85000, NULL, 1),
(273, '2018-05-12 13:03:47', 'rifqi', NULL, NULL, b'1', 'W559 VIZZ', 0, 'BT SAM W559/B3410 VIZZ', 59500, NULL, 1),
(274, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'X200 VIZZ', 0, 'BT SAM X200 VIZZ', 56500, NULL, 1),
(275, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'BA600 VIZZ', 0, 'BT SON BA600 VIZZ', 71500, NULL, 1),
(276, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'BA700 VIZZ', 0, 'BT SON BA700 VIZZ', 86500, NULL, 1),
(277, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'BA750 VIZZ', 0, 'BT SON BA750 VIZZ', 86500, NULL, 1),
(278, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'BA800 VIZZ', 0, 'BT SON BA800 VIZZ', 103000, NULL, 1),
(279, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'BA900 VIZZ', 0, 'BT SON BA900 VIZZ', 106000, NULL, 1),
(280, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'BA950 VIZZ', 0, 'BT SON BA950 VIZZ', 127500, NULL, 1),
(281, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'BST33 VIZZ', 0, 'BT SON BST33 VIZZ', 56500, NULL, 1),
(282, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'BST36 VIZZ', 0, 'BT SON BST36 VIZZ', 56500, NULL, 1),
(283, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'BST37 VIZZ', 0, 'BT SON BST37 VIZZ', 56500, NULL, 1),
(284, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'BST38 VIZZ', 0, 'BT SON BST38 VIZZ', 56500, NULL, 1),
(285, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'BST39 VIZZ', 0, 'BT SON BST39 VIZZ', 56500, NULL, 1),
(286, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'BST41 VIZZ', 0, 'BT SON BST41 VIZZ', 73000, NULL, 1),
(287, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'BST43 VIZZ', 0, 'BT SON BST43 VIZZ', 56500, NULL, 1),
(288, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'C3 SON VIZZ', 0, 'BT SON C3 VIZZ', 165000, NULL, 1),
(289, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'EP500 VIZZ', 0, 'BT SON EP500 VIZZ', 56500, NULL, 1),
(290, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'M4 SON VIZZ', 0, 'BT SON M4 VIZZ', 157500, NULL, 1),
(291, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'Z ULTRA VIZZ', 0, 'BT SON Z ULTRA VIZZ', 225000, NULL, 1),
(292, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'Z1 VIZZ', 0, 'BT SON Z1 VIZZ', 240000, NULL, 1),
(293, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'Z3 MINI VIZZ', 0, 'BT SON Z3 MINI VIZZ', 142500, NULL, 1),
(294, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'Z4 SON VIZZ', 0, 'BT SON Z4 VIZZ', 187500, NULL, 1),
(295, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'P12 VIZZ', 0, 'BT TRON P12 VIZZ', 87000, NULL, 1),
(296, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'Y11 VIZZ', 0, 'BT VIV Y11 VIZZ', 85000, NULL, 1),
(297, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'Y15 VIZZ', 0, 'BT VIV Y15 VIZZ', 110500, NULL, 1),
(298, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'Y33 VIZZ', 0, 'BT VIV Y33 VIZZ', 139500, NULL, 1),
(299, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'BM42 VIZZ', 0, 'BT XMI BM42 REDMI NOTE VIZZ', 180000, NULL, 1),
(300, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'BM44 VIZZ', 0, 'BT XMI BM44 REDMI 2 VIZZ', 123000, NULL, 1),
(301, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'BM45 VIZZ', 0, 'BT XMI BM45 REDMI NOTE 2 VIZZ', 180000, NULL, 1),
(302, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'REDMI 1S VIZZ', 0, 'BT XMI REDMI 1S VIZZ', 123000, NULL, 1),
(303, '2018-05-12 13:03:48', 'rifqi', NULL, NULL, b'1', 'BLADE A5 VIZZ', 0, 'BT ZTE BLADE A5 VIZZ', 115500, NULL, 1),
(304, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'BLADE QLUX VIZZ', 0, 'BT ZTE BLADE QLUX VIZZ', 114000, NULL, 1),
(305, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'V811 VIZZ', 0, 'BT ZTE V811 GOJEK 2 VIZZ', 70000, NULL, 1),
(306, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'V815W VIZZ', 0, 'BT ZTE V815W GOJEK VIZZ', 85000, NULL, 1),
(307, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZCD31 VIZZ', 0, 'CABLE DATA 3 IN 1 VIZZ', 70500, NULL, 1),
(308, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZCDC02-TYPE C VIZZ', 0, 'CABLE DATA CDC02 TYPE C VIZZ', 36000, NULL, 1),
(309, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZCDC03-TYPE C VIZZ', 0, 'CABLE DATA CDC03 TYPE C VIZZ', 45000, NULL, 1),
(310, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZCDI02-i5 VIZZ', 0, 'CABLE DATA CDI02-I5 IOS VIZZ', 37500, NULL, 1),
(311, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZCDI03-i5 VIZZ', 0, 'CABLE DATA CDI03-I5 IOS VIZZ', 45000, NULL, 1),
(312, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZCDM02-V8 VIZZ', 0, 'CABLE DATA CDM02-V8 MICRO VIZZ', 33000, NULL, 1),
(313, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZCDM03-V8 VIZZ', 0, 'CABLE DATA CDM03-V8 MICRO VIZZ', 37500, NULL, 1),
(314, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZCDI-100 VIZZ', 0, 'CABLE DATA IOS 100CM VIZZ', 28500, NULL, 1),
(315, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZCDI-180 VIZZ', 0, 'CABLE DATA IOS 180CM VIZZ', 32500, NULL, 1),
(316, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZCDI-30 VIZZ', 0, 'CABLE DATA IOS 30CM VIZZ', 22000, NULL, 1),
(317, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZCDI-60 VIZZ', 0, 'CABLE DATA IOS 60CM VIZZ', 25000, NULL, 1),
(318, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZCDI4-100 VIZZ', 0, 'CABLE DATA IOS IP-4 100CM VIZZ', 26500, NULL, 1),
(319, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZCDI4-180 VIZZ', 0, 'CABLE DATA IOS IP-4 180CM VIZZ', 31000, NULL, 1),
(320, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZCDM-100 VIZZ', 0, 'CABLE DATA MICRO 100CM VIZZ', 22000, NULL, 1),
(321, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZCDM-180 VIZZ', 0, 'CABLE DATA MICRO 180CM VIZZ', 25000, NULL, 1),
(322, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZCDM-30 VIZZ', 0, 'CABLE DATA MICRO 30CM VIZZ', 17500, NULL, 1),
(323, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZCDM-60 VIZZ', 0, 'CABLE DATA MICRO 60CM VIZZ', 19500, NULL, 1),
(324, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZCDC-100 VIZZ', 0, 'CABLE DATA MICRO TYPE C 2.0 100CM VIZZ', 28500, NULL, 1),
(325, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'MINI TUBE IOS VIZZ', 0, 'CAR CHARGER MINI TUBE IOS VIZZ', 110000, NULL, 1),
(326, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'MINI TUBE MICRO VIZZ', 0, 'CAR CHARGER MINI TUBE MICRO VIZZ', 105000, NULL, 1),
(327, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZ27QC IOS VIZZ', 0, 'CAR CHARGER VZ27QC IOS QUICK QUALCOMM 3.0 VIZZ', 105000, NULL, 1),
(328, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZ27QC MICRO VIZZ', 0, 'CAR CHARGER VZ27QC MICRO QUICK QUALCOMM 3.0 VIZZ', 102000, NULL, 1),
(329, '2018-05-12 13:03:49', 'rifqi', NULL, NULL, b'1', 'VZ27QC TYPE C VIZZ', 0, 'CAR CHARGER VZ27QC TYPE C QUICK QUALCOMM 3.0 VIZZ', 105000, NULL, 1),
(330, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'VZ38 SAVER VIZZ', 0, 'CAR CHARGER VZ38 SAVER DUAL USB VIZZ', 64000, NULL, 1),
(331, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'VZCC02 VIZZ', 0, 'CAR CHARGER VZCC02 DUAL OUTPUT 4.8A VIZZ', 103500, NULL, 1),
(332, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'VZCC02Q VIZZ', 0, 'CAR CHARGER VZCC02Q 2.4A+QUICK CHARGER VIZZ', 120000, NULL, 1),
(333, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'VZCC03 VIZZ', 0, 'CAR CHARGER VZCC03 THREE OUTPUT 7.2A VIZZ', 127500, NULL, 1),
(334, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'VZ01 CARHOLDER VIZZ', 0, 'CAR HOLDER VZ01 VIZZ', 79500, NULL, 1),
(335, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'VZ02 CARHOLDER VIZZ', 0, 'CAR HOLDER VZ02 VIZZ', 109500, NULL, 1),
(336, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'VZ030 CHARGER VIZZ', 0, 'CHARGER VZ030 MOHAWK QUICK 3.0 VIZZ', 82500, NULL, 1),
(337, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'VZ118 CHARGER VIZZ', 0, 'CHARGER VZ118 POWER IQ 1PORT VIZZ', 50500, NULL, 1),
(338, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'VZ128 CHARGER VIZZ', 0, 'CHARGER VZ128 POWER IQ 2PORT VIZZ', 71500, NULL, 1),
(339, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'VZ138 CHARGER VIZZ', 0, 'CHARGER VZ138 POWER IQ 3PORT VIZZ', 97500, NULL, 1),
(340, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'VZ139 CHARGER VIZZ', 0, 'CHARGER VZ139 3PORT VIZZ', 150000, NULL, 1),
(341, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'VZ28 CHARGER VIZZ', 0, 'CHARGER VZ28 DUAL USB VIZZ', 68500, NULL, 1),
(342, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'VZ48 CHARGER VIZZ', 0, 'CHARGER VZ48 4PORT VIZZ', 150000, NULL, 1),
(343, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'VZ58 CHARGER VIZZ', 0, 'CHARGER VZ58 5PORT VIZZ', 217500, NULL, 1),
(344, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'EJ01 GOPAI', 0, 'GOPAI EJ01 HANDSFREE', 37500, NULL, 1),
(345, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'EJ03 GOPAI', 0, 'GOPAI EJ03 HANDSFREE', 34500, NULL, 1),
(346, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'EJ05 GOPAI', 0, 'GOPAI EJ05 HANDSFREE', 28500, NULL, 1),
(347, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'PB01 GOPAI', 0, 'GOPAI PB01 CHARGER DUAL USB MICRO', 60000, NULL, 1),
(348, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'PB02 GOPAI', 0, 'GOPAI PB02 CHARGER DUAL USB 2.1A IOS', 66000, NULL, 1),
(349, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'PB03 GOPAI', 0, 'GOPAI PB03 CHARGER DUAL USB 2.1A TYPE-C', 66000, NULL, 1),
(350, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'PE01 GOPAI', 0, 'GOPAI PE01 HANDSFREE CONCH', 46500, NULL, 1),
(351, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'PE03 GOPAI', 0, 'GOPAI PE03 HANDSFREE METAL EARBUDS', 106500, NULL, 1),
(352, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'PE04 GOPAI', 0, 'GOPAI PE04 HANDSFREE METAL EARBUDS', 57000, NULL, 1),
(353, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'PE06 GOPAI', 0, 'GOPAI PE06 HANDSFREE FASHION', 49500, NULL, 1),
(354, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'PS05 GOPAI', 0, 'GOPAI PS05 ZINC ALLOY DATA CHARGING LINE 1.2M MICRO', 54000, NULL, 1),
(355, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'PS06 GOPAI', 0, 'GOPAI PS06 ZINC ALLOY DATA CHARGING LINE 1.2M IOS', 57000, NULL, 1),
(356, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'PS07 GOPAI', 0, 'GOPAI PS07 ZINC ALLOY DATA CHARGING LINE 1.2M TYPE C', 57000, NULL, 1),
(357, '2018-05-12 13:03:50', 'rifqi', NULL, NULL, b'1', 'PS08 GOPAI', 0, 'GOPAI PS08 MECHANICAL DATA LINE MICRO', 57000, NULL, 1),
(358, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PS09 GOPAI', 0, 'GOPAI PS09 MECHANICAL DATA LINE IOS', 61500, NULL, 1),
(359, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PS10 GOPAI', 0, 'GOPAI PS10 2 IN 1 ZINC ALLOY USB CABLE', 81000, NULL, 1),
(360, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PS11 GOPAI', 0, 'GOPAI PS11 FAST DATA CABLE 2A MICRO', 17500, NULL, 1),
(361, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PS12 GOPAI', 0, 'GOPAI PS12 FAST DATA CABLE 2A IOS', 22000, NULL, 1),
(362, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PS13 GOPAI', 0, 'GOPAI PS13 FAST DATA CABLE 2A TYPE C', 22000, NULL, 1),
(363, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PS15 GOPAI', 0, 'GOPAI PS15 ALUMINIUM ALLOY BRAIDED USB DATA CABLE 1.2M MICRO', 30000, NULL, 1),
(364, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PS20 GOPAI', 0, 'GOPAI PS20 ZINC ALLOY LED CABLE TYPE C', 52500, NULL, 1),
(365, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PS21 GOPAI', 0, 'GOPAI PS21 ZINC ALLOY USB DATA CABLE 4A MICRO', 52500, NULL, 1),
(366, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PS23 GOPAI', 0, 'GOPAI PS23 ZINC ALLOY USB DATA CABLE 4A TYPE C', 57000, NULL, 1),
(367, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PS27 GOPAI', 0, 'GOPAI PS27 DATA CABLE 7 SHAPE USB MICRO', 20500, NULL, 1),
(368, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PS28 GOPAI', 0, 'GOPAI PS28 DATA CABLE 7 SHAPE USB IOS', 24000, NULL, 1),
(369, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PS29 GOPAI', 0, 'GOPAI PS29 DATA CABLE 7 SHAPE USB TYPE C', 24000, NULL, 1),
(370, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PS30 GOPAI', 0, 'GOPAI PS30 ALUMINIUM ALLOY USB DATA CABLE 30CM MICRO', 19500, NULL, 1),
(371, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PS31 GOPAI', 0, 'GOPAI PS31 ALUMINIUM ALLOY USB DATA CABLE 30CM IOS', 25000, NULL, 1),
(372, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PS32 GOPAI', 0, 'GOPAI PS32 ALUMINIUM ALLOY USB DATA CABLE 30CM TYPE C', 25000, NULL, 1),
(373, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PT04 GOPAI', 0, 'GOPAI PT04 DUAL USB CHARGER', 48000, NULL, 1),
(374, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PY1000 PB GOPAI', 0, 'GOPAI PY1000 ALUMINIUM ALLOY DUAL USB 2.1A POWER BANK 16800mah', 247500, NULL, 1),
(375, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PY200 PB GOPAI', 0, 'GOPAI PY200 POWER BANK DUAL USB CATS EYE 10000mah', 210000, NULL, 1),
(376, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PY300 PB GOPAI', 0, 'GOPAI PY300 POWER BANK 10000mah', 187500, NULL, 1),
(377, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PY700 PB GOPAI', 0, 'GOPAI PY700 A CLASS POLYMER POWER BANK 12000mah', 217500, NULL, 1),
(378, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PY800 PB GOPAI', 0, 'GOPAI PY800 POWER BANK 10000mah', 150000, NULL, 1),
(379, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PY900 PB GOPAI', 0, 'GOPAI PY900 DIGITAL DISPLAY POWER BANK 12000mah', 210000, NULL, 1),
(380, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PZ02 GOPAI', 0, 'GOPAI PZ02 DUAL USB CAR CHARGER 2.4A', 49500, NULL, 1),
(381, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PZ03 GOPAI', 0, 'GOPAI PZ03 DUAL USB FAST CAR CHARGER', 57000, NULL, 1),
(382, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PZ08 GOPAI', 0, 'GOPAI PZ08 ALUMINIUM ALLOY DUAL USB CAR CHARGER FLASH 2.4A', 54000, NULL, 1),
(383, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'PZ09 GOPAI', 0, 'GOPAI PZ09 CAR CHARGER CIGARETTE LIGHTER', 49500, NULL, 1),
(384, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'TC03 GOPAI', 0, 'GOPAI TC03 DUAL USB CAR CHARGER 2.1A', 37500, NULL, 1),
(385, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'TC07 GOPAI', 0, 'GOPAI TC07 ALUMINIUM ALLOY DUAL USB CAR CHARGER 2.4A', 54000, NULL, 1),
(386, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'X16 GOPAI', 0, 'GOPAI X16 HIGH END DATA LINE COPPER WIRE CORE 1.2M MICRO', 39000, NULL, 1),
(387, '2018-05-12 13:03:51', 'rifqi', NULL, NULL, b'1', 'X17 GOPAI', 0, 'GOPAI X17 HIGH END DATA LINE COPPER WIRE CORE 1.2M IOS', 39000, NULL, 1),
(388, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'X18 GOPAI', 0, 'GOPAI X18 HIGH END DATA LINE COPPER WIRE CORE 1.2M TYPE C', 39000, NULL, 1),
(389, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'X19 GOPAI', 0, 'GOPAI X19 VOOC QUICK CHARGER DATA LINE', 49500, NULL, 1),
(390, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'X20 GOPAI', 0, 'GOPAI X20 QUICK CHARGER CABLE MICRO', 44500, NULL, 1),
(391, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'X21 GOPAI', 0, 'GOPAI X21 QUICK CHARGER CABLE TYPE C', 49000, NULL, 1),
(392, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'X4 GOPAI', 0, 'GOPAI X4 METAL DATA LINE MICRO 2M', 43500, NULL, 1),
(393, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'YP01 GOPAI', 0, 'GOPAI YP01 AUX AUDIO CABLE 1.5M', 30000, NULL, 1),
(394, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'B110 HEADSET VIZZ', 0, 'HF B110 HEADSET VIZZ ', 94500, NULL, 1),
(395, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'M1 HANDSFREE VIZZ', 0, 'HF M1 VIZZ', 36000, NULL, 1),
(396, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'M1+ HANDSFREE VIZZ', 0, 'HF M1+ VIZZ', 30000, NULL, 1),
(397, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'M2 HANDSFREE VIZZ', 0, 'HF M2 VIZZ', 83500, NULL, 1),
(398, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'M3 HANDSFREE VIZZ', 0, 'HF M3 VIZZ', 88000, NULL, 1),
(399, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'M3+ HANDSFREE VIZZ', 0, 'HF M3+ VIZZ', 93000, NULL, 1),
(400, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'KWI4.100 VIZZ', 0, 'KABEL WARNA TOPLES IOS IP-4 1M VIZZ', 10000, NULL, 1),
(401, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'KWI5 NEW', 0, 'KABEL WARNA TOPLES IOS IP-5 1M NEW VIZZ', 13500, NULL, 1),
(402, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'KW-MC1M VIZZ', 0, 'KABEL WARNA TOPLES MICRO 1M NEW VIZZ', 10000, NULL, 1),
(403, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'KW-MC30 VIZZ', 0, 'KABEL WARNA TOPLES MICRO 30CM VIZZ ', 8000, NULL, 1),
(404, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'VZ5000 PB AXEL VIZZ', 0, 'PB AXEL VZ5000 5000MAH VIZZ', 81000, NULL, 1),
(405, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'VZ6800 PB CLEON VIZZ', 0, 'PB CLEON VZ6800 6000MAH VIZZ', 172500, NULL, 1),
(406, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'VZ60 PB COL FUL VIZZ', 0, 'PB COLOURFULL VZ60 6000MAH VIZZ', 129000, NULL, 1),
(407, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'PB-01 DEVON VIZZ', 0, 'PB DEVON PB-01 6000MAH VIZZ', 185000, NULL, 1),
(408, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'VZ50 PB FELO VIZZ', 0, 'PB FELO VZ50 5000MAH VIZZ', 100000, NULL, 1),
(409, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'VZ11600 PB LYRA VIZZ', 0, 'PB LYRA VZ11600 11000MAH VIZZ', 289500, NULL, 1),
(410, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'PB-02 MAXEL VIZZ', 0, 'PB MAXEL PB02 13200MAH VIZZ', 225000, NULL, 1),
(411, '2018-05-12 13:03:52', 'rifqi', NULL, NULL, b'1', 'PB-03 MAXIMUS VIZZ', 0, 'PB MAXIMUS PB03 17600MAH VIZZ', 300000, NULL, 1),
(412, '2018-05-12 13:03:53', 'rifqi', NULL, NULL, b'1', 'MEZZO PB9000 VIZZ', 0, 'PB MEZZO 9000MAH VIZZ', 243000, NULL, 1),
(413, '2018-05-12 13:03:53', 'rifqi', NULL, NULL, b'1', 'VZ6000 PB NAVI VIZZ', 0, 'PB NAVI VZ6000 6000MAH VIZZ', 181500, NULL, 1),
(414, '2018-05-12 13:03:53', 'rifqi', NULL, NULL, b'1', 'VZ6600 PBPOCKET VIZZ', 0, 'PB POCKET VZ6600 6000MAH VIZZ', 100000, NULL, 1),
(415, '2018-05-12 13:03:53', 'rifqi', NULL, NULL, b'1', 'VZ11800 PBQUICK VIZZ', 0, 'PB QUICK VZ11800 11000MAH VIZZ', 334500, NULL, 1),
(416, '2018-05-12 13:03:53', 'rifqi', NULL, NULL, b'1', 'VZ8800 PB VEGA VIZZ', 0, 'PB VEGA VZ8800 8000MAH VIZZ', 219000, NULL, 1),
(417, '2018-05-12 13:03:53', 'rifqi', NULL, NULL, b'1', 'VITUS PB11000 VIZZ', 0, 'PB VITUS 11000MAH VIZZ', 277500, NULL, 1),
(418, '2018-05-12 13:03:53', 'rifqi', NULL, NULL, b'1', 'VZ1200 PB12000 VIZZ', 0, 'PB VZ1200 12000MAH VIZZ', 217500, NULL, 1),
(419, '2018-05-12 13:03:53', 'rifqi', NULL, NULL, b'1', 'VZ8000 PB7200 VIZZ', 0, 'PB VZ8000 7200MAH VIZZ', 142500, NULL, 1),
(420, '2018-05-12 13:03:53', 'rifqi', NULL, NULL, b'1', 'VZ880 PB11000 VIZZ', 0, 'PB VZ880 11000MAH VIZZ', 195000, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_product_category`
--

CREATE TABLE IF NOT EXISTS `m_product_category` (
  `m_product_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedUser` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`m_product_category_id`),
  UNIQUE KEY `idx_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_promo`
--

CREATE TABLE IF NOT EXISTS `m_promo` (
  `m_promo_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedUser` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `multiple` bit(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`m_promo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_promoline`
--

CREATE TABLE IF NOT EXISTS `m_promoline` (
  `m_promoline_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedUser` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `amount` double DEFAULT NULL,
  `free` bit(1) NOT NULL,
  `productpromo` bit(1) DEFAULT NULL,
  `qty` double DEFAULT NULL,
  `m_product_id` int(11) DEFAULT NULL,
  `m_promo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`m_promoline_id`),
  KEY `FK_4jkp1g9lehifwptpil86476ng` (`m_product_id`),
  KEY `FK_dqgayx9k0uhhesd355w5u0okw` (`m_promo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_promo_manage`
--

CREATE TABLE IF NOT EXISTS `m_promo_manage` (
  `m_promomanage_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedUser` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `byhour` bit(1) DEFAULT NULL,
  `dateeffective` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `timefrom` datetime DEFAULT NULL,
  `timeto` datetime DEFAULT NULL,
  PRIMARY KEY (`m_promomanage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_promo_manageline`
--

CREATE TABLE IF NOT EXISTS `m_promo_manageline` (
  `m_promo_manageline_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedUser` varchar(255) DEFAULT NULL,
  `m_promo_id` int(11) DEFAULT NULL,
  `m_promo_manage_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`m_promo_manageline_id`),
  KEY `FK_boy8vg2xeitk86ida0uc87axr` (`m_promo_id`),
  KEY `FK_frfvvy5qfk8m3ct4eutneff9k` (`m_promo_manage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_role`
--

CREATE TABLE IF NOT EXISTS `m_role` (
  `m_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedUser` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`m_role_id`),
  UNIQUE KEY `idx_name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `m_role`
--

INSERT INTO `m_role` (`m_role_id`, `created`, `createdUser`, `updated`, `updatedUser`, `active`, `description`, `name`) VALUES
(1, '2018-05-12 11:37:10', '', NULL, NULL, b'1', 'ROLE ROOT', 'ROOT'),
(2, '2018-05-12 11:37:10', '', NULL, NULL, b'1', 'ROLE ADMIN', 'ADMIN'),
(3, '2018-05-12 11:37:10', '', NULL, NULL, b'1', 'ROLE POS', 'POS');

-- --------------------------------------------------------

--
-- Table structure for table `m_unit`
--

CREATE TABLE IF NOT EXISTS `m_unit` (
  `m_unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedUser` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`m_unit_id`),
  UNIQUE KEY `idx_name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `m_unit`
--

INSERT INTO `m_unit` (`m_unit_id`, `created`, `createdUser`, `updated`, `updatedUser`, `active`, `description`, `name`) VALUES
(1, '2018-05-12 11:40:15', 'rifqi', NULL, NULL, b'1', 'satuan', 'pcs');

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE IF NOT EXISTS `m_user` (
  `m_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `createdUser` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedUser` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `m_role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`m_user_id`),
  UNIQUE KEY `idx_name` (`name`),
  KEY `FK_c2sxpx9rwwgykv3fkpup1w87b` (`m_role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`m_user_id`, `created`, `createdUser`, `updated`, `updatedUser`, `active`, `name`, `password`, `m_role_id`) VALUES
(1, '2018-05-12 11:37:10', '', NULL, NULL, b'1', 'rifqi', 'rifqi10', 1),
(2, '2018-05-12 11:37:10', '', NULL, NULL, b'1', 'admin', 'admin', 2),
(3, '2018-05-12 11:37:10', '', NULL, NULL, b'1', 'pos', 'pos', 3);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `m_configuration`
--
ALTER TABLE `m_configuration`
  ADD CONSTRAINT `FK_9e99ihbhnnblf9af9s21949e8` FOREIGN KEY (`m_locator_id`) REFERENCES `m_locator` (`m_locator_id`);

--
-- Constraints for table `m_order`
--
ALTER TABLE `m_order`
  ADD CONSTRAINT `FK_47cumci2tuxm3lb366jpe735n` FOREIGN KEY (`m_cashier_id`) REFERENCES `m_user` (`m_user_id`);

--
-- Constraints for table `m_orderline`
--
ALTER TABLE `m_orderline`
  ADD CONSTRAINT `FK_bi92smsyqshivduk3fneuxycy` FOREIGN KEY (`m_product_id`) REFERENCES `m_product` (`m_product_id`),
  ADD CONSTRAINT `FK_fu8lu6lfkblxk6305avq93fgr` FOREIGN KEY (`m_order_id`) REFERENCES `m_order` (`m_order_id`);

--
-- Constraints for table `m_orderpayment`
--
ALTER TABLE `m_orderpayment`
  ADD CONSTRAINT `FK_2f5nnmvw6vqvvyu17s50b3upj` FOREIGN KEY (`m_edc_id`) REFERENCES `m_edc` (`m_edc_id`),
  ADD CONSTRAINT `FK_qg2spuy9m2aj0kuuyybbmppok` FOREIGN KEY (`m_order_id`) REFERENCES `m_order` (`m_order_id`);

--
-- Constraints for table `m_privilege`
--
ALTER TABLE `m_privilege`
  ADD CONSTRAINT `FK_n7w6enhghdkkcc7p5h6pgq899` FOREIGN KEY (`m_menu_id`) REFERENCES `m_menu` (`m_menu_id`),
  ADD CONSTRAINT `FK_s7xi3rx60u51tqpia7045lmnx` FOREIGN KEY (`m_role_id`) REFERENCES `m_role` (`m_role_id`);

--
-- Constraints for table `m_product`
--
ALTER TABLE `m_product`
  ADD CONSTRAINT `FK_6jjiwejficd3ql5na6w41wyov` FOREIGN KEY (`m_product_category_id`) REFERENCES `m_product_category` (`m_product_category_id`),
  ADD CONSTRAINT `FK_jajtk333ol6ruuv0ngl7aknnn` FOREIGN KEY (`m_unit_id`) REFERENCES `m_unit` (`m_unit_id`);

--
-- Constraints for table `m_promoline`
--
ALTER TABLE `m_promoline`
  ADD CONSTRAINT `FK_4jkp1g9lehifwptpil86476ng` FOREIGN KEY (`m_product_id`) REFERENCES `m_product` (`m_product_id`),
  ADD CONSTRAINT `FK_dqgayx9k0uhhesd355w5u0okw` FOREIGN KEY (`m_promo_id`) REFERENCES `m_promo` (`m_promo_id`);

--
-- Constraints for table `m_promo_manageline`
--
ALTER TABLE `m_promo_manageline`
  ADD CONSTRAINT `FK_boy8vg2xeitk86ida0uc87axr` FOREIGN KEY (`m_promo_id`) REFERENCES `m_promo` (`m_promo_id`),
  ADD CONSTRAINT `FK_frfvvy5qfk8m3ct4eutneff9k` FOREIGN KEY (`m_promo_manage_id`) REFERENCES `m_promo_manage` (`m_promomanage_id`);

--
-- Constraints for table `m_user`
--
ALTER TABLE `m_user`
  ADD CONSTRAINT `FK_c2sxpx9rwwgykv3fkpup1w87b` FOREIGN KEY (`m_role_id`) REFERENCES `m_role` (`m_role_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
