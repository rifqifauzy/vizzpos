package pos.vizzpos.utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public final class DBConnection {

	public static String DB_URL = "db.url";
	public static String DB_USER = "db.user";
	public static String DB_PASSWORD = "db.password";
	public static String DB_DRIVER = "db.driver";
	public static String DB_DATABASE = "db.database";
	
	public static Connection getConnection() {
		Connection dbConnection = null;
		
		try {
			CodeSource codeSource = DBConnection.class.getProtectionDomain().getCodeSource();
	    	String path = codeSource.getLocation().toURI().getPath();
	    	
			Properties props = new Properties();
			FileInputStream fis = null;
			
			fis = new FileInputStream(path.concat("/database.properties"));
			props.load(fis);
			
			try {
				Class.forName(props.getProperty(DB_DRIVER));
			} catch (ClassNotFoundException e) {
				System.out.println(e.getMessage());
			}
			
			try {
				String url = props.getProperty(DB_URL).concat(props.getProperty(DB_DATABASE));
				String user = props.getProperty(DB_USER);
				String password = props.getProperty(DB_PASSWORD);
				
				dbConnection = DriverManager.getConnection(url, user, password);
				return dbConnection;
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		}
		
		return dbConnection;
	}
	
	public static Connection getConnectionStatic() {
		Connection dbConnection = null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		try {
			String url = "jdbc:mysql://localhost:3306/vizzpos";
			String user = "root";
			String password = "";
			
			dbConnection = DriverManager.getConnection(url, user, password);
			return dbConnection;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return dbConnection;
	}
}
