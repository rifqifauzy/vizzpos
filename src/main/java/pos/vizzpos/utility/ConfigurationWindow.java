package pos.vizzpos.utility;

import org.zkoss.lang.Strings;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import pos.vizzpos.CoreWindow;
import pos.vizzpos.entity.Configuration;
import pos.vizzpos.repository.ConfigurationRepository;

@SuppressWarnings("serial")
public class ConfigurationWindow extends CoreWindow {

	@Wire private Combobox cbLocator;
	@Wire private Intbox intSeq;
	@Wire private Textbox txtPrefix;
	@Wire private Checkbox chkDirectPrint;
	@Wire private Grid gridForm;
	
	@WireVariable private ConfigurationRepository configurationRepository;
	
	private Configuration configuration;
	
	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		configuration = configurationRepository.count() > 0 ? configurationRepository.findAll().get(0) : null;
		getConfiguration();
	}

	@Listen("onClick = #btnSimpan")
	public void simpan() {
		if (validasi()) {
			setConfiguration();
			configurationRepository.save(configuration);
			showMessage("Data disimpan");
		}
	}
	
	@Listen("onClick = #btnBatal")
	public void batal() {
		getConfiguration();
	}
	
	private void setConfiguration() {
		if (configuration == null)
			configuration = new Configuration();
		
		configuration.setLocator(cbLocator.getSelectedItem().getValue());
		configuration.setPrefix(txtPrefix.getValue());
		configuration.setSequence(intSeq.getValue());
		configuration.setDirectPrint(chkDirectPrint.isChecked());
	}
	
	private void getConfiguration() {
		if (configuration == null) {
			clearForm(gridForm);
		} else {
			cbLocator.setValue(configuration.getLocator().getName());
			intSeq.setValue(configuration.getSequence());
			txtPrefix.setValue(configuration.getPrefix());
			chkDirectPrint.setChecked(configuration.isDirectPrint());
		}
	}
	
	private boolean validasi() {
		if (cbLocator.getSelectedIndex() < 0) {
			showMessage("Locator belum dipilih");
			return false;
		}
		
		if (intSeq.getValue() == null) {
			showMessage("Sequence masih kosong");
			return false;
		}
		
		if (Strings.isBlank(txtPrefix.getValue())) {
			showMessage("Prefix masih kosong");
			return false;
		}
		
		return true;
	}
	
}
