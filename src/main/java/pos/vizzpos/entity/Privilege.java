package pos.vizzpos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="m_privilege")
public class Privilege extends CoreEntity {
	
	@Id
	@Column(name="m_privilege_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer privilegeId;
	
	@ManyToOne
	@JoinColumn(name = "m_role_id")
	private Role role;
	
	@ManyToOne
	@JoinColumn(name = "m_menu_id")
	private Menu menu;
	
	private boolean active;
	
	public Privilege() {
		super();
	}
	
	public Privilege(Role role, Menu menu, boolean active) {
		super();
		this.role = role;
		this.menu = menu;
		this.active = active;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}