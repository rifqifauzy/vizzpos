package pos.vizzpos.entity;

import java.util.Calendar;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;

import pos.vizzpos.utils.Constans;

@MappedSuperclass
public abstract class CoreEntity {

	private Calendar updated;
	private String updatedUser;
	private Calendar created;
	private String createdUser;

	public Calendar getUpdated() {
		return updated;
	}

	public void setUpdated(Calendar updated) {
		this.updated = updated;
	}

	public String getUpdateUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Calendar getCreated() {
		return created;
	}

	public void setCreated(Calendar created) {
		this.created = created;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	@PreUpdate
	public void updateAuditInfo() {
		setUpdated(Calendar.getInstance());
		setUpdatedUser(getUser());
	}
	
	@PrePersist
	public void updateCreatorInfo() {
		if(getCreated() == null) {
			setCreated(Calendar.getInstance());
			setCreatedUser(getUser());
		}
	}
	
	private String getUser() {
		Execution ex = Executions.getCurrent();
		Session s = ex.getSession();
		
		User user = (User)s.getAttribute(Constans.USER_LOGIN);
		if (user != null)
			return user.getName();
		else
			return Constans.EMPTY_STRING;
	}
}
