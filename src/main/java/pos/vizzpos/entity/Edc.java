package pos.vizzpos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name="m_edc", indexes={
		@Index(columnList="name", unique=true, name="idx_name")
})
public class Edc extends CoreEntity {

	@Id
	@Column(name="m_edc_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer edcId;
	
	@Column(nullable = false)
	private String name;
	
	@Column(name = "minamount", nullable = false)
	private Double minAmount = 0d;
	
	private String description, code;
	
	private boolean active;
	
	public Edc() {
		super();
	}

	public Edc(String name, Double minAmount, String description, String code, boolean active) {
		super();
		this.name = name;
		this.minAmount = minAmount;
		this.description = description;
		this.code = code;
		this.active = active;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(Double minAmount) {
		this.minAmount = minAmount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Integer getEdcId() {
		return edcId;
	}
	
}
