package pos.vizzpos.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import pos.vizzpos.repository.ConfigurationRepository;
import pos.vizzpos.utils.Utility;

@Entity
@Table(name="m_configuration")
public class Configuration extends CoreEntity {

	@Id
	@Column(name="m_configuration_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer configurationId;
	
	@ManyToOne
	@JoinColumn(name = "m_locator_id")
	private Locator locator;
	
	private int sequence;
	
	@Column(length = 5)
	private String prefix;
	
	@Column(name="directprint")
	private boolean directPrint;
	
	public Locator getLocator() {
		return locator;
	}

	public void setLocator(Locator locator) {
		this.locator = locator;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public Integer getConfigurationId() {
		return configurationId;
	}
	
	public boolean isDirectPrint() {
		return directPrint;
	}

	public void setDirectPrint(boolean directPrint) {
		this.directPrint = directPrint;
	}

	public String getDocumentNo(ConfigurationRepository configurationRepository, Date dateOrdered) {
		Integer count = configurationRepository.findCountOrderByDay(Utility.startOfDay(dateOrdered),
				Utility.endOfDay(dateOrdered));
		setSequence(count + 1);		
		configurationRepository.save(this);
		
		StringBuilder docNo = new StringBuilder();
		docNo.append(getPrefix() != null ? getPrefix() : "");
		docNo.append(locator.getCode());
		docNo.append(Utility.sdf.format(dateOrdered));
		docNo.append(Utility.dfDocument.format(sequence));
		
		return docNo.toString();
	}
}
