package pos.vizzpos.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "m_transaction")
public class Transaction extends CoreEntity {

	@Id
	@Column(name="m_transaction_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer transactionId;
	
	@ManyToOne
	@JoinColumn(name = "m_orderline_id")
	private OrderLine orderLine;
	
	@ManyToOne
	@JoinColumn(name = "m_product_id")
	private Product product;
	
	@ManyToOne
	@JoinColumn(name = "m_locator_id")
	private Locator locator;
	
	@Column(name = "movementdate")
	private Date movementDate;
	
	private Double qty;
	private boolean active;

	public OrderLine getOrderLine() {
		return orderLine;
	}

	public void setOrderLine(OrderLine orderLine) {
		this.orderLine = orderLine;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Integer getTransactionId() {
		return transactionId;
	}

	public Date getMovementDate() {
		return movementDate;
	}

	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	public Locator getLocator() {
		return locator;
	}

	public void setLocator(Locator locator) {
		this.locator = locator;
	}
}
