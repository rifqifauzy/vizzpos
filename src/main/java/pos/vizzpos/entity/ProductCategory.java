package pos.vizzpos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name="m_product_category", indexes={
		@Index(columnList="name", unique=true, name="idx_name")
})
public class ProductCategory extends CoreEntity {

	@Id
	@Column(name="m_product_category_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer productCategoryId;
	
	@Column(nullable = false)
	private String name;
	
	private String description;
	
	private boolean active;

	public ProductCategory() {
		
	}
	
	public ProductCategory(String name, String description, boolean active) {
		super();
		this.name = name;
		this.description = description;
		this.active = active;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getProductCategoryId() {
		return productCategoryId;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
}
