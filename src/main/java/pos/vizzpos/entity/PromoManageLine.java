package pos.vizzpos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="m_promo_manageline")
public class PromoManageLine extends CoreEntity {

	@Id
	@Column(name="m_promo_manageline_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer promoManageLineId;
	
	@ManyToOne
	@JoinColumn(name = "m_promo_manage_id")
	private PromoManage promoManage;
	
	@ManyToOne
	@JoinColumn(name = "m_promo_id")
	private Promo promo;

	public PromoManage getPromoManage() {
		return promoManage;
	}

	public void setPromoManage(PromoManage promoManage) {
		this.promoManage = promoManage;
	}

	public Promo getPromo() {
		return promo;
	}

	public void setPromo(Promo promo) {
		this.promo = promo;
	}

	public Integer getPromoManageLineId() {
		return promoManageLineId;
	}
}
