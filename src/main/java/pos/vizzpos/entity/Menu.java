package pos.vizzpos.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "m_menu",indexes={
		@Index(columnList="name", unique=false, name="idx_name"), 
		@Index(columnList="code", unique=false, name="idx_code")
})
public class Menu extends CoreEntity {
	
	@Id
	@Column(name="m_menu_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer menuId;
	
	@Column(nullable = false)
	private String code;
	
	private String parentCode, name, icon, zulpath;
	private Integer level;
	private boolean menuitem, active, menubar;
	
	@Transient
	private List<Menu> childs = new ArrayList<>();
	
	public Menu() {
		super();
	}
	
	public Menu(String code, String parentCode, String name, String icon, String zulpath, Integer level,
			boolean menuitem, boolean active, boolean menubar) {
		super();
		this.code = code;
		this.parentCode = parentCode;
		this.name = name;
		this.icon = icon;
		this.zulpath = zulpath;
		this.level = level;
		this.menuitem = menuitem;
		this.active = active;
		this.menubar = menubar;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getZulpath() {
		return zulpath;
	}

	public void setZulpath(String zulpath) {
		this.zulpath = zulpath;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public boolean isMenuitem() {
		return menuitem;
	}

	public void setMenuitem(boolean menuitem) {
		this.menuitem = menuitem;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isMenubar() {
		return menubar;
	}

	public void setMenubar(boolean menubar) {
		this.menubar = menubar;
	}

	public List<Menu> getChilds() {
		return childs;
	}

	public void setChilds(List<Menu> childs) {
		this.childs = childs;
	}

	public Integer getMenuId() {
		return menuId;
	}
}