package pos.vizzpos.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "m_inout",indexes={
		@Index(columnList="documentno", unique=false, name="idx_documentno")
})
public class InOut extends CoreEntity {

	@Id
	@Column(name="m_inout_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer inOutId;
	
	@Column(nullable = false, length = 40)
	private String documentno;
	
	@Column(name = "movementdate")
	private Date movementDate;
	
	@ManyToOne
	@JoinColumn(name = "m_locator_id")
	private Locator locator;
	
	@Column(name = "no_sj")
	private String noSJ;
	
	private String description, type;

	public String getDocumentno() {
		return documentno;
	}

	public void setDocumentno(String documentno) {
		this.documentno = documentno;
	}

	public Date getMovementDate() {
		return movementDate;
	}

	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	public Locator getLocator() {
		return locator;
	}

	public void setLocator(Locator locator) {
		this.locator = locator;
	}

	public String getNoSJ() {
		return noSJ;
	}

	public void setNoSJ(String noSJ) {
		this.noSJ = noSJ;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getInOutId() {
		return inOutId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
