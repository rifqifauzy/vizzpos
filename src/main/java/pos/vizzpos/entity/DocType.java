package pos.vizzpos.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import pos.vizzpos.repository.DocTypeRepository;
import pos.vizzpos.utils.Utility;

@Entity
@Table(name="m_doctype")
public class DocType extends CoreEntity {

	@Id
	@Column(name="m_doctype_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer docTypeId;
	
	private int sequence;
	
	@Column(length = 5)
	private String prefix;
	
	private String name, type;
	
	public int getSequence() {
		return sequence;
	}

	public Integer getDocTypeId() {
		return docTypeId;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDocumentNo(DocTypeRepository docTypeRepository, Date movementDate, String type) {
		Integer count = docTypeRepository.findCountInOutByDay(Utility.startOfDay(movementDate),
				Utility.endOfDay(movementDate), type);
		setSequence(count + 1);	
		docTypeRepository.save(this);
		
		StringBuilder docNo = new StringBuilder();
		docNo.append(getPrefix() != null ? getPrefix() : "");
		docNo.append(Utility.sdf.format(movementDate));
		docNo.append(Utility.dfDocument.format(sequence));
		
		return docNo.toString();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
