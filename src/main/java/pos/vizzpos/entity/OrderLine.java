package pos.vizzpos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "m_orderline")
public class OrderLine extends CoreEntity {

	@Id
	@Column(name="m_orderline_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer orderLineId;
	
	@ManyToOne
	@JoinColumn(name = "m_order_id")
	private Order order;
	
	@ManyToOne
	@JoinColumn(name = "m_product_id")
	private Product product;
	
	@Column(name = "discountamount", nullable = false)
	private Double discountAmount = 0d;
	
	@Column(name = "discountpercent", nullable = false)
	private Double discountPercent = 0d;
	
	private Integer guarantee;
	private Double qty, price;
	private boolean locked = false;
	
	@Column(name = "productpromo")
	private boolean productPromo;
	
	@Column(name = "typepromo")
	private String typePromo;
	
	@ManyToOne
	@JoinColumn(name = "m_product_free_id")
	private Product productFree;
	
	@Column(name = "qtyfree")
	private Double qtyFree;
	
	public boolean isProductPromo() {
		return productPromo;
	}
	public void setProductPromo(boolean productPromo) {
		this.productPromo = productPromo;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Double getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public Double getDiscountPercent() {
		return discountPercent;
	}
	public void setDiscountPercent(Double discountPercent) {
		this.discountPercent = discountPercent;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public boolean isLocked() {
		return locked;
	}
	public void setLocked(boolean locked) {
		this.locked = locked;
	}
	public Integer getOrderLineId() {
		return orderLineId;
	}
	public Integer getGuarantee() {
		return guarantee;
	}
	public void setGuarantee(Integer guarantee) {
		this.guarantee = guarantee;
	}
	public String getTypePromo() {
		return typePromo;
	}
	public void setTypePromo(String typePromo) {
		this.typePromo = typePromo;
	}
	public Double getTotalPrice() {
		return getTotalPriceOf(qty);
	}
	public Product getProductFree() {
		return productFree;
	}
	public void setProductFree(Product productFree) {
		this.productFree = productFree;
	}
	public Double getQtyFree() {
		return qtyFree;
	}
	public void setQtyFree(Double qtyFree) {
		this.qtyFree = qtyFree;
	}
	public Double getTotalPriceOf(double qty) {
		double newPrice = price - (discountPercent/100.0 * price);
		
		newPrice -= (discountPercent/100.0 * newPrice);
		
		newPrice -= discountAmount;
		
		return qty * newPrice;
	}
	public Double getDiscount(double qty) {
		double disc = discountPercent/100.0 * price;
		
		disc += discountAmount;
		
		return qty * disc;
	}
}
