package pos.vizzpos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name="m_role", indexes={
		@Index(columnList="name", unique=true, name="idx_name")
})
public class Role extends CoreEntity {
	
	@Id
	@Column(name="m_role_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer roleId;
	
	private String name, description;
	
	private boolean active;
	
	public Role() {
		super();
	}

	public Role(String nama, String keterangan, boolean active) {
		super();
		this.name = nama;
		this.description = keterangan;
		this.active = active;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Integer getRoleId() {
		return roleId;
	}

}