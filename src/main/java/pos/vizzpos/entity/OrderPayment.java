package pos.vizzpos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "m_orderpayment")
public class OrderPayment extends CoreEntity {

	@Id
	@Column(name="m_orderpayment_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer orderPaymentId;
	
	@ManyToOne
	@JoinColumn(name = "m_order_id")
	private Order order;
	
	@ManyToOne
	@JoinColumn(name = "m_edc_id")
	private Edc edc;
	
	@Column(name = "paymentamount", nullable = false)
	private Double paymentAmount;
	
	@Column(name = "paymenttype", nullable = false, length = 1)
	private String paymentType;
	
	@Column(name = "cardno")
	private String cardNo;
	
	@Column(name = "cardname")
	private String cardName;

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Edc getEdc() {
		return edc;
	}

	public void setEdc(Edc edc) {
		this.edc = edc;
	}

	public Double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public Integer getOrderPaymentId() {
		return orderPaymentId;
	}
}
