package pos.vizzpos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name="m_locator", indexes={
		@Index(columnList="code", unique=true, name="idx_code")
})
public class Locator extends CoreEntity {

	@Id
	@Column(name="m_locator_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer locatorId;
	
	@Column(nullable = false, length = 7)
	private String code;
	
	@Column(nullable = false)
	private String name;
	
	private String description, address;
	
	private boolean active;

	public Locator() {
		
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Integer getLocatorId() {
		return locatorId;
	}
	
}
