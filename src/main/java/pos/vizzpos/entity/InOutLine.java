package pos.vizzpos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "m_inoutline")
public class InOutLine extends CoreEntity {

	@Id
	@Column(name="m_inoutline_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer inOutLineId;
	
	@ManyToOne
	@JoinColumn(name = "m_inout_id")
	private InOut inOut;
	
	@ManyToOne
	@JoinColumn(name = "m_product_id")
	private Product product;
	
	private Double qty;

	public InOut getInOut() {
		return inOut;
	}

	public void setInOut(InOut inOut) {
		this.inOut = inOut;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Integer getInOutLineId() {
		return inOutLineId;
	}
	
}
