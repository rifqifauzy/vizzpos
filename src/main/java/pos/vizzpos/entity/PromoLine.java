package pos.vizzpos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="m_promoline")
public class PromoLine extends CoreEntity {

	@Id
	@Column(name="m_promoline_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer promoLineId;
	
	@ManyToOne
	@JoinColumn(name = "m_promo_id")
	private Promo promo;
	
	@ManyToOne
	@JoinColumn(name = "m_product_id")
	private Product product;
	
	private Double qty, amount;
	
	private boolean active, free;
	
	@Column(name = "productpromo")
	private boolean productPromo;

	@Column(name = "qtyfree")
	private Double qtyFree;
	
	@Column(name = "qtypay")
	private Double qtyPay;
	
	@ManyToOne
	@JoinColumn(name = "m_product_free_id")
	private Product productFree;
	
	public Promo getPromo() {
		return promo;
	}

	public void setPromo(Promo promo) {
		this.promo = promo;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Integer getPromoLineId() {
		return promoLineId;
	}

	public boolean isProductPromo() {
		return productPromo;
	}

	public void setProductPromo(boolean productPromo) {
		this.productPromo = productPromo;
	}

	public boolean isFree() {
		return free;
	}

	public void setFree(boolean free) {
		this.free = free;
	}

	public Double getQtyFree() {
		return qtyFree;
	}

	public void setQtyFree(Double qtyFree) {
		this.qtyFree = qtyFree;
	}

	public Double getQtyPay() {
		return qtyPay;
	}

	public void setQtyPay(Double qtyPay) {
		this.qtyPay = qtyPay;
	}

	public Product getProductFree() {
		return productFree;
	}

	public void setProductFree(Product productFree) {
		this.productFree = productFree;
	}
	
}
