package pos.vizzpos.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "m_order",indexes={
		@Index(columnList="documentno", unique=false, name="idx_documentno")
})
public class Order extends CoreEntity {

	@Id
	@Column(name="m_order_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer orderId;
	
	@Column(nullable = false, length = 40)
	private String documentno;
	
	@Column(name = "dateordered")
	private Date dateOrdered;
	
	@ManyToOne
	@JoinColumn(name = "m_cashier_id")
	private User cashier;
	
	@ManyToOne
	@JoinColumn(name = "m_locator_id")
	private Locator locator;
	
	@Column(name = "specialdiscount")
	private Double specialDiscount;
	
	@Column(name = "totalpayment")
	private Double totalPayment;
	
	@Column(name = "discountline")
	private Double discountLine;
	
	@Column(name = "changes")
	private Double change;
	
	private Double grandtotal, subtotal;
	private String notes;
	private boolean paid, voided, hold;
	
	public String getDocumentno() {
		return documentno;
	}
	public void setDocumentno(String documentno) {
		this.documentno = documentno;
	}
	public Date getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	public Double getSpecialDiscount() {
		return specialDiscount;
	}
	public void setSpecialDiscount(Double specialDiscount) {
		this.specialDiscount = specialDiscount;
	}
	public Double getTotalPayment() {
		return totalPayment;
	}
	public void setTotalPayment(Double totalPayment) {
		this.totalPayment = totalPayment;
	}
	public User getCashier() {
		return cashier;
	}
	public void setCashier(User cashier) {
		this.cashier = cashier;
	}
	public Double getGrandtotal() {
		return grandtotal;
	}
	public void setGrandtotal(Double grandtotal) {
		this.grandtotal = grandtotal;
	}
	public Double getChange() {
		return change;
	}
	public void setChange(Double change) {
		this.change = change;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public boolean isPaid() {
		return paid;
	}
	public void setPaid(boolean paid) {
		this.paid = paid;
	}
	public boolean isVoided() {
		return voided;
	}
	public void setVoided(boolean voided) {
		this.voided = voided;
	}
	public boolean isHold() {
		return hold;
	}
	public void setHold(boolean hold) {
		this.hold = hold;
	}
	public Integer getOrderId() {
		return orderId;
	}
	public Double getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}
	public Double getDiscountLine() {
		return discountLine;
	}
	public void setDiscountLine(Double discountLine) {
		this.discountLine = discountLine;
	}
	public Locator getLocator() {
		return locator;
	}
	public void setLocator(Locator locator) {
		this.locator = locator;
	}
}
