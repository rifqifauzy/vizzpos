package pos.vizzpos.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="m_promo_manage")
public class PromoManage extends CoreEntity {

	@Id
	@Column(name="m_promo_manage_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer promoManageId;
	
	@Column(nullable = false)
	private String name;
	
	@Column(name="dateeffective")
	private Date dateEffective;
	
	@Column(name="timefrom")
	private Date timeFrom;
	
	@Column(name="timeto")
	private Date timeTo;
	
	@Column(name="byhour")
	private boolean byHour;
	
	private boolean active;
	private String description;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDateEffective() {
		return dateEffective;
	}
	public void setDateEffective(Date dateEffective) {
		this.dateEffective = dateEffective;
	}
	public Date getTimeFrom() {
		return timeFrom;
	}
	public void setTimeFrom(Date timeFrom) {
		this.timeFrom = timeFrom;
	}
	public Date getTimeTo() {
		return timeTo;
	}
	public void setTimeTo(Date timeTo) {
		this.timeTo = timeTo;
	}
	public boolean isByHour() {
		return byHour;
	}
	public void setByHour(boolean byHour) {
		this.byHour = byHour;
	}
	public Integer getPromoManageId() {
		return promoManageId;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
}
