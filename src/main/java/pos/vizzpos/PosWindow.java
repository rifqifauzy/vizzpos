package pos.vizzpos;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.util.StringUtils;
import org.zkoss.lang.Strings;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.A;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import pos.vizzpos.bandbox.PendingOrderBandbox;
import pos.vizzpos.bandbox.ProductBandbox;
import pos.vizzpos.entity.Edc;
import pos.vizzpos.entity.Order;
import pos.vizzpos.entity.OrderLine;
import pos.vizzpos.entity.OrderPayment;
import pos.vizzpos.entity.Product;
import pos.vizzpos.entity.PromoLine;
import pos.vizzpos.entity.Transaction;
import pos.vizzpos.entity.User;
import pos.vizzpos.repository.EdcRepository;
import pos.vizzpos.repository.OrderLineRepository;
import pos.vizzpos.repository.OrderPaymentRepository;
import pos.vizzpos.repository.OrderRepository;
import pos.vizzpos.repository.ProductRepository;
import pos.vizzpos.repository.PromoLineRepository;
import pos.vizzpos.repository.PromoRepository;
import pos.vizzpos.repository.TransactionRepository;
import pos.vizzpos.repository.UserRepository;
import pos.vizzpos.utils.ComponentUtil;
import pos.vizzpos.utils.PaymentType;
import pos.vizzpos.utils.PromoType;
import pos.vizzpos.utils.RoleName;
import pos.vizzpos.utils.Utility;

@SuppressWarnings("serial")
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class PosWindow extends CoreWindow {
	
	@Wire private Textbox txtScanProduct, txtCardNumber, txtCardNo, txtNotes;
	@Wire private Label lblUser, lblSubtotal, lblDiscount, lblGrandtotal, lblSpecialDiscount, 
			lblTotalPayment, lblChange, lblCardNo, lblPromo;
	@Wire private Listbox listbox, listboxPayment;
	@Wire private Bandbox bndProduct, bndPending;
	@Wire private Button btnCash, btnCredit, btnDebit, btnSpecialDiscount, btnFinish, btnVoid, btnHold;
	@Wire private Doublebox dbAmount, dbSpecialDiscount;
	@Wire private Combobox cbEdc;
	@Wire private Hbox hboxAmount, hboxCard, hboxCardNo, hboxQuickCash, hboxLblQuick, hboxChange;
	@Wire private Vbox vboxSpecialDiscount;
	
	@Wire private Window winVoid;
	@Wire("#winVoid #txtUsernameSupervisor") private Textbox txtUsernameSupervisor;
	@Wire("#winVoid #txtPasswordSupervisor") private Textbox txtPasswordSupervisor;
	
	@WireVariable private ProductRepository productRepository;
	@WireVariable private OrderRepository orderRepository;
	@WireVariable private OrderLineRepository orderLineRepository;
	@WireVariable private OrderPaymentRepository orderPaymentRepository;
	@WireVariable private EdcRepository edcRepository;
	@WireVariable private PromoRepository promoRepository;
	@WireVariable private PromoLineRepository promoLineRepository;
	@WireVariable private TransactionRepository transactionRepository;
	@WireVariable private UserRepository userRepository;
	
	private User user;
	private Order order;
	private A aAddPayment = new A();
	private String paymenttype = EMPTY_STRING;
	private Double subtotal = 0d, discount = 0d, outstandingPayment = 0d, totalPayment = 0d;
	private ProductBandbox productBandbox;
	private PendingOrderBandbox pendingBandbox;
	
	private List<OrderLine> lines = new ArrayList<>();
	private List<OrderPayment> payments = new ArrayList<>();
	
	private ListModelList<OrderLine> modelLine;
	private ListModelList<OrderPayment> modelPayment;
	
	private static final String OUTSTANDING_PAYMENT = "oustanding_payment";
	private static final String SERATUS_RIBU = "100k";
	private static final String LIMA_PULUH_RIBU = "50k";
	private static final String DUA_PULUH_RIBU = "20k";
	private static final String SEPULUH_RIBU = "10k";
	private static final String LIMA_RIBU = "5k";
	
	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		defaultForm(true);
		productBandbox = (ProductBandbox) getController(bndProduct, ATTR_BANDBOX_PRODUCT);
		productBandbox.getListbox().addEventListener(Events.ON_SELECT, evt -> {
			setLine(productBandbox.getProduct());
		});
		
		pendingBandbox = (PendingOrderBandbox) getController(bndPending, ATTR_BANDBOX_PENDING_ORDER);
		pendingBandbox.getListbox().addEventListener(Events.ON_SELECT, evt -> {
			getOrder(pendingBandbox.getOrder());
		});
		
		txtCardNo.setWidgetListener("onBind", "jq(this).mask('9999 9999 9999 9999');");
		
		Session s = Sessions.getCurrent();
		user = (User)s.getAttribute(USER_LOGIN);
		
		if (user == null)
			Executions.sendRedirect("login.zul");
		else
			lblUser.setValue(user.getName());
	}
	
	@Listen("onClick = #miLogout")
	public void menuitemLogout() {
		Session s = Sessions.getCurrent();
		s.removeAttribute(USER_LOGIN);
		
		Executions.sendRedirect("login.zul");
	}
	
	@Listen("onOK = #txtScanProduct")
	public void scanProduct() {
		Product product = productRepository.findByCode(txtScanProduct.getValue());
		if (product == null)
			return;
		
		setLine(product);
	}

	@Listen("onClick = #btnCash")
	public void cash() {
		dbAmount.setValue(null);
		aAddPayment.detach();
		setVisibleComponent(false, hboxCard, hboxCardNo);
		
		hboxAmount.setVisible(true);
		
		paymenttype = PaymentType.CASH.getValue();
		
		addPaymentButton(hboxAmount);
		
		aAddPayment.addEventListener(Events.ON_CLICK, evt -> onPayment(paymenttype));
	}

	@Listen("onOK = #dbAmount")
	public void paymentCash() {
		if (paymenttype.equals(PaymentType.CASH.getValue()))
			onPayment(paymenttype);
		else
			cbEdc.setFocus(true);
	}
	
	@Listen("onOK = #txtCardNo")
	public void cardNo() {
		if (!paymenttype.equals(PaymentType.CASH.getValue()))
			onPayment(paymenttype);
	}
	
	@Listen("onClick = #btnDebit") 
	public void debit() {
		dbAmount.setValue(subtotal - discount - totalPayment);
		paymentEdcView(PaymentType.DEBIT.getValue());
	}
	
	@Listen("onClick = #btnCredit") 
	public void credit() {
		dbAmount.setValue(subtotal - discount - totalPayment);
		paymentEdcView(PaymentType.CREDIT.getValue());
	}
	
	@Listen("onOK = #cbEdc; onSelect = #cbEdc")
	public void edc() {
		if (!paymenttype.equals(PaymentType.CASH.getValue()))
			txtCardNo.setFocus(true);
	}
	
	@Listen("onClick = #btnFinish")
	public void finish() {
		setOrder();
		order.setPaid(true);
		order.setVoided(false);
		order.setHold(false);
		orderRepository.save(order);
		
		orderLineRepository.save(lines);
		orderPaymentRepository.save(payments);
		
		lines.forEach(l -> {
			Transaction t = new Transaction();
			t.setActive(true);
			t.setLocator(configuration.getLocator());
			t.setMovementDate(l.getOrder().getDateOrdered());
			t.setOrderLine(l);
			t.setQty(l.getQty());
			t.setProduct(l.getProduct());
			transactionRepository.save(t);
			
			if (l.getProductFree() != null) {
				Transaction tFree = new Transaction();
				tFree.setActive(true);
				tFree.setLocator(configuration.getLocator());
				tFree.setMovementDate(l.getOrder().getDateOrdered());
				tFree.setOrderLine(l);
				tFree.setQty(l.getQtyFree());
				tFree.setProduct(l.getProductFree());
				transactionRepository.save(tFree);
			}
		});
		
		if (configuration.isDirectPrint()) {
			Map<String, Object> params = new HashMap<>();
			params.put("m_order_id", order.getOrderId());
			
			directPrint(JASPER_BILL, params);
		}
		
		resetData();
	}
	
	@Listen("onClick = #btnHold")
	public void hold() {
		confirm("Tunda transaksi ini?", evt -> {
			setOrder();
			order.setPaid(false);
			order.setVoided(false);
			order.setHold(true);
			orderRepository.save(order);
			
			orderLineRepository.save(lines);
			orderPaymentRepository.save(payments);
			
			resetData();
		});
	}
	
	@Listen("onClick = #btnVoid")
	public void voidOrder() {
		winVoid.setParent(getSelf());
		winVoid.doModal();
	}
	
	@Listen("onClick = #winVoid #btnVoidSupervisor")
	public void voidSupervisor() {
		User supervisor = userRepository.findByNameAndPassword(txtUsernameSupervisor.getValue(), 
				txtPasswordSupervisor.getValue());
		if (supervisor != null && supervisor.getRole().getName().equalsIgnoreCase(RoleName.SUPERVISOR.toString())) {
			confirm("Void transaksi ini?", evt -> {
				setOrder();
				order.setPaid(false);
				order.setVoided(true);
				order.setHold(false);
				orderRepository.save(order);
				
				orderLineRepository.save(lines);
				payments.clear();
				
				resetData();
				
				winVoid.detach();
			});
		}
	}
	

	private void getOrder(Order order) {
		if (order == null)
			return;
		
		order.setHold(false);
		order.setVoided(false);
		order.setPaid(false);
		
		lines = orderLineRepository.findByOrder(order.getOrderId());
		payments = orderPaymentRepository.findByOrder(order.getOrderId());
		
		this.order = order;
		
		loadLines();
	}
	
	private void setOrder() {
		if (order == null)
			order = new Order();
		
		order.setCashier(getUser());
		order.setChange(totalPayment - (subtotal - discount));
		order.setDateOrdered(new Date());
		order.setDocumentno(getDocumentNo());
		order.setGrandtotal(subtotal - discount);
		order.setNotes(txtNotes.getValue());
		order.setTotalPayment(totalPayment);
		order.setSubtotal(subtotal);
		order.setDiscountLine(discount);
		order.setLocator(configuration.getLocator());
	}
	
	private void setLine(Product product) {
		if (order == null)
			order = new Order();
		
		Optional<OrderLine> optLine = lines.stream()
				.filter(e -> e.getProduct().getCode().equalsIgnoreCase(product.getCode()))
				.filter(e -> e.getOrder().getOrderId() == order.getOrderId())
				.findFirst();
		
		OrderLine line;
		if (optLine.isPresent()) {
			line = optLine.get();
			line.setQty(line.getQty() + 1d);
		} else {
			line = new OrderLine();
			line.setOrder(order);
			line.setProduct(product);
			line.setPrice(product.getPrice());
			line.setGuarantee(product.getGuarantee());
			line.setLocked(false);
			line.setProductPromo(false);
			line.setQty(1d);
			lines.add(line);
		}
		
		getPromo(product, line);
		
		loadLines();
		
		txtScanProduct.setValue(null);
		txtScanProduct.setFocus(true);
	}

	private void getPromo(Product product, OrderLine line) {
		PromoLine pl = promoLineRepository.findActivePromo(product.getProductId());
		if (pl != null) {
			if (pl.getPromo().getType().equalsIgnoreCase(PromoType.BUY_FREE.getValue())) {
				if (line.getQty() % pl.getQty() == 0) {
					line.setQtyFree(line.getQty()/ pl.getQty());
					line.setProductFree(pl.getProductFree());
				} else {
					if (line.getQty() < pl.getQty()) {
						line.setQtyFree(0d);
						line.setProductFree(null);
					} else {
						int qtyForPromo = (int) (line.getQty()/pl.getQty());
						line.setQtyFree(Double.valueOf(qtyForPromo));
					}
				}
			} else if (pl.getPromo().getType().equalsIgnoreCase(PromoType.BUY_PAY.getValue())) {
				if (line.getQty() % pl.getQty() == 0) {
					line.setDiscountAmount((line.getQty()/ pl.getQty()) * (pl.getQtyPay() * line.getPrice()));
				} else {
					if (line.getQty() < pl.getQty()) {
						line.setDiscountAmount(0d);
					} else {
						int qtyForPromo = (int) (line.getQty()/pl.getQty());
						line.setDiscountAmount(qtyForPromo * (pl.getQtyPay() * line.getPrice()));
					}
				}
			}
			
			line.setTypePromo(pl != null ? pl.getPromo().getType() : null);
		}
	}
	
	private void onPayment(String type) {
		if (dbAmount.getValue() == null || dbAmount.getValue() == 0d)
			return;
		
		if (!type.equals(PaymentType.CASH.getValue())) {
			if (cbEdc.getSelectedIndex() < 0)
				return;
			
			if (Strings.isBlank(txtCardNo.getValue()))
				return;
		}
		
		addPayment(type, dbAmount.getValue());
		loadPayment();
		dbAmount.setValue(0d);
		cbEdc.setSelectedIndex(-1);
		txtCardNo.setValue(null);
		setVisibleComponent(false, hboxAmount, hboxCard, hboxCardNo);
	}
	
	private void addPaymentButton(Component parent) {
		aAddPayment = new A(EMPTY_STRING, "images/icon_control/check.png");
		aAddPayment.setStyle("padding-left: 10px");
		parent.appendChild(aAddPayment);
	}
	
	private void paymentEdcView(String type) {
		aAddPayment.detach();
		setVisibleComponent(true, hboxAmount, hboxCard, hboxCardNo);
		
		cbEdc.getItems().clear();
		List<Edc> edcs = edcRepository.findByMinAmount(outstandingPayment);
		for (Edc edc : edcs) {
			Comboitem item = new Comboitem(edc.getName());
			item.setValue(edc);
			item.setParent(cbEdc);
		}
		
		lblCardNo.setValue("No Kartu " + StringUtils.capitalize(PaymentType.getNameByCode(type).toLowerCase()));
		paymenttype = type;
		addPaymentButton(hboxCardNo);
		aAddPayment.addEventListener(Events.ON_CLICK, evt -> {
			onPayment(paymenttype);
		});
	}
	
	private void loadLines() {
		renderLines();
		
		subtotal = lines.stream().mapToDouble(line -> line.getPrice() * line.getQty()).sum();
		discount = lines.stream().mapToDouble(line -> line.getTypePromo() != null && 
			line.getTypePromo().equalsIgnoreCase(PromoType.BUY_PAY.getValue()) ? 
					line.getDiscountAmount() : line.getDiscount(line.getQty())).sum();
		
		lblSubtotal.setValue(formatNumber(subtotal));
		lblDiscount.setValue(formatNumber(discount));
		lblGrandtotal.setValue(formatNumber(subtotal-discount));
		
		setButtonPayment(lines.size() == 0);
		hboxLblQuick.setVisible(lines.size() > 0);
		hboxQuickCash.setVisible(lines.size() > 0);
		loadPayment(subtotal-discount);
		setVisibleComponent(false, hboxAmount, hboxCard, hboxCardNo);
	}
	
	private void renderLines() {
		listbox.setItemRenderer((Listitem listitem, OrderLine line, int row) -> {
			listitem.setValue(line);
			
			Doublebox dbQty = new Doublebox(line.getQty());
			dbQty.setInplace(true);
			dbQty.setWidth("50px");
			dbQty.setStyle("text-align: center !important");
			dbQty.addEventListener(Events.ON_BLUR, evt -> {
				line.setQty(dbQty.getValue());
				getPromo(line.getProduct(), line);
				loadLines();
			});
			
			dbQty.addEventListener(Events.ON_OK, evt -> {
				line.setQty(dbQty.getValue());
				getPromo(line.getProduct(), line);
				loadLines();
			});
			
			Vlayout layoutProduct = new Vlayout();
			layoutProduct.appendChild(ComponentUtil.label(line.getProduct().getCode()));
			if (line.getGuarantee() > 0)
				layoutProduct.appendChild(ComponentUtil.labelSmall(
						"Garansi : " + Utility.integerFormat(line.getGuarantee()) + " hari"));
			
			if (line.getDiscountAmount() > 0d)
				layoutProduct.appendChild(ComponentUtil.label("Disc " + formatNumber(line.getDiscountAmount())));
			
			if (line.getProductFree() != null)
				layoutProduct.appendChild(ComponentUtil.label("Free ".concat(formatNumber(line.getQtyFree()))
						.concat(" ").concat(line.getProductFree().getCode())));
			
			Combobox cbDisc = new Combobox((line.getDiscountPercent() != null || line.getDiscountPercent() > 0) ? 
					formatNumber(line.getDiscountPercent()) : null);
			cbDisc.setMold("rounded");
			cbDisc.setReadonly(true);
			cbDisc.setWidth("70px");
			loadDiscPercent(cbDisc);
			cbDisc.addEventListener("onSelect", evt -> {
				Double disc = cbDisc.getSelectedItem().getValue();
				line.setDiscountPercent(disc);
				loadLines();
			});
			
			Hbox hboxDisc = new Hbox();
			hboxDisc.appendChild(ComponentUtil.label("Disc : "));
			hboxDisc.appendChild(cbDisc);
			hboxDisc.appendChild(ComponentUtil.label(" %"));
			
			if (line.getTypePromo() == null)
				layoutProduct.appendChild(hboxDisc);
			
			A delete = new A(EMPTY_STRING, "images/icon_control/delete_16.png");
			delete.addEventListener(Events.ON_CLICK, evt -> {
				lines.remove(line);
				loadLines();
			});
			
			listitem.appendChild(lc(dbQty));
			listitem.appendChild(lc(layoutProduct));
			listitem.appendChild(lc(line.getTypePromo() != null && 
					line.getTypePromo().equalsIgnoreCase(PromoType.BUY_PAY.getValue()) ? 
					(line.getPrice() * line.getQty()) : line.getTotalPrice()));
			listitem.appendChild(lc(delete));
		});
		
		modelLine = new ListModelList<>(lines);
		listbox.setModel(modelLine);
	}
	
	private void loadDiscPercent(Combobox cbDisc) {
		for (Double i = 0d; i <= 70d; i+=5d) {
			if (i == 5d)
				continue;
			Comboitem ci = new Comboitem(formatNumber(i));
			ci.setValue(i);
			ci.setParent(cbDisc);
		}
	}
	
	private void loadPayment(Double grandtotal) {
		totalBayar(grandtotal);
		renderPayment(grandtotal);
		
		setVisibleComponent(false, hboxAmount, hboxCard, hboxCardNo);
	}
	
	private void loadPayment() {
		loadPayment(subtotal-discount);
	}
	
	private void totalBayar(Double grandtotal) {
		hboxQuickCash.getChildren().clear();
		if (lines.size() == 0)
			return;
		
		totalPayment = payments.size() > 0 ? payments.stream().mapToDouble(pay -> pay.getPaymentAmount()).sum() : 0d;
		
		outstandingPayment = grandtotal - totalPayment;
		
		Map<String, Double> maps = new HashMap<>();
		
		maps.put(SERATUS_RIBU, 100000d);
		maps.put(LIMA_PULUH_RIBU, 50000d);
		maps.put(DUA_PULUH_RIBU, 20000d);
		maps.put(SEPULUH_RIBU, 10000d);
		maps.put(LIMA_RIBU, 5000d);
		
		maps = maps.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.naturalOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
		
		if (outstandingPayment > 0d) {
			maps.put(OUTSTANDING_PAYMENT, outstandingPayment);
		}
		
		maps.forEach((key, value) -> {
			Button btn = new Button(key.equals(OUTSTANDING_PAYMENT) ? formatNumber(value) : key);
			btn.setSclass("mybutton button green");
			btn.setStyle("padding: 0.5em 1.5em 0.55em !important");
			
			btn.addEventListener(Events.ON_CLICK, evt -> {
				addPayment(PaymentType.CASH.getValue(), value);
				loadPayment(grandtotal);
				setVisibleComponent(false, hboxAmount, hboxCard, hboxCardNo);
			});
			
			hboxQuickCash.appendChild(btn);
		});
		
		btnFinish.setDisabled(totalPayment < grandtotal);
		lblTotalPayment.setValue(formatNumber(totalPayment));
		lblChange.setValue(formatNumber(totalPayment - grandtotal));
	}
	
	private void renderPayment(Double grandtotal) {
		listboxPayment.setItemRenderer((Listitem listitem, OrderPayment payment, int row) -> {
			listitem.getChildren().clear();
			listitem.setValue(payment);
			
			Doublebox dbPaymentAmount = new Doublebox(payment.getPaymentAmount());
			dbPaymentAmount.setInplace(true);
			dbPaymentAmount.setFormat(NUMBER_THOUSAND_SEPARATOR);
			dbPaymentAmount.addEventListener(Events.ON_OK, evt -> {
				payment.setPaymentAmount(dbPaymentAmount.getValue());
				loadPayment(grandtotal);
			});
			
			dbPaymentAmount.addEventListener(Events.ON_BLUR, evt -> {
				payment.setPaymentAmount(dbPaymentAmount.getValue());
				loadPayment(grandtotal);
			});
			
			A delete = new A(EMPTY_STRING, "images/icon_control/delete_16.png");
			delete.addEventListener(Events.ON_CLICK, evt -> {
				payments.remove(payment);
				loadPayment(grandtotal);
			});
			
			listitem.appendChild(lc(PaymentType.getNameByCode(payment.getPaymentType())));
			listitem.appendChild(lc(dbPaymentAmount));
			listitem.appendChild(lc(delete));
		});
		
		modelPayment = new ListModelList<>(payments);
		listboxPayment.setModel(modelPayment);
	}
	
	private void resetData() {
		defaultForm(true);
		order = new Order();
		lines.clear();
		payments.clear();
		subtotal = 0d; 
		discount = 0d;
		outstandingPayment = 0d;
		totalPayment = 0d;
		
		pendingBandbox.load();
		loadLines();
		loadPayment();
	}

	private void defaultForm(boolean disabled) {
		txtScanProduct.setFocus(disabled);
		btnFinish.setDisabled(disabled);
		
		setButtonPayment(disabled);
		setZeroLabel(lblTotalPayment, lblSubtotal, lblSpecialDiscount, lblGrandtotal, lblDiscount, lblChange);
	}
	
	private void setButtonPayment(boolean disabled) {
		btnCash.setDisabled(disabled);
		btnDebit.setDisabled(disabled);
		btnCredit.setDisabled(disabled);
	}
	
	private void setZeroLabel(Label...labels) {
		for (Label label : labels) {
			label.setValue("0");
		}
	}
	
	private String getDocumentNo() {
		return configuration.getDocumentNo(configurationRepository, new Date());
	}
	
	private void addPayment(String type, Double amount) {
		if (order == null)
			order = new Order();
		
		Optional<OrderPayment> optLine = payments.stream()
				.filter(e -> e.getPaymentType().equals(type))
				.filter(e -> e.getOrder().getOrderId() == order.getOrderId())
				.findFirst();
		
		OrderPayment payment;
		if (optLine.isPresent() && optLine.get().getPaymentType().equals(PaymentType.CASH.getValue())) {
			payment = optLine.get();
			payment.setPaymentAmount(payment.getPaymentAmount() + amount);
		} else {
			boolean isCash = type.equals(PaymentType.CASH.getValue());
			
			payment = new OrderPayment();
			payment.setOrder(order);
			payment.setCardNo(!isCash ? txtCardNo.getValue() : null);
			payment.setEdc(!isCash ? cbEdc.getSelectedItem().getValue() : null);
			payment.setPaymentAmount(amount);
			payment.setPaymentType(type);
			payments.add(payment);
		}
	}
}