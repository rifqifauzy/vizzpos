package pos.vizzpos.tree;

import java.util.Collection;

import org.zkoss.zul.DefaultTreeNode;
import org.zkoss.zul.TreeNode;

public class CustomTreeNode<T> extends DefaultTreeNode<T> {

	private static final long serialVersionUID = 4297081559823318837L;

	private boolean open = false;
	
	public CustomTreeNode(T data, Collection<? extends TreeNode<T>> listData, boolean open) {
		super(data, listData, open);
		this.setOpen(open);
	}
	
	public CustomTreeNode(T data, Collection<? extends TreeNode<T>> listData) {
		super(data, listData);
		this.setOpen(true);
	}

	public CustomTreeNode(T data) {
		super(data);
	}
	
	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
        this.open = open;
    }
	
}
