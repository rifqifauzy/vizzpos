package pos.vizzpos;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import pos.vizzpos.entity.Menu;
import pos.vizzpos.entity.User;
import pos.vizzpos.repository.ConfigurationRepository;
import pos.vizzpos.repository.MenuRepository;
import pos.vizzpos.repository.UserRepository;
import pos.vizzpos.utils.RoleName;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class LoginWindow extends CoreWindow {
	
	private static final long serialVersionUID = 1L;
	
	@Wire private Textbox txtUsername, txtPassword;
	
	@WireVariable private UserRepository userRepository;
	@WireVariable private MenuRepository menuRepository;
	@WireVariable private ConfigurationRepository configurationRepository;
	
	private User user;

	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		createMenu();
		
		Session s = Sessions.getCurrent();
		user = (User) s.getAttribute(USER_LOGIN);
		txtUsername.setValue(null);
		txtPassword.setValue(null);
		
		if (user != null) {
			if (user.getRole().getName().equalsIgnoreCase(RoleName.POS.toString())) {
				if (configurationRepository.count() < 1) {
					showMessage("Belum ada konfigurasi");
					return;
				}
				
				Executions.sendRedirect("pos.zul");
			} else {
				Executions.sendRedirect("main.zul");
			}
		}
	}

	@Listen("onClick = #btnLogin; onOK = #txtPassword; onOK = #txtUsername")
	public void login() {
		User user = userRepository.findByNameAndPassword(txtUsername.getValue(), txtPassword.getValue());
		if (user != null) {
			Session s = Sessions.getCurrent();
			s.setAttribute(USER_LOGIN, user);

			if (user.getRole().getName().equalsIgnoreCase(RoleName.SUPERVISOR.toString()) || 
					user.getRole().getName().equalsIgnoreCase(RoleName.POS.toString()))
				Executions.sendRedirect("pos.zul");
			else 
				Executions.sendRedirect("main.zul");
		} else {
			showMessage("Username dan password tidak cocok");
			return;
		}
	}
	
	private void createMenu() {
		createMenu("01", null, "Master", null, null, 0, false, true, true);
		createMenu("0101", "01", "Satuan", null, ZUL_MASTER_UNIT, 1, true, true, true);
		createMenu("0102", "01", "Kategori Produk", null, ZUL_MASTER_PRODUCT_CATEGORY, 1, true, true, true);
		createMenu("0103", "01", "Produk", null, ZUL_MASTER_PRODUCT, 1, true, true, true);
		createMenu("0104", "01", "Locator", null, ZUL_MASTER_LOCATOR, 1, true, true, true);
		createMenu("0105", "01", "EDC", null, ZUL_MASTER_EDC, 1, true, true, true);
		createMenu("0106", "01", "Promo", null, ZUL_MASTER_PROMO, 1, true, true, true);
		createMenu("0107", "01", "Atur Promo", null, ZUL_MASTER_PROMO_MANAGE, 1, true, true, true);
		createMenu("0108", "01", "Doc Type", null, ZUL_MASTER_DOC_TYPE, 1, true, true, true);
		
		createMenu("02", null, "Utilitas", null, null, 0, false, true, true);
		createMenu("0201", "02", "Konfigurasi", null, ZUL_UTILITY_CONFIGURATION, 1, true, true, true);
		
		createMenu("03", null, "Import", null, null, 0, false, true, true);
		createMenu("0301", "03", "Product", null, ZUL_IMPORT_PRODUCT, 1, true, true, true);
		
		createMenu("04", null, "Aktifitas", null, null, 0, false, true, true);
		createMenu("0401", "04", "Penerimaan Barang", null, ZUL_ACTIVITY_RECEIPT, 1, true, true, true);
		createMenu("0402", "04", "Opname", null, ZUL_ACTIVITY_OPNAME, 1, true, true, true);
		
		createMenu("05", null, "Laporan", null, null, 0, false, true, true);
		createMenu("0501", "05", "Penjualan", null, null, 1, false, true, true);
		createMenu("050101", "0501", "Detail per Hari", null, ZUL_REPORT_SALES_PER_DAY, 2, true, true, true);
	}

	private void createMenu(String code, String parentCode, String name, String icon,
			String zulpath, Integer level, boolean menuitem, boolean active,
			boolean menubar) {
		
		try {
			Menu menu = new Menu(code, parentCode, name, icon, zulpath, 
					level, menuitem, active, menubar);
			
			if (menuRepository.findMenuByCode(code) == null) 
				menuRepository.saveAndFlush(menu);			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}