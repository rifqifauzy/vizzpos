package pos.vizzpos.bandbox;

import java.util.Date;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Bandpopup;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Space;
import org.zkoss.zul.Textbox;

import pos.vizzpos.utils.Constans;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public abstract class CoreBandbox extends SelectorComposer<Bandbox> implements Constans {
	
	private static final long serialVersionUID = 7097338139024043880L;
	
	protected Listbox listbox = new Listbox();
	protected Textbox txtCari = new Textbox();
	
	@Override
	public void doAfterCompose(Bandbox comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
	}
	
	protected void createPopup(Bandbox bandbox, String listboxWidth, Listheader... lhs) {
		Bandpopup bandpopup = new Bandpopup();
		bandpopup.setStyle("padding: 10px");
		
		bandbox.appendChild(bandpopup);
		
		Hbox hbox = new Hbox();
		hbox.setSpacing("5px");
		hbox.setAlign("center");
		
		listbox = new Listbox();
		txtCari = new Textbox();
		txtCari.setPlaceholder("Cari Data");
		
		hbox.appendChild(txtCari);
		bandpopup.appendChild(hbox);
		
		Space space = new Space();
		space.setHeight("10px");
		
		bandpopup.appendChild(space);
		bandpopup.appendChild(listbox);
		
		listbox.setMold("paging");
		listbox.setPageSize(5);
		listbox.getItems().clear();
		Listhead listhead = new Listhead();
		for (Listheader listheader : lhs) {
			listhead.appendChild(listheader);
		}
		
		listbox.appendChild(listhead);
		listbox.setRows(5);
		listbox.setWidth(listboxWidth);
		
		bandbox.setReadonly(true);
		bandbox.addEventListener("onOpen", e -> txtCari.setFocus(true));
	}
	
	protected Listheader lhSort(String title, String width, String sort) {
		Listheader listheader = new Listheader();
		listheader.setLabel(title);
		if(sort != null)
			listheader.setSort(sort);
		return listheader;
	}
	
	protected Listheader lh(String title, String width) {
		Listheader listheader = new Listheader();
		listheader.setLabel(title);
		if(width != null)
			listheader.setWidth(width);
		return listheader;
	}
	
	protected Listheader lh(String title, String width, String align) {
		Listheader listheader = new Listheader();
		listheader.setLabel(title);
		listheader.setAlign(align);
		if(width != null)
			listheader.setWidth(width);
		return listheader;
	}
	
	protected Listcell lc(final String label) {
		Listcell listcell = new Listcell(label != null ? label : EMPTY_STRING);
		listcell.setStyle("white-space: nowrap;");
		return listcell;
	}
	
	protected Listcell lc(Object object){
		if(object == null){
			return lc(" ");
		}else if(object instanceof String){
			return lc((String) object);
		}else if(object instanceof Number){
			return lc((Number) object);
		}else if(object instanceof Date){
			return lc((Date) object);
		}else if(object instanceof Component){
			return lc((Component) object);
		}else{
			return lc(" ");
		}
	}
}
