package pos.vizzpos.bandbox;

import java.util.List;

import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;

import pos.vizzpos.entity.Product;
import pos.vizzpos.repository.ProductRepository;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ProductBandbox extends CoreBandbox {

	private static final long serialVersionUID = 611604313668410761L;

	@WireVariable private ProductRepository productRepository;
	
	private ListModelList<Product> listModel;
	private Product product;
	
	@Override
	public void doAfterCompose(Bandbox comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		comp.setAttribute(ATTR_BANDBOX_PRODUCT, this);
		txtCari.setFocus(true);
		
		createPopup(comp, "650px", lhSort("Kode", "250px", "auto(code)"), lhSort("Nama Produk", "", "auto(name)"));
		
		load(txtCari.getValue());
		
		txtCari.addEventListener("onChanging", evt -> load(((InputEvent) evt).getValue()));
	}
	
	public void load(String search) {
		search = "%".concat(search.toLowerCase()).concat("%");
		List<Product> products = productRepository.findByCodeOrName(search, search);
		listModel = new ListModelList<>(products);
		listbox.setModel(listModel);
		render();
	}
	
	public void render() {
		listbox.setItemRenderer((Listitem listitem, Product product, int row) -> {
			listitem.getChildren().clear();
			listitem.setValue(product);
			
			listitem.appendChild(lc(product.getCode()));
			listitem.appendChild(lc(product.getName()));
		});
	}
	
	public Listbox getListbox() {
		return listbox;
	}

	public Product getProduct() {
		return getProduct(true);
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Product getProduct(boolean selection) {
		setProduct(listbox.getSelectedItem().getValue());
		if (selection)
			listbox.clearSelection();
		this.getSelf().close();
		return product;
	}
}
