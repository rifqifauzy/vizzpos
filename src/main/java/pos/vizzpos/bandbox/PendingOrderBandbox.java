package pos.vizzpos.bandbox;

import java.util.List;

import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;

import pos.vizzpos.entity.Order;
import pos.vizzpos.repository.OrderRepository;
import pos.vizzpos.utils.Utility;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class PendingOrderBandbox extends CoreBandbox {

	private static final long serialVersionUID = 7026045460852747633L;

	@WireVariable
	private OrderRepository orderRepository;

	private ListModelList<Order> listModel;
	private Order order;

	@Override
	public void doAfterCompose(Bandbox comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		comp.setAttribute(ATTR_BANDBOX_PENDING_ORDER, this);
		
		createPopup(comp, "650px", lhSort("No Nota", "150px", "auto(documentno)"), 
				lhSort("Waktu Order", "150px", "auto(dateordered)"),
				lh("Catatan", ""), lhSort("Grantotal", "100px", "auto(grandtotal)"));

		txtCari.setVisible(false);
		
		load();
	}

	public void load() {
		List<Order> orders = orderRepository.findByHoldOrderByOrderIdDesc(true);
		listModel = new ListModelList<>(orders);
		listbox.setModel(listModel);
		render();
	}

	public void render() {
		listbox.setItemRenderer((Listitem listitem, Order order, int row) -> {
			listitem.getChildren().clear();
			listitem.setValue(order);

			listitem.appendChild(lc(order.getDocumentno()));
			listitem.appendChild(lc(Utility.sdfFull.format(order.getDateOrdered())));
			listitem.appendChild(lc(order.getNotes()));
			listitem.appendChild(lc(Utility.integerFormat(order.getGrandtotal())));
		});
	}

	public Listbox getListbox() {
		return listbox;
	}

	public Order getOrder() {
		setOrder(listbox.getSelectedItem().getValue());
		listbox.clearSelection();
		this.getSelf().close();
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}
}
