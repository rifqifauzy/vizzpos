package pos.vizzpos.template;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Map;

import javax.servlet.ServletContext;

import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Window;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import pos.vizzpos.CoreWindow;
import pos.vizzpos.utility.DBConnection;

@SuppressWarnings("serial")
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ReportTemplateWindow extends CoreWindow {

	@Wire private Iframe iframe;
	
	private String jasperSrc;
	private Map<String, Object> parameters;

	@SuppressWarnings("unchecked")
	@Override
	public void doAfterCompose(Window comp) throws Exception {
		super.doAfterCompose(comp);
		jasperSrc = (String) getParameter(ATTR_TEMPLATE);
		parameters = (Map<String, Object>) getParameter(ATTR_PARAMETERS);
		openReport();
	}

	private void openReport() throws SQLException, ClassNotFoundException {
		try {
			@SuppressWarnings("deprecation")
			ServletContext context = (ServletContext) Executions.getCurrent().getDesktop().getWebApp().getNativeContext();
			String contextPath = context.getRealPath("/");
			JasperPrint jasperPrint = JasperFillManager.fillReport(contextPath + jasperSrc, parameters, 
					DBConnection.getConnectionStatic());
			ByteArrayOutputStream bytesOutputStream = new ByteArrayOutputStream();
			BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(bytesOutputStream);

			JRExporter exporter = new net.sf.jasperreports.engine.export.JRPdfExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, bufferedOutputStream);
			exporter.exportReport();

			InputStream mediais = new ByteArrayInputStream(bytesOutputStream.toByteArray());
			bytesOutputStream.close();

			AMedia amedia = new AMedia("report.pdf", "pdf", "application/pdf", mediais);
			
			iframe.setContent(amedia);
		} catch (JRException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
