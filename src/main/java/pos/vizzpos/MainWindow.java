package pos.vizzpos;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Include;
import org.zkoss.zul.Menubar;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Tabpanels;
import org.zkoss.zul.Tabs;
import org.zkoss.zul.Window;

import pos.vizzpos.entity.Menu;
import pos.vizzpos.entity.User;
import pos.vizzpos.repository.MenuRepository;
import pos.vizzpos.repository.OrderLineRepository;
import pos.vizzpos.repository.OrderPaymentRepository;
import pos.vizzpos.repository.OrderRepository;
import pos.vizzpos.repository.TransactionRepository;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class MainWindow extends CoreWindow {
	
	private static final long serialVersionUID = 5917809999016379566L;
	
	@Wire private Tabbox txContent;
	@Wire private Tabpanels tpsContent;
	@Wire private Tabs tsContent;
	@Wire private Menubar mbMain;
	@Wire private Menuitem miUserData, miCompany, miRole;
	
	@WireVariable private MenuRepository menuRepository;
	@WireVariable private OrderRepository orderRepository;
	@WireVariable private OrderPaymentRepository orderPaymentRepository;
	@WireVariable private OrderLineRepository orderLineRepository;
	@WireVariable private TransactionRepository transactionRepository;
	
	private User user;
	
	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		Session s = Sessions.getCurrent();
		user = (User)s.getAttribute(USER_LOGIN);
		
		if (user != null) {
			setVisibleComponent(user.getRole().getName().equalsIgnoreCase("ROOT"), miRole);
			loadMenu();
		} else {
			Executions.sendRedirect("login.zul");
		}
	}
	
	@Listen("onClick = #miUserData")
	public void menuitemUserData() {
		loadContent(ZUL_USER_DATA, TITLE_USER_DATA, tsContent, tpsContent);		
	}
	
	@Listen("onClick = #miRole")
	public void menuitemRole() {
		loadContent(ZUL_ROLE, TITLE_ROLE, tsContent, tpsContent);		
	}
	
	@Listen("onClick = #miUser")
	public void menuitemMenu() {
		loadContent(ZUL_USER, TITLE_USER, tsContent, tpsContent);		
	}
	
	@Listen("onClick = #miCompany")
	public void menuitemInfoPerusahaan() {
		loadContent(ZUL_COMPANY, TITLE_COMPANY, tsContent, tpsContent);		
	}
	
	@Listen("onClick = #miClear")
	public void menuitemClearData() {
		if (user.getRole().getName().equalsIgnoreCase("ROOT")) {
			confirm("Clear data?", evt -> {
				if (evt.getName().equals("onOK")) {
					transactionRepository.deleteAll();
					orderLineRepository.deleteAll();
					orderPaymentRepository.deleteAll();
					orderRepository.deleteAll();
					configuration.setSequence(1);
					configurationRepository.save(configuration);
				}
			});
		}
	}
	
	@Listen("onClick = #miLogout")
	public void menuitemLogout() {
		Session s = Sessions.getCurrent();
		s.removeAttribute(USER_LOGIN);
		
		Executions.sendRedirect("login.zul");
	}
	
	public void loadContent(String zul, String title, Tabs tabParent, Tabpanels tabpanelParent, Object param) {
		Session s = Sessions.getCurrent();
		s.setAttribute(param.getClass().getName(), param);
		
		Include inc = new Include(zul);
		Tab tab = new Tab(title);
		tab.setParent(tabParent);
		tab.setClosable(true);
		
		Tabpanel tabpanel = new Tabpanel();
		tabpanel.appendChild(inc);
		tabpanel.setHeight("100%");
		tabpanel.setParent(tabpanelParent);
		
		tab.setSelected(true);
	}
	
	public void loadContent(String zul, String title, Tabs tabParent, Tabpanels tabpanelParent) {
		Include inc = new Include(zul);
		Tab tab = new Tab(title);
		tab.setParent(tabParent);
		tab.setClosable(true);
		
		Tabpanel tabpanel = new Tabpanel();
		tabpanel.appendChild(inc);
		tabpanel.setHeight("100%");
		tabpanel.setParent(tabpanelParent);
		
		tab.setSelected(true);
	}
	
	private void loadMenu() {
		if (user == null)
			return;
		tsContent.getChildren().clear();
		tpsContent.getChildren().clear();
		List<Menu> list = new ArrayList<>();
		
		List<Menu> listMenu = menuRepository.findMenuByRoleId(user.getRole().getRoleId());
		
		for (Menu menu : listMenu) {
			list.add(menu);
		}
		
		List<MenuTreeNode> nodes = new ArrayList<>();
		for (Menu menu : list) {
			if (!menu.isMenuitem()) {
				nodes.add(new MenuTreeNode(menu, getChilds(menu, list)));
			}
		}
		
		for (MenuTreeNode menuTreeNode : nodes) {
			if (!menuTreeNode.getParent().isMenubar())
				System.out.println("ra kanggo");
			else 
				renderMenubar(menuTreeNode.getParent(), nodes);
		}
	}
	
	private void renderMenubar(Menu parent, List<MenuTreeNode> nodes) {
		if (parent.isMenubar() && parent.getLevel() == 0) {
			org.zkoss.zul.Menu mn = new org.zkoss.zul.Menu(parent.getName(), parent.getIcon());
			Menupopup popup = new Menupopup();
			popup.setSclass(CLASS_MENUPOPUP);
			
			renderMenuitem(parent, nodes, popup);
			
			mn.appendChild(popup);
			mbMain.appendChild(mn);
		}
	}

	private void renderMenuitem(Menu parent, List<MenuTreeNode> nodes, Menupopup popup) {
		for (Menu child : getMenubarChilds(parent, nodes)) {
			if (child.isMenubar() && child.isMenuitem()) {
				Menuitem mnItem = new Menuitem(child.getName(), child.getIcon());
				
				mnItem.addEventListener("onClick", 
						e -> loadContent(child.getZulpath(), child.getName(), tsContent, tpsContent));
				
				popup.appendChild(mnItem);
			} else if (child.isMenubar() && !child.isMenuitem()) {
				org.zkoss.zul.Menu mn = new org.zkoss.zul.Menu(child.getName(), child.getIcon());
				Menupopup popup2 = new Menupopup();
				popup2.setSclass(CLASS_MENUPOPUP);
				
				renderMenuitem(child, nodes, popup2);
				
				mn.appendChild(popup2);
				popup.appendChild(mn);
			}
		}
	}
	
	private List<Menu> getChilds(Menu parent, List<Menu> list) {
		List<Menu> childs = new ArrayList<>();
		for (Menu menu : list) {
			if (menu.getParentCode() != null && 
					menu.getParentCode().equalsIgnoreCase(parent.getCode())) {
				childs.add(menu);
			}
		}
		return childs;
	}
	
	private List<Menu> getMenubarChilds(Menu parent, List<MenuTreeNode> nodes) {
		for (MenuTreeNode menuTreeNode : nodes) {
			if (parent == menuTreeNode.getParent() && parent.isMenubar())
				return menuTreeNode.getChilds();
		}
		
		return null;
	}
	
	public Tabbox getTxContent() {
		return txContent;
	}

	public Tabpanels getTpsContent() {
		return tpsContent;
	}

	public Tabs getTsContent() {
		return tsContent;
	}

	class MenuTreeNode {
		private Menu parent;
		private List<Menu> childs = new ArrayList<>();
		
		public MenuTreeNode(Menu parent, List<Menu> childs) {
			super();
			this.parent = parent;
			this.childs = childs;
		}

		public Menu getParent() {
			return parent;
		}

		public void setParent(Menu parent) {
			this.parent = parent;
		}

		public List<Menu> getChilds() {
			return childs;
		}

		public void setChilds(List<Menu> childs) {
			this.childs = childs;
		}
	}
}