package pos.vizzpos;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;

import pos.vizzpos.entity.Role;
import pos.vizzpos.entity.User;
import pos.vizzpos.repository.MenuRepository;
import pos.vizzpos.repository.PrivilegeRepository;
import pos.vizzpos.repository.RoleRepository;
import pos.vizzpos.repository.UserRepository;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class IndexWindow extends CoreWindow {
	
	private static final long serialVersionUID = -3651700006524323996L;

	@WireVariable private UserRepository userRepository;
	@WireVariable private RoleRepository roleRepository;
	@WireVariable private MenuRepository menuRepository;
	@WireVariable private PrivilegeRepository privilegeRepository;
	
	private User user;
	
	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		registerData();
		
		Session s = Sessions.getCurrent();
		
		user = (User)s.getAttribute(USER_LOGIN);
		
		if(user!=null)
			Executions.sendRedirect("main.zul");
		else
			Executions.sendRedirect("login.zul");
	}
	
	private void registerData() {
		if (roleRepository.count() < 1) {
			Role roleRoot = new Role("ROOT", "ROLE ROOT", true);
			roleRepository.saveAndFlush(roleRoot);
			
			User userRoot = new User("rifqi", "rifqi10", true, roleRoot);
			userRepository.saveAndFlush(userRoot);
			
			Role roleAdmin = new Role("ADMIN", "ROLE ADMIN", true);
			roleRepository.saveAndFlush(roleAdmin);
			
			User userAdmin = new User("admin", "admin", true, roleAdmin);
			userRepository.saveAndFlush(userAdmin);
			
			Role rolePos = new Role("POS", "ROLE POS", true);
			roleRepository.saveAndFlush(rolePos);
			
			User userPos = new User("pos", "pos", true, rolePos);
			userRepository.saveAndFlush(userPos);
		}
	}
}