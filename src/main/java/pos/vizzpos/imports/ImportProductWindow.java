package pos.vizzpos.imports;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.zkoss.io.Files;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import pos.vizzpos.CoreWindow;
import pos.vizzpos.entity.Product;
import pos.vizzpos.repository.ProductRepository;
import pos.vizzpos.repository.UnitRepository;

@SuppressWarnings("serial")
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ImportProductWindow extends CoreWindow {

	@Wire private Listbox listbox;
	@Wire private Textbox txtFile;
	
	@WireVariable private ProductRepository productRepository;
	@WireVariable private UnitRepository unitRepository;

	private List<Product> products = new ArrayList<>();
	private List<Product> existProducts = new ArrayList<>();
	private ListModelList<Product> listModel;
	
	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
	}
	
	@Listen("onUpload = #btnUpload")
	public void upload(UploadEvent uploadEvent) throws IOException {
		String fullFilename = "";
		products.clear();
		
		if (uploadEvent != null) {
			Media media = uploadEvent.getMedia();
			if (!FilenameUtils.getExtension(media.getName()).equalsIgnoreCase("xls")) {
				showMessage("File yang diupload bukan file Excel 97-2003 (.xls)");
				return;
			}
			
			String tempDir = System.getProperty("java.io.tmpdir");
			if (!tempDir.substring(tempDir.length() - 1).equalsIgnoreCase("/") ||
					!tempDir.substring(tempDir.length() - 1).equalsIgnoreCase("\\"))
				tempDir += "/";
			
    		txtFile.setValue(media.getName());
    		
    		String filename = media.getName().replaceAll(" ", "_");
    		
    		Files.copy(new File(tempDir + filename), media.getStreamData());
    		fullFilename = tempDir + filename;
		}
		
		importExcel(fullFilename);
	}
	
	@Listen("onClick = #btnSimpan")
	public void simpan() {
		existProducts = productRepository.findAll();
		List<String> list = new ArrayList<>();
		existProducts.forEach(ex -> list.add(ex.getCode()));
		
		products.forEach(p -> {
			if (!list.contains(p.getCode()))
				productRepository.save(p);
		});
		
		batal();
	}
	
	@Listen("onClick = #btnBatal")
	public void batal() {
		products.clear();
		render();
		txtFile.setValue(null);
	}
	
	private void importExcel(String path) throws IOException {
		FileInputStream input = new FileInputStream(new File(path));
		HSSFWorkbook wb = new HSSFWorkbook(input);
		HSSFSheet sheet = wb.getSheetAt(0);
		
		String code = "", name = "";
		Double price = 0d;
		
		Iterator<Row> rows = sheet.iterator();
		while (rows.hasNext()) {
			HSSFRow row = (HSSFRow) rows.next();
			if (row.getRowNum() == 0)
				continue;
			
			if (row.getCell(0) == null)
				break;
			
			code = getObjectString(getCellValue(row, 0));
			name = getObjectString(getCellValue(row, 1));
			price = getObjectDouble(getCellValue(row, 2));
			
			Product product = new Product();
			product.setCode(code);
			product.setName(name);
			product.setPrice(price);
			product.setActive(true);
			product.setGuarantee(0);
			product.setProductCategory(null);
			product.setUnit(unitRepository.findByName("pcs"));
			products.add(product);
		}
		
		render();
	}
	
	private void render() {
		listbox.setItemRenderer((Listitem listitem, Product product, int row) -> {
			listitem.getChildren().clear();
			listitem.setValue(product);
			
			listitem.appendChild(lc(row + 1));
			listitem.appendChild(lc(product.getCode()));
			listitem.appendChild(lc(product.getName()));
			listitem.appendChild(lc(product.getPrice()));
		});
		
		listModel = new ListModelList<>(products);
		listbox.setModel(listModel);
	}
}
