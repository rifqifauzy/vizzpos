package pos.vizzpos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.PromoManage;

@Repository
public interface PromoManageRepository extends JpaRepository<PromoManage, Integer> {
	
	PromoManage findByPromoManageId(Integer promoManageId);
	List<PromoManage> findByNameContainingAllIgnoreCaseOrderByNameAsc(String name);
	List<PromoManage> findByNameContainingAndActiveAllIgnoreCaseOrderByNameAsc(String name, boolean active);
}
