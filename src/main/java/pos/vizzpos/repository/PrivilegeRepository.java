package pos.vizzpos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.Privilege;

@Repository
public interface PrivilegeRepository extends JpaRepository<Privilege, Integer> {

	public static final String SQL_FIND_ONE = "select p.* "
			+ "from m_privilege p "
			+ "where p.m_role_id = ? "
			+ "and p.m_menu_id = ? "
			+ "limit 1";
	
	@Query(value = SQL_FIND_ONE, nativeQuery=true)
	Privilege findOneByRoleIdAndMenuId(Integer roleId, Integer menuId);
}
