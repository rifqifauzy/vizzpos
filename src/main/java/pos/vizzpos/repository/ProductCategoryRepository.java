package pos.vizzpos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.ProductCategory;

@Repository
public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Integer> {
	
	List<ProductCategory> findByNameContainingAndDescriptionContainingAllIgnoreCaseOrderByNameAsc(String name, String description);

	List<ProductCategory> findByNameContainingAndDescriptionContainingAndActiveAllIgnoreCaseOrderByNameAsc(String name,
			String description, boolean active);
	
	List<ProductCategory> findByActiveOrderByNameAsc(boolean active);

	ProductCategory findByProductCategoryId(Integer productCategoryId);

}
