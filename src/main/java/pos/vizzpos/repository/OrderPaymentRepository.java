package pos.vizzpos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.OrderPayment;

@Repository
public interface OrderPaymentRepository extends JpaRepository<OrderPayment, Integer> {

	@Query(value = "select * from m_orderpayment "
			+ " where m_order_id = ? "
			+ " order by m_orderpayment_id ", nativeQuery=true)
	List<OrderPayment> findByOrder(Integer orderId);
}
