package pos.vizzpos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.Menu;

@Repository
public interface MenuRepository extends JpaRepository<Menu, Integer> {

	List<Menu> findByActiveOrderByCodeAscParentCodeAscLevelAscMenubarAsc(boolean active);
	List<Menu> findByParentCodeAndActiveOrderByMenuId(String parentCode, boolean active);
	
	public static final String SQL = "select m.* "
			+ "from m_privilege p "
			+ "join m_menu m on m.m_menu_id = p.m_menu_id "
			+ "where p.m_role_id = ? ";
	
	@Query(value = SQL, nativeQuery=true)
	List<Menu> findMenuByRoleId(Integer roleId);
	
	public static final String SQL_FIND_BY_CODE = "select m.* "
			+ "from m_menu m "
			+ "where m.code = ? "
			+ "limit 1";
	
	@Query(value = SQL_FIND_BY_CODE, nativeQuery=true)
	Menu findMenuByCode(String code);
}
