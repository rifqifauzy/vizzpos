package pos.vizzpos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

	List<Transaction> findByActiveOrderByMovementDate(boolean hold);
	
	@Query(value = "select * from m_transaction "
			+ " where m_orderline_id = ? ", nativeQuery=true)
	List<Transaction> findByOrderLine(Integer orderLineId);
	
	@Query(value = "select * from m_transaction "
			+ " where m_orderline_id > 0 ", nativeQuery=true)
	List<Transaction> findAllOrderLine();
	
	@Query(value = "select * from m_transaction "
			+ " where m_product_id = ? "
			+ " order by movementdate ", nativeQuery=true)
	List<Transaction> findByProduct(Integer productId);
	
	@Query(value = "select * from m_transaction "
			+ " where m_locator_id = ? "
			+ " order by movementdate ", nativeQuery=true)
	List<Transaction> findByLocator(Integer locatorId);
	
	@Query(value = "select * from m_transaction "
			+ " where m_locator_id = ? "
			+ " and m_product_id = ? "
			+ " order by movementdate ", nativeQuery=true)
	List<Transaction> findByLocatorAndProduct(Integer locatorId, Integer productId);
}
