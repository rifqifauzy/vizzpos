package pos.vizzpos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.PromoManageLine;

@Repository
public interface PromoManageLineRepository extends JpaRepository<PromoManageLine, Integer> {
	@Query(value = "select * from m_promo_manageline "
			+ " where m_promo_manage_id = ? "
			+ " order by m_promo_manageline_id ", nativeQuery=true)
	List<PromoManageLine> findByPromoManage(Integer promoManageId);
}
