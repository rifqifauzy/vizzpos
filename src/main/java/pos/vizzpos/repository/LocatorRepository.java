package pos.vizzpos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.Locator;

@Repository
public interface LocatorRepository extends JpaRepository<Locator, Integer> {

	List<Locator> findByNameContainingAndCodeContainingAllIgnoreCaseOrderByNameAsc(String name, String code);
	List<Locator> findByNameContainingAndCodeContainingAndActiveAllIgnoreCaseOrderByNameAsc(String name, String code, 
			boolean active);
	
	List<Locator> findByActiveOrderByNameAsc(boolean active);

	Locator findByLocatorId(Integer unitId);
}
