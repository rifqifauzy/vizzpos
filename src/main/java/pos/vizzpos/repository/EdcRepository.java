package pos.vizzpos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.Edc;

@Repository
public interface EdcRepository extends JpaRepository<Edc, Integer> {
	
	List<Edc> findByNameContainingAllIgnoreCaseOrderByNameAsc(String name);
	List<Edc> findByNameContainingAndActiveAllIgnoreCaseOrderByNameAsc(String name, boolean active);
	
	@Query(value = "select * from m_edc where minamount <= ?1 order by name", nativeQuery = true)
	List<Edc> findByMinAmount(Double minAmount);

	Edc findByEdcId(Integer edcId);
}
