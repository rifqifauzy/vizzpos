package pos.vizzpos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.Company;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Integer> {

}
