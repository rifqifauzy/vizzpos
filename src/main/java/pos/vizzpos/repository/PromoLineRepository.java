package pos.vizzpos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.PromoLine;

@Repository
public interface PromoLineRepository extends JpaRepository<PromoLine, Integer> {
	@Query(value = "select * from m_promoline "
			+ " where m_promo_id = ? "
			+ " order by m_promoline_id ", nativeQuery=true)
	List<PromoLine> findByPromo(Integer promoId);
	
	@Query(value = " select pl.* " +
			" from m_promo p " +
			" join m_promoline pl on pl.m_promo_id = p.m_promo_id " +
			" join m_promo_manageline pml on pml.m_promo_id = p.m_promo_id " +
			" join m_promo_manage pm on pm.m_promo_manage_id = pml.m_promo_manage_id " +
			" where pm.active = 1 " +
			" and p.active = 1 " +
			" and pl.m_product_id = ?  " +
			" and pm.dateeffective <= now() " +
			" order by pm.dateeffective desc, pm.created desc " +
			" limit 1 ", nativeQuery=true)
	PromoLine findActivePromo(Integer productId);
}
