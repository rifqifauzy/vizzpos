package pos.vizzpos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.Unit;

@Repository
public interface UnitRepository extends JpaRepository<Unit, Integer> {

	List<Unit> findByNameContainingAndDescriptionContainingAllIgnoreCaseOrderByNameAsc(String name, String description);

	List<Unit> findByNameContainingAndDescriptionContainingAndActiveAllIgnoreCaseOrderByNameAsc(String name,
			String description, boolean active);
	
	List<Unit> findByActiveOrderByNameAsc(boolean active);

	Unit findByUnitId(Integer unitId);
	
	@Query(value = "select * from m_unit where lower(name) = ?1 limit 1", nativeQuery = true)
	Unit findByName(String name);
}
