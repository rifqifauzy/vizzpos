package pos.vizzpos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	List<User> findByNameContainingAllIgnoreCaseOrderByNameAsc(String name);
	List<User> findByNameContainingAndActiveAllIgnoreCaseOrderByNameAsc(String name, boolean active);
	List<User> findByActiveOrderByNameAsc(boolean active);
	
	User findByNameAndPassword(String name, String password);
	User findByUserId(Integer userId);
}
