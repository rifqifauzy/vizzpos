package pos.vizzpos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.InOut;

@Repository
public interface InOutRepository extends JpaRepository<InOut, Integer> {
	
}
