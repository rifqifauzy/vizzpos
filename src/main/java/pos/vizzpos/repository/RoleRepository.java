package pos.vizzpos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
	List<Role> findByNameContainingAndDescriptionContainingAllIgnoreCaseOrderByNameAsc(String name, String description);
	List<Role> findByNameContainingAndDescriptionContainingAndActiveAllIgnoreCaseOrderByNameAsc(String name, String description, boolean active);
	List<Role> findByActiveOrderByNameAsc(boolean active);
	List<Role> findByActiveOrderByRoleId(boolean active);
	
	Role findByRoleId(Integer roleId);
	Role findByName(String name);
}
