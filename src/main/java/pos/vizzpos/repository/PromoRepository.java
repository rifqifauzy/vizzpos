package pos.vizzpos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.Promo;

@Repository
public interface PromoRepository extends JpaRepository<Promo, Integer> {
	
	Promo findByPromoId(Integer promoId);
	List<Promo> findByNameContainingAllIgnoreCaseOrderByNameAsc(String name);
	List<Promo> findByNameContainingAndActiveAllIgnoreCaseOrderByNameAsc(String name, boolean active);
	
	
	
}
