package pos.vizzpos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.OrderLine;

@Repository
public interface OrderLineRepository extends JpaRepository<OrderLine, Integer> {

	@Query(value = "select * from m_orderline "
			+ " where m_order_id = ? "
			+ " order by m_orderline_id ", nativeQuery=true)
	List<OrderLine> findByOrder(Integer orderId);
}
