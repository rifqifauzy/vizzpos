package pos.vizzpos.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.Configuration;

@Repository
public interface ConfigurationRepository extends JpaRepository<Configuration, Integer> {

	@Query(value = "select count(*) from m_order where dateordered between ?1 and ?2", nativeQuery = true)
	Integer findCountOrderByDay(Date start, Date end);
	
}
