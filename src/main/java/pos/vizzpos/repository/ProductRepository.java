package pos.vizzpos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

	List<Product> findByActiveAndNameContainingAndCodeContainingAllIgnoreCaseOrderByName(boolean active, String name, String code);
	List<Product> findByNameContainingAndCodeContainingAllIgnoreCaseOrderByName(String name, String code);
	List<Product> findByActiveOrderByName(boolean active);
	
	Product findByProductId(Integer productId);
	
	@Query(value = "select * from m_product where code = ?1 limit 1", nativeQuery = true)
	Product findByCode(String code);
	
	@Query(value = "select * from m_product where name = ?1 limit 1", nativeQuery = true)
	Product findByName(String name);
	
	@Query(value = "select * from m_product "
			+ " where (lower(code) like ?1 or lower(name) like ?2) "
			+ " and active = 1 order by name", nativeQuery=true)
	List<Product> findByCodeOrName(String code, String name);
}
