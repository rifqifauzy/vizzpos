package pos.vizzpos.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pos.vizzpos.entity.DocType;

@Repository
public interface DocTypeRepository extends JpaRepository<DocType, Integer> {

	@Query(value = "select count(*) from m_inout "
			+ " where movementdate between ?1 and ?2 and type=?", nativeQuery = true)
	Integer findCountInOutByDay(Date start, Date end, String type);
	
	List<DocType> findByNameContainingAllIgnoreCaseOrderByNameAsc(String name);
	DocType findByDocTypeId(Integer docTypeId);
}
