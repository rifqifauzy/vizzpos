package pos.vizzpos.utils;

import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zkmax.ui.util.DesktopRecycle;

import pos.vizzpos.entity.User;

public class KeepDesktop extends DesktopRecycle implements Constans {

	private boolean isMain = false;
	
	@Override
	public Desktop beforeService(Execution arg0, String arg1) {
		// TODO Auto-generated method stub
		
		User user = (User) arg0.getSession().getAttribute(USER_LOGIN);
		if (user != null) {
			return isMain ? super.beforeService(arg0, arg1) : null;
		} else {
			return arg0.getDesktop();
		}
	}
	
	@Override
	public void afterService(Desktop desktop) {
		// TODO Auto-generated method stub
		isMain = desktop.getRequestPath().equalsIgnoreCase("/main.zul");
		super.afterService(desktop);
	}
}