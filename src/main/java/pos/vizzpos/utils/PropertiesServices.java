package pos.vizzpos.utils;

import java.io.IOException;
import java.util.Properties;

public class PropertiesServices {
	
	private static Properties properties;
	
	private static void loadFile() throws IOException{
		if(properties == null) 
			properties = new Properties();
		
		if(properties.isEmpty()){
			properties.load(PropertiesServices.class.getResourceAsStream("vizzpos.properties"));
		}
	}
	
	public static String getContent(String key){
		try {
			loadFile();
		} catch (IOException e) { }
		
		if(properties.containsKey(key))
			return properties.getProperty(key);
		return key;
	}

} 
