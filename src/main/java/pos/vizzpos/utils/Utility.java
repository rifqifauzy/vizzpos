package pos.vizzpos.utils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Utility {

	public static final String THOUSAND_FORMAT = "#,##0";
	
	public static SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
	public static SimpleDateFormat sdfFull = new SimpleDateFormat("dd-MM-yy HH:mm");
	public static SimpleDateFormat sdfHour = new SimpleDateFormat("HH:mm");
	public static DecimalFormat dfDocument = new DecimalFormat("0000");
	public static DecimalFormat df = new DecimalFormat();
	
	public static Date endOfDay(Date date) {
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date);
	    calendar.set(Calendar.HOUR_OF_DAY, 23);
	    calendar.set(Calendar.MINUTE, 59);
	    calendar.set(Calendar.SECOND, 59);
	    calendar.set(Calendar.MILLISECOND, 999);
	    return calendar.getTime();
	}

	public static Date startOfDay(Date date) {
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date);
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    return calendar.getTime();
	}
	
	public static String integerFormat(Number number) {
		if (number == null)
			return "";
		
		df.applyPattern(THOUSAND_FORMAT);
		return df.format(number);
	}
}
