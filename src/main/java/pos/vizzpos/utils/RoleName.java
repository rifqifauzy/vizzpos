package pos.vizzpos.utils;

public enum RoleName {

	POS, ROOT, GUDANG, ADMIN, SUPERVISOR;
}
