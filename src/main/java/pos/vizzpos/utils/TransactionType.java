package pos.vizzpos.utils;

public enum TransactionType {

	SALES_ORDER("C-"), 
	RECEIPT("V+"), 
	INVENTORY_MINUS("I-"),
	INVENTORY_PLUS("I+");
	
	private String value;
	
	private TransactionType(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public static String getNameByCode(String value) {
		for (TransactionType e : TransactionType.values()) {
			if (value.equalsIgnoreCase(e.getValue()))
				return e.name();
		}
		return null;
	}
}
