package pos.vizzpos.utils;

public enum PromoType {
	
	BUNDLE("B"), 
	DISCOUNT("D"),
	BUY_FREE("F"), 
	BUY_PAY("P");

	private String value;
	
	private PromoType(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public static String getNameByCode(String value) {
		for (PromoType e : PromoType.values()) {
			if (value.equals(e.value))
				return e.name();
		}
		return null;
	}
}
