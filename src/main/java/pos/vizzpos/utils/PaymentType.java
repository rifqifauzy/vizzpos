package pos.vizzpos.utils;

public enum PaymentType {
	
	CASH("C"), 
	CREDIT("K"), 
	DEBIT("D");

	private String value;
	
	private PaymentType(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public static String getNameByCode(String value) {
		for (PaymentType e : PaymentType.values()) {
			if (value == e.value)
				return e.name();
		}
		return null;
	}
}
