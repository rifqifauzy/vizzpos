package pos.vizzpos.utils;

public interface Constans {
	
	/*public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
	public static final String DB_URL = "jdbc:mysql://localhost:3306/vizzpos";
	public static final String DB_USER = "root";
	public static final String DB_PASSWORD = "";*/
	
	public static final String EMPTY_STRING = "";
	public static final String DATE_FORMAT = "dd/MM/yyyy";
	public static final String DATE_FORMAT_TIPE_DOKUMEN = "yyyyMM";
	public static final String NUMBER_FORMAT = "#,###.##";
	public static final String NUMBER_THOUSAND_SEPARATOR = "#,##0";
	
	public static final String BARANG_PERSEDIAAN = "Persediaan";
	public static final String BARANG_NON_PERSEDIAAN = "Non Persediaan";
	public static final String BARANG_SERVIS = "Servis";
	
	public static final String PILIH_DATA_DIHAPUS = "Pilih data untuk dihapus";
	public static final String DATA_SUDAH_DIHAPUS = "Data dihapus";
	public static final String DATA_DIGUNAKAN = "Tidak bisa menghapus data karena data sedang digunakan";
	
	public static final String ZUL_USER_DATA = "pages/setting/userData.zul";
	public static final String ZUL_MENU = "pages/setting/menu.zul";
	public static final String ZUL_ROLE = "pages/setting/role.zul";
	public static final String ZUL_USER = "pages/setting/user.zul";
	public static final String ZUL_PRIVILEGE = "pages/setting/privilege.zul";
	public static final String ZUL_COMPANY = "pages/setting/company.zul";
	public static final String ZUL_SETTING = "pages/setting/setting.zul";
	
	public static final String ZUL_MASTER_UNIT = "pages/master/unit.zul";
	public static final String ZUL_MASTER_PRODUCT_CATEGORY = "pages/master/productCategory.zul";
	public static final String ZUL_MASTER_PRODUCT = "pages/master/product.zul";
	public static final String ZUL_MASTER_LOCATOR = "pages/master/locator.zul";
	public static final String ZUL_MASTER_EDC = "pages/master/edc.zul";
	public static final String ZUL_MASTER_PROMO = "pages/master/promo.zul";
	public static final String ZUL_MASTER_PROMO_MANAGE = "pages/master/promoManage.zul";
	public static final String ZUL_MASTER_DOC_TYPE = "pages/master/docType.zul";
	
	public static final String ZUL_UTILITY_CONFIGURATION = "pages/utility/configuration.zul";
	public static final String ZUL_IMPORT_PRODUCT = "pages/imports/importProduct.zul";
	
	public static final String ZUL_ACTIVITY_RECEIPT = "pages/activity/receipt.zul";
	public static final String ZUL_ACTIVITY_OPNAME = "pages/activity/opname.zul";
	
	public static final String ZUL_REPORT_SALES_PER_DAY = "pages/report/salesPerDay.zul";
	
	public static final String ZUL_REPORT_TEMPLATE = "pages/template/reportTemplate.zul";
	public static final String JASPER_BILL = "reports/bill.jasper";
	public static final String JASPER_SALES_PER_DAY = "reports/sales_per_product_detail.jasper";
	
	public static final String TITLE_USER = "User";
	public static final String TITLE_ROLE = "Role";
	public static final String TITLE_USER_DATA = "User Data";
	public static final String TITLE_COMPANY = "Company";
	
	public static final String USER_LOGIN = "UserLogin";
	
	public static final String CLASS_MENUPOPUP = "menupopup-border";
	public static final String CLASS_MANDATORY = "mandatory";
	
	public static final String UTILITY_MASTER_GUDANG = "Master Gudang";
	
	public static final String STYLE_MARGIN_BOTTOM_10 = "margin-bottom:10px";
	public static final String STYLE_BACKGROUND_MANDATORY = "background-color:#ffffcc";
	
	public static final String ICON_SIMPAN_32 = "images/icon_control/simpan_32.png";
	public static final String ICON_BATAL_32 = "images/icon_control/batal_32.png";
	public static final String ICON_HAPUS_32 = "images/icon_control/hapus_32.png";
	public static final String ICON_LIST_32 = "images/icon_control/list_32.png";
	public static final String ICON_REPORT_32 = "images/icon_control/report_32.png";
	public static final String ICON_TAMBAH_32 = "images/icon_control/tambah_32.png";
	public static final String ICON_UNPAID = "images/icon_control/unpaid.png";
	
	public static final String ATTR_TEMPLATE = "attr_template";
	public static final String ATTR_PARAMETERS = "attr_parameters";
	
	public static final String ATTR_BANDBOX_CUSTOMER = "attr_bd_customer";
	public static final String ATTR_BANDBOX_PENDING_ORDER = "attr_bd_pending_order";
	public static final String ATTR_BANDBOX_PRODUCT = "attr_bd_product";
	
	public static final String ATTR_COMBOBOX_SALESMAN = "attr_cb_item";
	
	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	public static final String IMG_BARU_32 = "images/icon_control/tambah_32.png";
	public static final String IMG_BATAL_32 = "images/icon_control/batal_32.png";
	public static final String IMG_FORM_32 = "images/icon_control/form_32.png";
	public static final String IMG_LIST_32 = "images/icon_control/list_32.png";
	
	public static final String TEXT_BATAL = "Batal";
	public static final String TEXT_FORM = "Form";
	public static final String TEXT_LIST = "List";
	public static final String TEXT_BARU = "Baru";
	
	public static final String ALIGN_CENTER = "center";
	public static final String GREEN = "green", RED = "red", BLUE = "blue", VIOLET = "violet", BLACK = "black";
	
	/*Utility Other Database*/
	public static final String DB_NAME_JLB = "JLB";
	public static final String DB_TABLE_NAME = "warehs";
}
