package pos.vizzpos.utils;

import org.zkoss.zul.Label;

public final class ComponentUtil {

	public static Label label(String value, String style) {
		Label label = new Label(value);
		if (style != null)
			label.setStyle(style);
		return label;
	}
	
	public static Label label(String value) {
		return label(value, null);
	}
	
	public static Label labelSmall(String value) {
		return label(value, "font-size:12px");
	}
}
