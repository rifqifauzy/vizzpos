package pos.vizzpos.utils;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.zkoss.image.AImage;
import org.zkoss.util.media.AMedia;
import org.zkoss.zul.Filedownload;

public class Excel implements Constans {
	private String file = "Excel";
	private HSSFWorkbook workbook = new HSSFWorkbook(); 
	private HSSFSheet sheet = workbook.createSheet("Sheet");
	private CreationHelper helper = workbook.getCreationHelper();
	
	private HSSFFont fontBold = workbook.createFont();
	private HSSFCellStyle style = workbook.createCellStyle();
	private HSSFCellStyle styleCenter = workbook.createCellStyle();
	private HSSFCellStyle styleBold = workbook.createCellStyle();
	private HSSFCellStyle styleBoldCenter = workbook.createCellStyle();
	private HSSFCellStyle styleBoldRight = workbook.createCellStyle();
	private HSSFCellStyle styleRight = workbook.createCellStyle();
	private HSSFCellStyle styleBoldLeft = workbook.createCellStyle();
	private HSSFCellStyle styleDate = workbook.createCellStyle();
	private HSSFCellStyle styleDateRed = workbook.createCellStyle();
	private HSSFCellStyle styleDateTime = workbook.createCellStyle();
	private HSSFCellStyle styleDecimal = workbook.createCellStyle();
	private HSSFCellStyle styleDecimalBold = workbook.createCellStyle();
	private HSSFCellStyle styleMoneyDolar = workbook.createCellStyle();
	private HSSFCellStyle styleMoneyIDR = workbook.createCellStyle();
	private HSSFCellStyle styleTextBlue = workbook.createCellStyle();
	private HSSFCellStyle styleTextViolet = workbook.createCellStyle();
	private HSSFCellStyle styleTextRed = workbook.createCellStyle();
	private HSSFCellStyle styleTextGreen = workbook.createCellStyle();
	
	public void download(){
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			workbook.write(out);
			AMedia amedia = new AMedia(file +  ".xls", "xls", "Download Excel Exception", out.toByteArray());
			Filedownload.save(amedia);
		} catch (Exception e) {
			System.out.println("Can't create Excel : "+e);
		}
	}
	
	private void initStyle(){
		{
			fontBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		}
		
		{
			style.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			style.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
			style.setBorderTop(HSSFCellStyle.NO_FILL);
			style.setBorderBottom(HSSFCellStyle.NO_FILL);
			style.setBorderLeft(HSSFCellStyle.NO_FILL);
			style.setBorderRight(HSSFCellStyle.NO_FILL);
			style.setWrapText(true);
			
			for (int i = 0; i < 10000; i++) {
				HSSFRow row = getRow(i);
				row.setRowStyle(style);
			}
		}
		
		{
			styleCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			styleCenter.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
			styleCenter.setBorderTop(HSSFCellStyle.NO_FILL);
			styleCenter.setBorderBottom(HSSFCellStyle.NO_FILL);
			styleCenter.setBorderLeft(HSSFCellStyle.NO_FILL);
			styleCenter.setBorderRight(HSSFCellStyle.NO_FILL);
			styleCenter.setWrapText(true);
		}
		
		{
			styleBoldCenter.setFont(fontBold);
			styleBoldCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			styleBoldCenter.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
			styleBoldCenter.setWrapText(true);
		}
		
		{
			styleBoldRight.setFont(fontBold);
			styleBoldRight.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
			styleBoldRight.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
			styleBoldRight.setBorderTop(HSSFCellStyle.NO_FILL);
			styleBoldRight.setBorderBottom(HSSFCellStyle.NO_FILL);
			styleBoldRight.setBorderLeft(HSSFCellStyle.NO_FILL);
			styleBoldRight.setBorderRight(HSSFCellStyle.NO_FILL);
			styleBoldRight.setWrapText(true);
		}
		
		{
			styleBoldRight.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
			styleBoldRight.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
			styleBoldRight.setBorderTop(HSSFCellStyle.NO_FILL);
			styleBoldRight.setBorderBottom(HSSFCellStyle.NO_FILL);
			styleBoldRight.setBorderLeft(HSSFCellStyle.NO_FILL);
			styleBoldRight.setBorderRight(HSSFCellStyle.NO_FILL);
			styleBoldRight.setWrapText(true);
		}
		
		{
			styleBoldLeft.setFont(fontBold);
			styleBoldLeft.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			styleBoldLeft.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
			styleBoldLeft.setBorderTop(HSSFCellStyle.NO_FILL);
			styleBoldLeft.setBorderBottom(HSSFCellStyle.NO_FILL);
			styleBoldLeft.setBorderLeft(HSSFCellStyle.NO_FILL);
			styleBoldLeft.setBorderRight(HSSFCellStyle.NO_FILL);
			styleBoldLeft.setWrapText(true);
		}		
		
		{
			styleBold.setFont(fontBold);
			styleBold.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			styleBold.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
			styleBold.setWrapText(true);
		}
		
		{
			styleDate.setDataFormat(helper.createDataFormat().getFormat("dd/MM/yyyy"));
		}
		
		{
			styleDateTime.setDataFormat(helper.createDataFormat().getFormat("dd/mm/yyyy h:mm"));
		}
		
		{
			styleDecimal.setDataFormat(helper.createDataFormat().getFormat("#,###.##"));
		}
		
		{
			styleDecimalBold.setDataFormat(helper.createDataFormat().getFormat("#,###.##"));
			styleDecimalBold.setFont(fontBold);
			styleDecimalBold.setWrapText(true);
		}
		
		{
			styleMoneyDolar.setDataFormat(helper.createDataFormat().getFormat("$ #,##0.00"));
		}
		
		{
			styleMoneyIDR.setDataFormat(helper.createDataFormat().getFormat("Rp #,##0.00"));
		}
		
	}
	
	public HSSFCellStyle getStyleTextBlue() {
		HSSFFont font = workbook.createFont();
		font.setColor(HSSFColor.BLUE.index);
		styleTextBlue.setFont(font);
		return styleTextBlue;
	}
	
	public HSSFCellStyle getStyleTextViolet() {
		HSSFFont font = workbook.createFont();
		font.setColor(HSSFColor.VIOLET.index);
		styleTextViolet.setFont(font);
		return styleTextViolet;
	}
	
	public HSSFCellStyle getStyleTextRed() {
		HSSFFont font = workbook.createFont();
		font.setColor(HSSFColor.RED.index);
		styleTextRed.setFont(font);
		return styleTextRed;
	}
	
	public HSSFCellStyle getStyleDateRed() {
		styleDateRed.setDataFormat(helper.createDataFormat().getFormat("dd/MM/yyyy"));
		
		HSSFFont font = workbook.createFont();
		font.setColor(HSSFColor.RED.index);
		styleDateRed.setFont(font);
		return styleDateRed;
	}
	
	public HSSFCellStyle getStyleCell(String format, String color) {
		HSSFCellStyle styleDate = workbook.createCellStyle();
		styleDate.setDataFormat(helper.createDataFormat().getFormat(format));
		
		HSSFFont font = workbook.createFont();
		font.setColor(getColor(color));
		styleDate.setFont(font);
		return styleDate;
	}
	
	public HSSFCellStyle getStyleTextGreen() {
		HSSFFont font = workbook.createFont();
		font.setColor(HSSFColor.GREEN.index);
		styleTextGreen.setFont(font);
		return styleTextGreen;
	}

	public short getColor(String color) {
		if (color.equalsIgnoreCase(GREEN))
			return HSSFColor.GREEN.index;
		else if (color.equalsIgnoreCase(RED))
			return HSSFColor.RED.index;
		else if (color.equalsIgnoreCase(VIOLET))
			return HSSFColor.VIOLET.index;
		else if (color.equalsIgnoreCase(BLUE))
			return HSSFColor.BLUE.index;
		else 
			return HSSFColor.BLACK.index;
	}
	
	public HSSFCellStyle getStyleDecimalBold() {
		return styleDecimalBold;
	}

	public HSSFCellStyle getStyleRight() {
		styleRight.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		return styleRight;
	}

	public HSSFWorkbook getWorkbook() {
		return workbook;
	}

	public HSSFSheet getSheet() {
		return sheet;
	}
	
	public void setLandscape(boolean landscape) {
		sheet.getPrintSetup().setLandscape(landscape);
	}
	
	public void cleanRow() {
		int numberOfRows = sheet.getPhysicalNumberOfRows();

	    if(numberOfRows > 0) {
	        for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
	            if (sheet.getRow(i) != null) {
	                sheet.removeRow(sheet.getRow(i));
	            }
	        }               
	    }
	}
	
	public HSSFCell getCell(int row, int column){
		HSSFCell ExcelCell = getRow(row).getCell(column);
		if(ExcelCell == null){
			ExcelCell = getRow(row).createCell(column);
		}
		return ExcelCell;
	}

	public HSSFRow getRow( int row){
		HSSFRow ExcelRow = sheet.getRow(row);
		if(ExcelRow == null){
			ExcelRow = sheet.createRow(row);
		}
		return ExcelRow;
	}
	
	public HSSFCellStyle getStyle() {
		return style;
	}
	
	public HSSFCellStyle getStyleBoldCenter() {
		return styleBoldCenter;
	}
	
	public HSSFCellStyle getStyleBoldRight() {
		return styleBoldRight;
	}
	
	public HSSFCellStyle getStyleBoldLeft() {
		return styleBoldLeft;
	}
	
	public HSSFCellStyle getStyleCenter() {
		return styleCenter;
	}

	public HSSFCellStyle getStyleDate() {
		return styleDate;
	}
	
	public HSSFCellStyle getStyleDateTime() {
		return styleDateTime;
	}

	public HSSFCellStyle getStyleDecimal() {
		return styleDecimal;
	}
	
	public HSSFCellStyle getStyleMoneyDolar() {
		return styleMoneyDolar;
	}

	public HSSFCellStyle getStyleMoneyIDR() {
		return styleMoneyIDR;
	}

	public HSSFCellStyle getStyleBold() {
		return styleBold;
	}

	public void setStyleBold(HSSFCellStyle styleBold) {
		this.styleBold = styleBold;
	}

	public void merge(String cell){
		sheet.addMergedRegion(CellRangeAddress.valueOf(cell));
	}
	
	public void setSheet(HSSFSheet sheet) {
		this.sheet = sheet;
	}
	
	@SuppressWarnings("static-access")
	public void setCell(int row, int column, Object object, HSSFCellStyle ccStyle){
		HSSFCell excelCell = getRow(row).getCell(column);
		if(excelCell == null){
			excelCell = getRow(row).createCell(column);
		}
		
		if(ccStyle != null){
			excelCell.setCellStyle(ccStyle);
		}
		
		if(object instanceof String){
			excelCell.setCellValue((String) object);
		}else if(object instanceof Double){
			excelCell.setCellValue((Double) object);
			if((Double) object > 0d){
				excelCell.setCellType(excelCell.CELL_TYPE_NUMERIC);
			}
		}else if(object instanceof Integer){
			excelCell.setCellValue((Integer) object);
		}else if(object instanceof Date){
			excelCell.setCellValue((Date) object);
		}else if(object instanceof Calendar){
			excelCell.setCellValue((Calendar) object);
		}else if(object instanceof Boolean){
			excelCell.setCellValue((Boolean) object);
		}else{
			excelCell.setCellValue("");
		}
	}
	
	@SuppressWarnings("static-access")
	public void setCell(int row, int column, Object object, HSSFCellStyle ccStyle, String currency){
		HSSFCell excelCell = getRow(row).getCell(column);
		if(excelCell == null){
			excelCell = getRow(row).createCell(column);
		}
		
		if(ccStyle != null){
			excelCell.setCellStyle(ccStyle);
		}
		
		if(object instanceof String){
			excelCell.setCellValue((String) object);
		}else if(object instanceof Double){
			excelCell.setCellValue((Double) object);
			if((Double) object > 0d){
				if(!currency.equals("")){
					HSSFCellStyle styleMoney = workbook.createCellStyle();
					styleMoney.setDataFormat(helper.createDataFormat().getFormat(currency+" #,##0.00"));
					excelCell.setCellStyle(styleMoney);
				}else{
					excelCell.setCellStyle(styleDecimal);
				}
				
				excelCell.setCellType(excelCell.CELL_TYPE_NUMERIC);
			}else{
				if(!currency.equals("")){
					HSSFCellStyle styleMoney = workbook.createCellStyle();
					styleMoney.setDataFormat(helper.createDataFormat().getFormat(currency+" #,##0.00"));
					excelCell.setCellStyle(styleMoney);
				}else{
					excelCell.setCellStyle(styleDecimal);
				}
				
				excelCell.setCellType(excelCell.CELL_TYPE_NUMERIC);
			}
		}else if(object instanceof Integer){
			excelCell.setCellValue((Integer) object);
		}else if(object instanceof Date){
			excelCell.setCellValue((Date) object);
		}else if(object instanceof Calendar){
			excelCell.setCellValue((Calendar) object);
		}else if(object instanceof Boolean){
			excelCell.setCellValue((Boolean) object);
		}else{
			excelCell.setCellValue("");
		}
	}
	
	public void setFile(String file) {
		this.file = file;
		initStyle();
	}
	
	public void setWidths(Integer... widths){
		int column = 0;
		for (Integer width : widths) {
			sheet.setColumnWidth(column, width);
			column++;
		}
	}
	
	public void setWidthsColumn(int column, Integer... widths){
		for (Integer width : widths) {
			sheet.setColumnWidth(column, width);
			column++;
		}
	}
	
	public void setBorderCells(int rowStart, int rowEnd, int cellStart, int cellEnd){
		for (int i = rowStart; i <= rowEnd; i++) {
			for (int j = cellStart; j <= cellEnd; j++) {
				HSSFCell cell = getCell(i, j);
				if(cell != null){
					HSSFCellStyle styles = cell.getCellStyle();
					if(styles != null){
						styles.setBorderTop(HSSFCellStyle.BORDER_THIN);
						styles.setBorderBottom(HSSFCellStyle.BORDER_THIN);
						styles.setBorderLeft(HSSFCellStyle.BORDER_THIN);
						styles.setBorderRight(HSSFCellStyle.BORDER_THIN);
						styles.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
						styles.setWrapText(true);
						getCell(i, j).setCellStyle(styles);
					}else{
						styles = workbook.createCellStyle();
						styles.setBorderTop(HSSFCellStyle.NO_FILL);
						styles.setBorderBottom(HSSFCellStyle.NO_FILL);
						styles.setBorderLeft(HSSFCellStyle.NO_FILL);
						styles.setBorderRight(HSSFCellStyle.NO_FILL);
						styles.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
						styles.setWrapText(false);
						getCell(i, j).setCellStyle(styles);
					}
				}
			}
		}
	}
	
	public void setImage(String file, Integer row, Integer col, Integer width, Integer height) throws Exception{
		try {
			AImage aImage = new AImage(file);
			byte[] fileBytes = scale(aImage.getByteData(), width, height);
			int fileIdx = workbook.addPicture(fileBytes, Workbook.PICTURE_TYPE_JPEG);
		    
		    ClientAnchor picAnchor = helper.createClientAnchor();
		    picAnchor.setCol1(col);
		    picAnchor.setRow1(row);
		    
		    Drawing drawing = sheet.createDrawingPatriarch();
		    Picture pict = drawing.createPicture(picAnchor, fileIdx);
		    pict.resize();
		} catch (Exception e) {}
	}
	
	private byte[] scale(byte[] fileData, int width, int height) {
    	ByteArrayInputStream in = new ByteArrayInputStream(fileData);
    	try {
    		BufferedImage img = ImageIO.read(in);
    		if(height == 0) {
    			height = (width * img.getHeight())/ img.getWidth(); 
    		}
    		if(width == 0) {
    			width = (height * img.getWidth())/ img.getHeight();
    		}
    		
    		Image scaledImage = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
    		BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    		imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0,0,0), null);

    		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

    		ImageIO.write(imageBuff, "jpg", buffer);
    		return buffer.toByteArray();
    	} catch (IOException e) {
    		return fileData;
    	}
    }
}
