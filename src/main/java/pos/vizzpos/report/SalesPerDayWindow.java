package pos.vizzpos.report;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Window;

import pos.vizzpos.CoreWindow;
import pos.vizzpos.utils.Utility;

@SuppressWarnings("serial")
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class SalesPerDayWindow extends CoreWindow {
	
	@Wire private Datebox dtFrom, dtTo;
	@Wire private Checkbox chkThisDay;
	
	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
	}

	@Listen("onClick = #chkThisDay")
	public void thisDay() {
		dtFrom.setValue(chkThisDay.isChecked() ? new Date() : null);
		dtTo.setValue(chkThisDay.isChecked() ? new Date() : null);
	}
	
	@Listen("onClick = #btnProses")
	public void proses() {
		if (dtFrom.getValue() == null || dtTo.getValue() == null)
			return;
		
		Map<String, Object> params = new HashMap<>();
		params.put("datefrom", new Timestamp(Utility.startOfDay(dtFrom.getValue()).getTime()));
		params.put("dateto", new Timestamp(Utility.endOfDay(dtTo.getValue()).getTime()));
		params.put("locatorId", configuration.getLocator().getLocatorId());
		
		loadReport(ZUL_REPORT_TEMPLATE, "Laporan Penjualan per Hari", JASPER_SALES_PER_DAY, params);
	}
	
	@Listen("onClick = #btnBatal")
	public void batal() {
		dtFrom.setValue(null);
		dtTo.setValue(null);
	}
}
