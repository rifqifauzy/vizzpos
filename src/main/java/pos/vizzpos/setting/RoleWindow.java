package pos.vizzpos.setting;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.lang.Strings;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.DefaultTreeModel;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.TreeitemRenderer;
import org.zkoss.zul.Treerow;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import pos.vizzpos.CoreWindow;
import pos.vizzpos.entity.Menu;
import pos.vizzpos.entity.Privilege;
import pos.vizzpos.entity.Role;
import pos.vizzpos.repository.MenuRepository;
import pos.vizzpos.repository.PrivilegeRepository;
import pos.vizzpos.repository.RoleRepository;
import pos.vizzpos.tree.CustomTreeNode;

@SuppressWarnings("serial")
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class RoleWindow extends CoreWindow {
	
	@Wire private Textbox txtName, txtDescription, txtSearchName, txtSearchDescription;
	@Wire private Checkbox chkActive;
	@Wire private Listbox listbox;
	@Wire private Combobox cbStatus;
	@Wire private Grid gridForm;
	@Wire private Vlayout vLayoutList;
	@Wire private Tree tree; 
	
	@WireVariable private RoleRepository roleRepository;
	@WireVariable private MenuRepository menuRepository;
	@WireVariable private PrivilegeRepository privilegeRepository;
	
	private Role role;
	private List<Menu> listMenuByRole = new ArrayList<>();
	
	private ListModelList<Role> listModel;
	private DefaultTreeModel<Menu> model;

	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		cbStatus.setSelectedIndex(0);
		role = (Role) getParameter(Role.class);
		if (role != null) {
			formView(true);
			getRole(role);
			loadDataByParam(role.getRoleId());
			removeParameter(Role.class);
		} else {
			formView(false);
			loadData(txtSearchName.getValue(), txtSearchDescription.getValue());
		}
	}
	
	@Listen("onClick = #chkAktif")
	public void aktifForm() {
		disableForm(gridForm, !chkActive.isChecked());
	}
	
	@Listen("onClick = #btnSimpan")
	public void simpan() {
		if (vLayoutList.isVisible())
			return;
		
		if (validasi()) {
			setRole();
			roleRepository.save(role);
			
			for (Treeitem ti : tree.getItems()) {
				Menu menu = ti.getValue();
				Privilege privilege = privilegeRepository.findOneByRoleIdAndMenuId(role.getRoleId(), menu.getMenuId());
				if (ti.isSelected()) {
					if (privilege == null) {
						privilege = new Privilege(role, menu, true);
						privilegeRepository.save(privilege);
					}
				} else {
					if (privilege != null)
						privilegeRepository.delete(privilege);
				}
			}
			
			formView(false);
			loadData(txtSearchName.getValue(), txtSearchDescription.getValue());
			clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
			role = new Role();
			clearForm(gridForm);
		}
	}
	
	@Listen("onClick = #btnView")
	public void view() {
		formView(!gridForm.isVisible());
	}
	
	@Listen("onClick = #btnBatal")
	public void batal() {
		formView(!gridForm.isVisible());
		clearObject();
	}

	@Listen("onClick = #btnRefresh")
	public void refresh() {
		if (gridForm.isVisible() && (role == null || role.getRoleId() == null))
			return;
		
		if (vLayoutList.isVisible()) {
			loadData(txtSearchName.getValue(), txtSearchDescription.getValue());
		} else { 
			if (role == null)
				return;
			
			roleRepository.flush();
			role = roleRepository.findOne(role.getRoleId());
			getRole(role);
		}
	}
	
	@Listen("onClick = #btnHapus")
	public void hapus() {
		if (vLayoutList.isVisible() && listbox.getSelectedIndex() < 0) {
			showMessage(PILIH_DATA_DIHAPUS);
			return;
		}
		
		role = listbox.getSelectedItem().getValue();
		confirmDelete(role.getName(), new EventListener<Event>() {
			
			@Override
			public void onEvent(Event event) throws Exception {
				// TODO Auto-generated method stub
				if (event.getName().equals("onOK")) {
					try {
						roleRepository.delete(role);
						clearObject();
					} catch (Exception e) {
						showMessage("Data sudah digunakan");
						return;
					}
				}
			}
		});
	}
	
	@Listen("onChanging = #txtSearchName")
	public void searchName(InputEvent event) {
		loadData(event.getValue(), txtSearchDescription.getValue());
	}
	
	@Listen("onChanging = #txtSearchDescription")
	public void searchDescription(InputEvent event) {
		loadData(txtSearchName.getValue(), event.getValue());
	}
	
	@Listen("onSelect = #cbStatus")
	public void cariStatus() {
		loadData(txtSearchName.getValue(), txtSearchDescription.getValue());
	}
	
	@Listen("onSelect = #listbox")
	public void pilih() {
		role = listbox.getSelectedItem().getValue();

		btnFirst.setDisabled(getIndexSelected() == 0 ? true : getIndexSelected() == (listModel.size() - 1) ? false : false);
		btnPrev.setDisabled(getIndexSelected() == 0 ? true : getIndexSelected() == (listModel.size() - 1) ? false : false);
		btnNext.setDisabled(getIndexSelected() == (listModel.size() - 1) ? true : getIndexSelected() == 0 ? false : false);
		btnLast.setDisabled(getIndexSelected() == (listModel.size() - 1) ? true : getIndexSelected() == 0 ? false : false);
		
		getRole(role);
	}
	
	@Listen("onClick = #btnFirst")
	public void first() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(0);
		pilih();
	}
	
	@Listen("onClick = #btnPrev")
	public void prev() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(getIndexSelected() - 1);
		pilih();
	}
	
	@Listen("onClick = #btnNext")
	public void next() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(getIndexSelected() + 1);
		pilih();
	}
	
	@Listen("onClick = #btnLast")
	public void last() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(listModel.size() - 1);
		pilih();
	}
	
	@Listen("onSort = #listbox > listhead > listheader")
	public void sort() {
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
		listModel.clearSelection();
	}
	
	private int getIndexSelected() {
		if (role == null)
			return -1;
		
		return listModel.indexOf(role);				
	}
	
	private void formView(boolean isForm) {
		gridForm.setVisible(isForm);
		vLayoutList.setVisible(!isForm);
		btnBatal.setTooltiptext(isForm ? TEXT_BATAL : TEXT_BARU);
		btnBatal.setImage(isForm ? IMG_BATAL_32 : IMG_BARU_32);
		btnBatal.setLabel(isForm ? TEXT_BATAL : TEXT_BARU);
		
		btnView.setTooltiptext(isForm ? TEXT_LIST : TEXT_FORM);
		btnView.setLabel(isForm ? TEXT_LIST : TEXT_FORM);
		btnView.setImage(isForm ? IMG_LIST_32 : IMG_FORM_32);
	}
	
	private void loadDataByParam(Integer id) {
		List<Role> list = new ArrayList<>();
		list.add(roleRepository.findByRoleId(id));
		listModel = new ListModelList<>(list);
		listbox.setModel(listModel);
		renderItem();
		listbox.setSelectedIndex(0);
	}
	
	private void loadData(String name, String description) {
		List<Role> list;
		
		boolean aktif = cbStatus.getSelectedIndex() == 1;
		if (cbStatus.getSelectedIndex() == 0)
			list = roleRepository.findByNameContainingAndDescriptionContainingAllIgnoreCaseOrderByNameAsc(name, description);
		else
			list = roleRepository.findByNameContainingAndDescriptionContainingAndActiveAllIgnoreCaseOrderByNameAsc(name, description, aktif);
		
		listModel = new ListModelList<>(list);
		listbox.setModel(listModel);
		renderItem();
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
	}
	
	private void renderItem() {
		listbox.setItemRenderer(new ListitemRenderer<Role>() {

			@Override
			public void render(Listitem listitem, Role role, int row) throws Exception {
				// TODO Auto-generated method stub
				listitem.getChildren().clear();
				listitem.setValue(role);
				
				listitem.addEventListener(Events.ON_DOUBLE_CLICK, new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						// TODO Auto-generated method stub
						formView(true);
						getRole(role);
					}
				});
				
				listitem.appendChild(lc(row + 1));
				listitem.appendChild(lc(role.getName()));
				listitem.appendChild(lc(role.getDescription()));
			}
		});
	}
	
	private void getRole(Role role) {
		if (role == null)
			return;
		txtName.setValue(role.getName());
		txtDescription.setValue(role.getDescription());
		chkActive.setChecked(role.isActive());
		disableForm(gridForm, !role.isActive());
		listMenuByRole = menuRepository.findMenuByRoleId(role.getRoleId());
		loadTree();
	}
	
	private void setRole() {
		if (role == null)
			role = new Role();
		role.setName(txtName.getValue());
		role.setDescription(txtDescription.getValue());
		role.setActive(chkActive.isChecked());
	}
	
	private boolean validasi() {
		if (Strings.isBlank(txtName.getValue())) {
			showMessage("Name masih kosong");
			return false;
		}
		
		return true;
	}
	
	private void clearObject() {
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
		clearForm(gridForm);
		loadData(txtSearchName.getValue(), txtSearchDescription.getValue());
		role = new Role();
		loadTree();
		if (model != null)
			model.clearSelection();
		tree.clearSelection();
	}
	
	private void loadTree() {
		if (role == null)
			return;
		
		List<Menu> details = new ArrayList<>();
		List<Menu> list = menuRepository.findByParentCodeAndActiveOrderByMenuId(null, true);
		for (Menu menu : list) {
			if (menu.isActive()) {
				menu.setChilds(getChilds(menu, list));
				details.add(menu);
			}
		}
		
		model = new DefaultTreeModel<>(getFileInfoTreeData(details));
		tree.setModel(model);
		model.setMultiple(true);
		tree.setItemRenderer(new TreeitemRenderer<CustomTreeNode<Menu>>() {

			@Override
			public void render(Treeitem treeitem, CustomTreeNode<Menu> data, int arg2) throws Exception {
				// TODO Auto-generated method stub
				Menu menu = data.getData();
				treeitem.setValue(menu);
				
				Treerow tr = treeitem.getTreerow();
				if(tr == null) {
					tr = new Treerow();
				} else {
					tr.getChildren().clear();
				}
				
				treeitem.appendChild(tr);
				tr.appendChild(tc(menu.getName()));
				
				// set selected
				treeitem.setSelected(setSelectedMenu(menu.getMenuId()));
				
				treeitem.setOpen(data.isOpen());
			}
		});
	}
	
	private boolean setSelectedMenu(Integer menuId) {
		if (listMenuByRole.size() == 0)
			return false;
		
		for (Menu menu : listMenuByRole) {
			if (menu.getMenuId() == menuId)
				return true;
		}
		
		return false;
	}
	
	private List<Menu> getChilds(Menu parent, List<Menu> list) {
		List<Menu> childs = new ArrayList<>();
		for (Menu menu : list) {
			if (menu.getParentCode() != null && 
					menu.getParentCode() == parent.getCode()) {
				childs.add(menu);
			}
		}
		
		for (Menu menu : childs) {
			Menu detail = menu;
			detail.setChilds(getChilds(detail, list));
		}
		
		return childs;
	}

	private CustomTreeNode<Menu> getFileInfoTreeData(List<Menu> details) {
		List<CustomTreeNode<Menu>> root = new ArrayList<CustomTreeNode<Menu>>();
		for (Menu menu : details) {
			addChilds(root, menu);
		}
		return new CustomTreeNode<Menu>(null, root);
	}

	private void addChilds(List<CustomTreeNode<Menu>> parents, Menu menu) {
		List<CustomTreeNode<Menu>> childs = new ArrayList<CustomTreeNode<Menu>>();
		for (Menu menu2 : menuRepository.findByParentCodeAndActiveOrderByMenuId(menu.getCode(), true)) {
			if (menuRepository.findByParentCodeAndActiveOrderByMenuId(menu2.getCode(), true).size() > 0) {
				addChilds(childs, menu2);
			} else {
				childs.add(new CustomTreeNode<Menu>(menu2));
			}
		}
		parents.add(new CustomTreeNode<Menu>(menu, childs));
	}
	
}
