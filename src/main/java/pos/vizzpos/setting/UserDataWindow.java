package pos.vizzpos.setting;

import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import pos.vizzpos.CoreWindow;
import pos.vizzpos.entity.User;
import pos.vizzpos.repository.UserRepository;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class UserDataWindow extends CoreWindow {

	private static final long serialVersionUID = -4727063016154752573L;

	@Wire private Textbox txtName, txtRole;
	@Wire private Window winUserData, winGantiPassword;
	
	@Wire("#winGantiPassword #txtLama") private Textbox txtLama;
	@Wire("#winGantiPassword #txtBaru") private Textbox txtBaru;
	@Wire("#winGantiPassword #txtKonfirmasi") private Textbox txtKonfirmasi;
	
	private User user;
	
	@WireVariable private UserRepository userRepository;
	
	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		user = getUser();
		
		txtName.setValue(user.getName());
		txtRole.setValue(user.getRole().getName());
	}
	
	@Listen("onClick = #btnGantiPassword")
	public void gantiPassword() {
		winGantiPassword.setParent(winUserData);
		winGantiPassword.doModal();
	}
	
	@Listen("onClick = #winGantiPassword #btnSimpan")
	public void simpan() {
		if (txtLama.getValue().equals(user.getPassword())) {
			if (txtBaru.getValue().equals(txtKonfirmasi.getValue())) {
				user.setPassword(txtBaru.getValue());
				userRepository.save(user);
				showMessage("Password sudah diganti");
			}
		} else {
			showMessage("Password lama salah");
		}
	}
}