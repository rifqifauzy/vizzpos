package pos.vizzpos.setting;

import java.io.File;
import java.io.IOException;

import org.zkoss.image.AImage;
import org.zkoss.io.Files;
import org.zkoss.lang.Strings;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Image;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import pos.vizzpos.CoreWindow;
import pos.vizzpos.entity.Company;
import pos.vizzpos.repository.CompanyRepository;
import pos.vizzpos.utils.PropertiesServices;

@SuppressWarnings("serial")
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class CompanyWindow extends CoreWindow {
	
	@Wire private Textbox txtName, txtPhone, txtFax, txtEmail, txtPostalCode, txtAddress, txtNpwp, txtCity;
	@Wire private Textbox txtFacebook, txtWebsite, txtInstagram, txtEvent;
	@Wire private Grid gridForm;
	@Wire private Image imgLogo;
	
	@WireVariable private CompanyRepository companyRepository;
	
	private Company company;
	private File file;
	private Media media;
	
	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		company = companyRepository.count() > 0 ? companyRepository.findAll().get(0) : null;
		getCompany();
	}

	@Listen("onClick = #btnSimpan")
	public void simpan() {
		if (validasi()) {
			setCompany();
			companyRepository.save(company);
			showMessage("Data disimpan");
		}
	}
	
	@Listen("onClick = #btnBatal")
	public void batal() {
		getCompany();
	}
	
	@Listen("onUpload = #btnUpload")
	public void upload(UploadEvent event){
		media = event.getMedia();
		if (media instanceof org.zkoss.image.Image) {
            imgLogo.setContent((org.zkoss.image.Image) media);
        } else {
            showMessage("File yang diupload bukan gambar");
            return;
        }
	}
	
	@Listen("onClick = #btnHapus")
	public void hapus(){
		imgLogo.setContent((org.zkoss.image.Image) null);
		media = null;
		if (company != null)
			company.setLogo(null);
	}
	
	private void setCompany() {
		if (company == null)
			company = new Company();
		
		company.setAddress(txtAddress.getValue());
		company.setEmail(txtEmail.getValue());
		company.setFax(txtFax.getValue());
		company.setPostalcode(txtPostalCode.getValue());
		company.setCity(txtCity.getValue());
		company.setName(txtName.getValue());
		company.setNpwp(txtNpwp.getValue());
		company.setPhone(txtPhone.getValue());
		company.setInstagram(txtInstagram.getValue());
		company.setFacebook(txtFacebook.getValue());
		company.setWebsite(txtWebsite.getValue());
		company.setEvent(txtEvent.getValue());
		
		if(media != null){
			String imageLocation = PropertiesServices.getContent("setting.location.file") + media.getName();
			file = new File(imageLocation);
			try {
				Files.copy(file, media.getStreamData());
				company.setLogo(imageLocation);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private void getCompany() {
		if (company == null) {
			clearForm(gridForm);
		} else {
			txtName.setValue(company.getName()); 
			txtPhone.setValue(company.getPhone()); 
			txtFax.setValue(company.getFax()); 
			txtEmail.setValue(company.getEmail()); 
			txtPostalCode.setValue(company.getPostalcode()); 
			txtAddress.setValue(company.getAddress()); 
			txtNpwp.setValue(company.getNpwp()); 
			txtCity.setValue(company.getCity());
			txtFacebook.setValue(company.getFacebook());
			txtWebsite.setValue(company.getWebsite());
			txtEvent.setValue(company.getEvent());
			txtInstagram.setValue(company.getInstagram());
			
			if(company.getLogo() != null){
				try {
					AImage img = new AImage(company.getLogo());
					imgLogo.setContent(img);
				} catch (IOException e) {
					
				}
			}
		}
	}
	
	private boolean validasi() {
		if (Strings.isBlank(txtName.getValue())) {
			showMessage("Nama masih kosong");
			return false;
		}
		
		if (Strings.isBlank(txtAddress.getValue())) {
			showMessage("Alamat masih kosong");
			return false;
		}
		
		if (Strings.isBlank(txtPhone.getValue())) {
			showMessage("Telepon masih kosong");
			return false;
		}
		
		if (Strings.isBlank(txtEmail.getValue())) {
			showMessage("Email masih kosong");
			return false;
		}
		
		if (Strings.isBlank(txtCity.getValue())) {
			showMessage("Kota masih kosong");
			return false;
		}
		
		return true;
	}
}
