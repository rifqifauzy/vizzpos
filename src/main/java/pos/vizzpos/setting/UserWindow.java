package pos.vizzpos.setting;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.lang.Strings;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import pos.vizzpos.CoreWindow;
import pos.vizzpos.entity.User;
import pos.vizzpos.repository.UserRepository;

@SuppressWarnings("serial")
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class UserWindow extends CoreWindow {
	
	@Wire private Textbox txtName, txtSearchName, txtPassword, txtUlangPassword;
	@Wire private Checkbox chkActive;
	@Wire private Listbox listbox;
	@Wire private Combobox cbStatus, cbRole;
	@Wire private Grid gridForm;
	@Wire private Vlayout vLayoutList;
	
	@WireVariable private UserRepository userRepository;
	
	private User user;
	
	private ListModelList<User> listModel;

	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		cbStatus.setSelectedIndex(0);
		user = (User) getParameter(User.class);
		if (user != null) {
			formView(true);
			getUser(user);
			loadDataByParam(user.getUserId());
			removeParameter(User.class);
		} else {
			formView(false);
			loadData(txtSearchName.getValue());
		}
	}
	
	@Listen("onClick = #chkAktif")
	public void aktifForm() {
		disableForm(gridForm, !chkActive.isChecked());
	}
	
	@Listen("onClick = #btnSimpan")
	public void simpan() {
		if (vLayoutList.isVisible())
			return;
		
		if (validasi()) {
			setUser();
			userRepository.save(user);
			
			formView(false);
			loadData(txtSearchName.getValue());
			clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
			user = new User();
			clearForm(gridForm);
		}
	}
	
	@Listen("onClick = #btnView")
	public void view() {
		formView(!gridForm.isVisible());
	}
	
	@Listen("onClick = #btnBatal")
	public void batal() {
		formView(!gridForm.isVisible());
		clearObject();
	}

	@Listen("onClick = #btnRefresh")
	public void refresh() {
		if (gridForm.isVisible() && (user == null || user.getUserId() == null))
			return;
		
		if (vLayoutList.isVisible()) {
			loadData(txtSearchName.getValue());
		} else { 
			if (user == null)
				return;
			
			userRepository.flush();
			user = userRepository.findOne(user.getUserId());
			getUser(user);
		}
	}
	
	@Listen("onClick = #btnHapus")
	public void hapus() {
		if (vLayoutList.isVisible() && listbox.getSelectedIndex() < 0) {
			showMessage(PILIH_DATA_DIHAPUS);
			return;
		}
		
		user = listbox.getSelectedItem().getValue();
		confirmDelete(user.getName(), new EventListener<Event>() {
			
			@Override
			public void onEvent(Event event) throws Exception {
				// TODO Auto-generated method stub
				if (event.getName().equals("onOK")) {
					try {
						userRepository.delete(user);
						clearObject();
					} catch (Exception e) {
						showMessage("Data sudah digunakan");
						return;
					}
				}
			}
		});
	}
	
	@Listen("onChanging = #txtSearchName")
	public void searchName(InputEvent event) {
		loadData(event.getValue());
	}
	
	@Listen("onSelect = #cbStatus")
	public void cariStatus() {
		loadData(txtSearchName.getValue());
	}
	
	@Listen("onSelect = #listbox")
	public void pilih() {
		user = listbox.getSelectedItem().getValue();

		btnFirst.setDisabled(getIndexSelected() == 0 ? true : getIndexSelected() == (listModel.size() - 1) ? false : false);
		btnPrev.setDisabled(getIndexSelected() == 0 ? true : getIndexSelected() == (listModel.size() - 1) ? false : false);
		btnNext.setDisabled(getIndexSelected() == (listModel.size() - 1) ? true : getIndexSelected() == 0 ? false : false);
		btnLast.setDisabled(getIndexSelected() == (listModel.size() - 1) ? true : getIndexSelected() == 0 ? false : false);
		
		getUser(user);
	}
	
	@Listen("onClick = #btnFirst")
	public void first() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(0);
		pilih();
	}
	
	@Listen("onClick = #btnPrev")
	public void prev() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(getIndexSelected() - 1);
		pilih();
	}
	
	@Listen("onClick = #btnNext")
	public void next() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(getIndexSelected() + 1);
		pilih();
	}
	
	@Listen("onClick = #btnLast")
	public void last() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(listModel.size() - 1);
		pilih();
	}
	
	@Listen("onSort = #listbox > listhead > listheader")
	public void sort() {
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
		listModel.clearSelection();
	}
	
	private int getIndexSelected() {
		if (user == null)
			return -1;
		
		return listModel.indexOf(user);				
	}
	
	private void formView(boolean isForm) {
		gridForm.setVisible(isForm);
		vLayoutList.setVisible(!isForm);
		btnBatal.setTooltiptext(isForm ? TEXT_BATAL : TEXT_BARU);
		btnBatal.setImage(isForm ? IMG_BATAL_32 : IMG_BARU_32);
		btnBatal.setLabel(isForm ? TEXT_BATAL : TEXT_BARU);
		
		btnView.setTooltiptext(isForm ? TEXT_LIST : TEXT_FORM);
		btnView.setLabel(isForm ? TEXT_LIST : TEXT_FORM);
		btnView.setImage(isForm ? IMG_LIST_32 : IMG_FORM_32);
	}
	
	private void loadDataByParam(Integer id) {
		List<User> list = new ArrayList<>();
		list.add(userRepository.findByUserId(id));
		listModel = new ListModelList<>(list);
		listbox.setModel(listModel);
		renderItem();
		listbox.setSelectedIndex(0);
	}
	
	private void loadData(String name) {
		List<User> list;
		
		boolean active = cbStatus.getSelectedIndex() == 1;
		if (cbStatus.getSelectedIndex() == 0)
			list = userRepository.findByNameContainingAllIgnoreCaseOrderByNameAsc(name);
		else
			list = userRepository.findByNameContainingAndActiveAllIgnoreCaseOrderByNameAsc(name, active);
		
		listModel = new ListModelList<>(list);
		listbox.setModel(listModel);
		renderItem();
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
	}
	
	private void renderItem() {
		listbox.setItemRenderer(new ListitemRenderer<User>() {

			@Override
			public void render(Listitem listitem, User user, int row) throws Exception {
				// TODO Auto-generated method stub
				listitem.getChildren().clear();
				listitem.setValue(user);
				
				listitem.addEventListener(Events.ON_DOUBLE_CLICK, e -> {
					formView(true);
					getUser(user);
				});
				
				listitem.appendChild(lc(row + 1));
				listitem.appendChild(lc(user.getName()));
				listitem.appendChild(lc(user.getRole().getName()));
			}
		});
	}
	
	private void getUser(User user) {
		if (user == null)
			return;
		txtName.setValue(user.getName());
		cbRole.setValue(user.getRole().getName());
		txtPassword.setValue(user.getPassword());
		txtUlangPassword.setValue(user.getPassword());
		chkActive.setChecked(user.isActive());
		disableForm(gridForm, !user.isActive());
	}
	
	private void setUser() {
		if (user == null)
			user = new User();
		user.setName(txtName.getValue());
		user.setRole(cbRole.getSelectedItem().getValue());
		user.setPassword(txtPassword.getValue());
		user.setActive(chkActive.isChecked());
	}
	
	private boolean validasi() {
		if (Strings.isBlank(txtName.getValue())) {
			showMessage("Nama masih kosong");
			return false;
		}
		
		if (!txtPassword.getValue().equals(txtUlangPassword.getValue())) {
			showMessage("Password tidak cocok");
			return false;
		}
		
		if (cbRole.getSelectedIndex() < 0) {
			showMessage("Role belum dipilih");
			return false;
		}
		
		return true;
	}
	
	private void clearObject() {
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
		clearForm(gridForm);
		loadData(txtSearchName.getValue());
		user = new User();
	}
	
}
