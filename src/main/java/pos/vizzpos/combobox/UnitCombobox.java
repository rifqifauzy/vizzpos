package pos.vizzpos.combobox;

import java.util.List;

import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;

import pos.vizzpos.entity.Unit;
import pos.vizzpos.repository.UnitRepository;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class UnitCombobox extends CoreCombobox {

	private static final long serialVersionUID = 3186498666283494660L;
	
	@WireVariable private UnitRepository unitRepository;
	
	@Override
	public void doAfterCompose(Combobox comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		setContext(refresh, view);	
		loadItem();
		
		refresh.addEventListener(Events.ON_CLICK, evt -> loadItem());
		
		view.addEventListener(Events.ON_CLICK, evt -> {
			if (getSelf().getSelectedIndex() >= 0) {
				Unit unit = getSelf().getSelectedItem().getValue();
				if (unit != null)
					mainWindow.loadContent(ZUL_MASTER_UNIT, "Unit", tsContent, tpsContent, unit);
			}
		});
	}
	
	private void loadItem() {
		getSelf().getItems().clear();
		List<Unit> list = unitRepository.findByActiveOrderByNameAsc(true);
		for (Unit unit : list) {
			Comboitem cb = new Comboitem(unit.getName());
			cb.setValue(unit);
			cb.setParent(this.getSelf());
		}
	}
}
