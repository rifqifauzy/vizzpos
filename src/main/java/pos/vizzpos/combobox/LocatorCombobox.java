package pos.vizzpos.combobox;

import java.util.List;

import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;

import pos.vizzpos.entity.Locator;
import pos.vizzpos.repository.LocatorRepository;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class LocatorCombobox extends CoreCombobox {

	private static final long serialVersionUID = -6660898570111521035L;
	
	@WireVariable private LocatorRepository locatorRepository;
	
	@Override
	public void doAfterCompose(Combobox comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		setContext(refresh, view);	
		loadItem();
		
		refresh.addEventListener(Events.ON_CLICK, evt -> loadItem());
		
		view.addEventListener(Events.ON_CLICK, evt -> {
			if (getSelf().getSelectedIndex() >= 0) {
				Locator locator = getSelf().getSelectedItem().getValue();
				if (locator != null)
					mainWindow.loadContent(ZUL_MASTER_LOCATOR, "Locator", tsContent, tpsContent, locator);
			}
		});
	}
	
	private void loadItem() {
		getSelf().getItems().clear();
		List<Locator> list = locatorRepository.findByActiveOrderByNameAsc(true);
		for (Locator locator : list) {
			Comboitem cb = new Comboitem(locator.getName());
			cb.setValue(locator);
			cb.setParent(this.getSelf());
		}
	}
}
