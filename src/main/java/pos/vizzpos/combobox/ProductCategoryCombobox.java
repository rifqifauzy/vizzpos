package pos.vizzpos.combobox;

import java.util.List;

import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;

import pos.vizzpos.entity.ProductCategory;
import pos.vizzpos.repository.ProductCategoryRepository;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ProductCategoryCombobox extends CoreCombobox {

	private static final long serialVersionUID = 3186498666283494660L;
	
	@WireVariable private ProductCategoryRepository productCategoryRepository;
	
	@Override
	public void doAfterCompose(Combobox comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		setContext(refresh, view);	
		loadItem();
		
		refresh.addEventListener(Events.ON_CLICK, evt -> loadItem());
		
		view.addEventListener(Events.ON_CLICK, evt -> {
			if (getSelf().getSelectedIndex() >= 0) {
				ProductCategory productCategory = getSelf().getSelectedItem().getValue();
				if (productCategory != null)
					mainWindow.loadContent(ZUL_MASTER_PRODUCT_CATEGORY, "Product Category", tsContent, tpsContent, productCategory);
			}
		});
	}
	
	private void loadItem() {
		getSelf().getItems().clear();
		List<ProductCategory> list = productCategoryRepository.findByActiveOrderByNameAsc(true);
		for (ProductCategory productCategory : list) {
			Comboitem cb = new Comboitem(productCategory.getName());
			cb.setValue(productCategory);
			cb.setParent(this.getSelf());
		}
	}
}
