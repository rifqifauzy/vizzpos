package pos.vizzpos.combobox;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanels;
import org.zkoss.zul.Tabs;
import org.zkoss.zul.Window;

import pos.vizzpos.MainWindow;
import pos.vizzpos.utils.Constans;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public abstract class CoreCombobox extends SelectorComposer<Combobox> implements Constans {
	
	private static final long serialVersionUID = 7097338139024043880L;
	
	protected Menupopup menupopup = new Menupopup();
	protected Menuitem refresh = new Menuitem("Refresh", "/images/icon_control/refresh_16.png");
	protected Menuitem view = new Menuitem("View Data", "/images/icon_control/view_16.png");
	protected Tabs tsContent;
	protected Tabpanels tpsContent;
	
	protected MainWindow mainWindow = new MainWindow();
	
	@Override
	public void doAfterCompose(Combobox comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
	}

	protected void setContext(Menuitem...menuitems) {
		menupopup.setParent(getSelf().getParent());
		getSelf().setContext(menupopup);
		
		Window win = (Window) Path.getComponent("/winMain");
		Tabbox txContent = (Tabbox) win.getChildren().get(2).getChildren().get(0).getChildren().get(0).getChildren().get(0);
		tsContent = (Tabs) txContent.getChildren().get(0);
		tpsContent = (Tabpanels) txContent.getChildren().get(1);
		
		for (Menuitem menuitem : menuitems) {
			menupopup.appendChild(menuitem);
		}
	}
	
	protected Menuitem addMenuitem(String label) {
		Menuitem menuitem = new Menuitem(label);
		return menuitem;
	}

	public Menuitem getRefresh() {
		return refresh;
	}

	public Menuitem getView() {
		return view;
	}
	
	protected Connection getDBConnection(String driver, String url, String user, String pass) {
		Connection dbConnection = null;

		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		try {
			dbConnection = DriverManager.getConnection(url, user, pass);
			return dbConnection;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return dbConnection;
	}
	
}
