package pos.vizzpos.combobox;

import java.util.List;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;

import pos.vizzpos.entity.Role;
import pos.vizzpos.repository.RoleRepository;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class RoleCombobox extends CoreCombobox {

	private static final long serialVersionUID = 3186498666283494660L;
	
	@WireVariable private RoleRepository roleRepository;
	
	@Override
	public void doAfterCompose(Combobox comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		setContext(refresh, view);	
		loadItem();
		
		refresh.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				// TODO Auto-generated method stub
				loadItem();
			}
		});
		
		view.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				// TODO Auto-generated method stub
				if (getSelf().getSelectedIndex() >= 0) {
					Role role = getSelf().getSelectedItem().getValue();
					if (role != null)
						mainWindow.loadContent(ZUL_ROLE, "Role", tsContent, tpsContent, role);
				}
			}
		});
	}
	
	private void loadItem() {
		getSelf().getItems().clear();
		List<Role> list = roleRepository.findByActiveOrderByNameAsc(true);
		for (Role role : list) {
			Comboitem cb = new Comboitem(role.getName());
			cb.setValue(role);
			cb.setParent(this.getSelf());
		}
	}
}
