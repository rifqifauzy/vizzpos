package pos.vizzpos;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Auxheader;
import org.zkoss.zul.Center;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Include;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Tabpanels;
import org.zkoss.zul.Tabs;
import org.zkoss.zul.Toolbarbutton;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treecol;
import org.zkoss.zul.Treefoot;
import org.zkoss.zul.Treefooter;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;
import org.zkoss.zul.impl.NumberInputElement;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import pos.vizzpos.entity.Configuration;
import pos.vizzpos.entity.User;
import pos.vizzpos.repository.CompanyRepository;
import pos.vizzpos.repository.ConfigurationRepository;
import pos.vizzpos.utility.DBConnection;
import pos.vizzpos.utils.Constans;
import pos.vizzpos.utils.Excel;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public abstract class CoreWindow extends SelectorComposer<Window> implements Constans {

	private static final long serialVersionUID = -67549907961299699L;
	
	protected SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);		
	protected SimpleDateFormat sdfTipeDokumen = new SimpleDateFormat(DATE_FORMAT_TIPE_DOKUMEN);
	
	protected Tabs tsContent;
	protected Tabpanels tpsContent;
	
	protected Excel excel = new Excel();
	
	@Wire protected Center center;
	
	@Wire protected Toolbarbutton btnBatal, btnSimpan, btnHapus, btnView, btnRefresh, btnFirst, btnPrev, btnNext, btnLast;
	
	@WireVariable protected CompanyRepository infoPerusahaanRepository;
	@WireVariable protected ConfigurationRepository configurationRepository;
	
	protected Configuration configuration;
	
	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		Window win = (Window) Path.getComponent("/winMain");
		
		configuration = configurationRepository.count() > 0 ? configurationRepository.findAll().get(0) : null;
		
		if (win != null) {
			Tabbox txContent = (Tabbox) win.getChildren().get(2).getChildren().get(0).getChildren().get(0).getChildren().get(0);
			tsContent = (Tabs) txContent.getChildren().get(0);
			tpsContent = (Tabpanels) txContent.getChildren().get(1);
		}
	}

	protected void clearSelection(Listbox listbox, Toolbarbutton...toolbarbuttons) {
		listbox.setSelectedIndex(-1);
		
		for (Toolbarbutton toolbarbutton : toolbarbuttons) {
			toolbarbutton.setDisabled(true);
		}
	}
	
	protected void clearSelection(Tree tree, Toolbarbutton...toolbarbuttons) {
		tree.clearSelection();
		
		for (Toolbarbutton toolbarbutton : toolbarbuttons) {
			toolbarbutton.setDisabled(true);
		}
	}
	
	protected void clearSelection(Toolbarbutton...toolbarbuttons) {
		for (Toolbarbutton toolbarbutton : toolbarbuttons) {
			toolbarbutton.setDisabled(true);
		}
	}
	
	protected void loadContent(String zul, String title) {
		Include inc = new Include(zul);
		Tab tab = new Tab(title);
		tab.setParent(tsContent);
		tab.setClosable(true);
		
		Tabpanel tabpanel = new Tabpanel();
		tabpanel.appendChild(inc);
		tabpanel.setHeight("100%");
		tabpanel.setParent(tpsContent);
		
		tab.setSelected(true);
	}
	
	protected void loadContent(String zul, String title, Object param) {
		Session s = Sessions.getCurrent();
		s.setAttribute(param.getClass().getName(), param);
		
		Include inc = new Include(zul);
		Tab tab = new Tab(title);
		tab.setParent(tsContent);
		tab.setClosable(true);
		
		Tabpanel tabpanel = new Tabpanel();
		tabpanel.appendChild(inc);
		tabpanel.setHeight("100%");
		tabpanel.setParent(tpsContent);
		
		tab.setSelected(true);
	}

	protected void loadContent(String zul, String title, Tabs tabParent, Tabpanels tabpanelParent) {
		Include inc = new Include(zul);
		Tab tab = new Tab(title);
		tab.setParent(tabParent);
		tab.setClosable(true);
		
		Tabpanel tabpanel = new Tabpanel();
		tabpanel.appendChild(inc);
		tabpanel.setHeight("100%");
		tabpanel.setParent(tabpanelParent);
		
		tab.setSelected(true);
	}
	
	protected void loadContent(String zul, String title, Tabs tabParent, Tabpanels tabpanelParent, Object param) {
		Session s = Sessions.getCurrent();
		s.setAttribute(param.getClass().getName(), param);
		
		Include inc = new Include(zul);
		Tab tab = new Tab(title);
		tab.setParent(tabParent);
		tab.setClosable(true);
		
		Tabpanel tabpanel = new Tabpanel();
		tabpanel.appendChild(inc);
		tabpanel.setHeight("100%");
		tabpanel.setParent(tabpanelParent);
		
		tab.setSelected(true);
	}
	
	protected void loadReport(String zul, String title, String jasperSrc, Map<String, Object> parameters) {
		Session s = Sessions.getCurrent();
		s.setAttribute(ATTR_PARAMETERS, parameters);
		s.setAttribute(ATTR_TEMPLATE, jasperSrc);
		
		Include inc = new Include(zul);
		Tab tab = new Tab(title);
		tab.setParent(tsContent);
		tab.setClosable(true);
		
		Tabpanel tabpanel = new Tabpanel();
		tabpanel.appendChild(inc);
		tabpanel.setHeight("100%");
		tabpanel.setParent(tpsContent);
		
		tab.setSelected(true);
	}
	
	protected Object getController(Component comp, String attr){
		return comp.getAttribute(attr);
	}

	protected Object getParameter(String attribute) {
		Session session = Sessions.getCurrent();
		return session.getAttribute(attribute);
	}
	
	protected Object getParameter(Class<?> cls) {
		Session session = Sessions.getCurrent();
		return session.getAttribute(cls.getName());
	}
	
	protected void removeParameter(Class<?> cls) {
		Session session = Sessions.getCurrent();
		session.removeAttribute(cls.getName());
	}

	protected boolean isValidEmailAddress(String email) {
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}
	
	protected User getUser() {
		Execution ex = Executions.getCurrent();
		Session s = ex.getSession();
		
		User user = (User)s.getAttribute(Constans.USER_LOGIN);
		return user;
	}
	
	protected void showMessage(String message) {
		Messagebox.show(message, "Pesan", Messagebox.OK, Messagebox.INFORMATION);
	}
	
	protected void confirmDelete(String name, EventListener<Event> eventListener) {
		Messagebox.show("Hapus " + name + "?", "Konfirmasi", Messagebox.OK | Messagebox.NO,
				Messagebox.QUESTION, eventListener);
	}
	
	protected void confirm(String message, EventListener<Event> eventListener) {
		Messagebox.show(message, "Konfirmasi", Messagebox.OK | Messagebox.NO,
				Messagebox.QUESTION, eventListener);
	}
	
	protected Listcell lc(final String label) {
		Listcell listcell = new Listcell(label != null ? label : EMPTY_STRING);
		listcell.setStyle("white-space: nowrap;");
		return listcell;
	}
	
	protected Listcell lc(final Component component) {
		Listcell listcell = new Listcell();
		component.setParent(listcell);
		return listcell;
	}
	
	protected Listcell lc(final Number number) {
		return lc(formatNumber(number));
	}
	
	protected Listcell lc(final Date date) {
		return lc(formatDate(date));
	}
	
	protected Listcell lc(Object object){
		if(object == null){
			return lc(" ");
		}else if(object instanceof String){
			return lc((String) object);
		}else if(object instanceof Number){
			return lc((Number) object);
		}else if(object instanceof Date){
			return lc((Date) object);
		}else if(object instanceof Component){
			return lc((Component) object);
		}else{
			return lc(" ");
		}
	}

	protected void setVisibleComponent(boolean visible, Component...components) {
		for (Component component : components) {
			component.setVisible(visible);
		}
	}
	
	protected Auxheader axr(String label) {
		Auxheader ax = new Auxheader(label);
		return ax;
	}
	
	protected Auxheader axr(String label, int colspan) {
		Auxheader ax = new Auxheader(label);
		ax.setColspan(colspan);
		return ax;
	}
	
	protected Auxheader axr(String label, int colspan, String style) {
		Auxheader ax = new Auxheader(label);
		ax.setColspan(colspan);
		ax.setStyle(style);
		return ax;
	}
	
	protected Treecol tcl(String label, String width, String align, String style) {
		Treecol treecol = new Treecol(label);
		treecol.setStyle(style);
		treecol.setAlign(align);
		treecol.setWidth(width);
		return treecol;
	}
	
	protected Treecell tc(String label, int span){
		Treecell treecell = new Treecell();
		treecell.setLabel(label);
		treecell.setSpan(span);
		return treecell;
	}
	
	protected Treecell tc(String label){
		Treecell treecell = new Treecell();
		treecell.setLabel(label);
		return treecell;
	}
	
	protected Treecell tc(String label, String style){
		Treecell treecell = new Treecell();
		treecell.setLabel(label);
		treecell.setStyle(style);
		return treecell;
	}
	
	protected Treecell tc(String label, String image, String styleImage, String tooltipImg){
		Treecell treecell = new Treecell();
		treecell.setLabel(label);
		
		Image img = new Image(image);
		img.setStyle(styleImage);
		img.setTooltiptext(tooltipImg);
		treecell.appendChild(img);
		return treecell;
	}
	
	protected Treecell tc(Component comp, String image, String styleImage, String tooltipImg){
		Treecell treecell = new Treecell();
		Hbox hbox = new Hbox();
		treecell.appendChild(hbox);
		hbox.appendChild(comp);
		
		Image img = new Image(image);
		img.setStyle(styleImage);
		img.setTooltiptext(tooltipImg);
		hbox.appendChild(img);
		return treecell;
	}
	
	protected Treecell tc(String label, String style, int span){
		Treecell treecell = new Treecell();
		treecell.setLabel(label);
		treecell.setStyle(style);
		treecell.setSpan(span);
		return treecell;
	}
	
	protected Treecell tc(Date date){
		Treecell treecell = new Treecell();
		treecell.setLabel(formatDate(date));
		return treecell;
	}
	
	protected Treecell tc(final Number number){
		Treecell treecell = new Treecell();
		treecell.setLabel(formatNumber(number));
		return treecell;
	}
	
	protected Treecell tc(Component component){
		Treecell treecell = new Treecell();
		treecell.appendChild(component);
		return treecell;
	}

	protected Treefooter tfr(String string){
		Treefooter treefooter = new Treefooter();
		treefooter.setLabel(string);
		return treefooter;
	}
	
	protected Treefooter tfr(String string, String style){
		Treefooter treefooter = new Treefooter();
		treefooter.setLabel(string);
		treefooter.setStyle(style);
		return treefooter;
	}
	
	protected Treefooter tfr(String string, String style, int span){
		Treefooter treefooter = new Treefooter();
		treefooter.setLabel(string);
		treefooter.setStyle(style);
		treefooter.setSpan(span);
		return treefooter;
	}
	
	protected Treefooter tfr(String string, int span){
		Treefooter treefooter = new Treefooter();
		treefooter.setLabel(string);
		treefooter.setSpan(span);
		return treefooter;
	}
	
	protected Treefooter tfr(Number number){
		Treefooter treefooter = new Treefooter();
		treefooter.setLabel(formatNumber(number));
		return treefooter;
	}
	
	protected Treefooter tfr(Number number, String style){
		Treefooter treefooter = new Treefooter();
		treefooter.setLabel(formatNumber(number));
		treefooter.setStyle(style);
		return treefooter;
	}
	
	protected void clearTreefoot(Tree tree) {
		for (Component cmp : tree.getChildren()) {
			if (cmp instanceof Treefoot)
				tree.removeChild(cmp);
		}
	}
	
	protected void clearForm(Component parent) {
		if (parent instanceof InputElement) {
			((InputElement) parent).setRawValue(null);
		}

		List<Component> list = parent.getChildren();
		for (Component child : list) {
			clearForm(child);
		}
	}
	
	protected void disableForm(Component parent, boolean isActive) {
		if (parent instanceof InputElement) {
			((InputElement) parent).setDisabled(isActive);
		}

		List<Component> list = parent.getChildren();
		for (Component child : list) {
			disableForm(child, isActive);
		}
	}
	
	protected void clearForm() {
		List<InputElement> list = getInputElementList(getSelf());
		for (InputElement inputElement : list) {
			inputElement.setRawValue(null);
		}
	}
	
	protected List<InputElement> getInputElementList(Component parent) {
		Collection<Component> fellows = parent.getFellows();
		List<InputElement> elements = new ArrayList<InputElement>();
		for (Component c : fellows) {
			if (c instanceof InputElement) {
				elements.add((InputElement) c);
			}

		}
		return elements;
	}
	
	protected void setWidthComponent(String width, InputElement...elements) {
		for (InputElement element : elements) {
			element.setWidth(width);
		}
	}
	
	protected void setInplaceComponent(boolean isInplace, InputElement...elements) {
		for (InputElement element : elements) {
			element.setInplace(isInplace);
		}
	}
	
	protected void setNumberFormatComponent(NumberInputElement...elements) {
		for (NumberInputElement numberInputElement : elements) {
			numberInputElement.setFormat(NUMBER_FORMAT);
		}
	}
	
	protected String formatDate(Date date) {
		if (date == null)
			return EMPTY_STRING;
		return sdf.format(date);
	}

	protected String getChar(int i) {
	    return i > 0 && i < 27 ? String.valueOf((char)(i + 64)).toLowerCase() : null;
	}
	
	protected String formatNumber(Number number) {
		if(number instanceof Double){
			number = Double.isNaN((Double)number) ? 0d : Double.isInfinite((Double)number) ? 0d : number;
		}
		
		DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
		symbols.setGroupingSeparator('.');
		DecimalFormat formatter = new DecimalFormat(NUMBER_FORMAT, symbols);
		return formatter.format(number);
	}
	
	protected void directPrint(String jasper, Map<String, Object> parameters) {
		try {
			@SuppressWarnings("deprecation")
			ServletContext context = (ServletContext) Executions.getCurrent().getDesktop().getWebApp().getNativeContext();
			String contextPath = context.getRealPath("/");
			JasperPrint jasperPrint = JasperFillManager.fillReport(contextPath + jasper, 
					parameters, DBConnection.getConnectionStatic());
			JasperPrintManager.printReport(jasperPrint, false);
		} catch (JRException e) {
			e.printStackTrace();
		}
	}
	
	protected Object getCellValue(HSSFRow row, int index) {
		if (row.getCell(index) != null) {
			if (row.getCell(index).getCellType() == HSSFCell.CELL_TYPE_STRING) {
				return row.getCell(index).getStringCellValue();
			} else if (row.getCell(index).getCellType() == HSSFCell.CELL_TYPE_BOOLEAN) {
				return row.getCell(index).getBooleanCellValue();
			} else if (row.getCell(index).getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
				if (HSSFDateUtil.isCellDateFormatted(row.getCell(index))) {
					return row.getCell(index).getDateCellValue();
			    } else {
			    	return row.getCell(index).getNumericCellValue();
			    }
			} else if (row.getCell(index).getCellType() == HSSFCell.CELL_TYPE_ERROR) {
				return row.getCell(index).getErrorCellValue();
			} else if (row.getCell(index).getCellType() == HSSFCell.CELL_TYPE_BLANK) {
				return null;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	protected String getObjectString(Object obj) {
		if (obj != null) {
			if (obj instanceof Double)
				return String.valueOf(((Double) obj).intValue());
			else if (obj instanceof Integer)
				return String.valueOf(obj);
			else if (obj instanceof String)
				return (String) obj;
			else 
				return null;
		} else {
			return null;
		}
	}

	protected Double getObjectDouble(Object obj) {
		if (obj != null) {
			if (obj instanceof Double)
				return (Double) obj;
			else if (obj instanceof Integer)
				return ((Integer) obj).doubleValue();
			else if (obj instanceof String)
				return ((String) obj).equalsIgnoreCase(EMPTY_STRING) ? null : Double.valueOf((String) obj);
			else 
				return null;
		} else {
			return null;
		}
	}
}
