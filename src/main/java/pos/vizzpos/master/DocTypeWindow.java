package pos.vizzpos.master;

import java.util.ArrayList;
import java.util.List;

import org.springframework.orm.jpa.JpaSystemException;
import org.zkoss.lang.Strings;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import pos.vizzpos.CoreWindow;
import pos.vizzpos.entity.DocType;
import pos.vizzpos.repository.DocTypeRepository;
import pos.vizzpos.utils.TransactionType;

@SuppressWarnings("serial")
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class DocTypeWindow extends CoreWindow {
	
	@Wire private Textbox txtName, txtSearchName, txtPrefix;
	@Wire private Listbox listbox;
	@Wire private Intbox intSeq;
	@Wire private Combobox cbType;
	@Wire private Grid gridForm;
	@Wire private Vlayout vLayoutList;
	
	@WireVariable private DocTypeRepository docTypeRepository;
	
	private DocType docType;
	
	private ListModelList<DocType> listModel;

	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		docType = (DocType) getParameter(DocType.class);
		loadType();
		if (docType != null) {
			formView(true);
			getDocType(docType);
			loadDataByParam(docType.getDocTypeId());
			removeParameter(DocType.class);
		} else {
			formView(false);
			loadData(txtSearchName.getValue());
		}
	}
	
	@Listen("onClick = #btnSimpan")
	public void simpan() {
		if (vLayoutList.isVisible())
			return;
		
		if (validasi()) {
			setDocType();
			
			try {
				docTypeRepository.save(docType);
			} catch (Exception e) {
				if (e instanceof JpaSystemException) {
					showMessage(txtName.getValue() + " sudah ada");
					return;
				}
			}
			
			formView(false);
			loadData(txtSearchName.getValue());
			clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
			docType = new DocType();
			clearForm(gridForm);
		}
	}
	
	@Listen("onClick = #btnView")
	public void view() {
		formView(!gridForm.isVisible());
	}
	
	@Listen("onClick = #btnBatal")
	public void batal() {
		formView(!gridForm.isVisible());
		clearObject();
	}

	@Listen("onClick = #btnRefresh")
	public void refresh() {
		if (gridForm.isVisible() && (docType == null || docType.getDocTypeId() == null))
			return;
		
		if (vLayoutList.isVisible()) {
			loadData(txtSearchName.getValue());
		} else { 
			if (docType == null)
				return;
			
			docTypeRepository.flush();
			docType = docTypeRepository.findOne(docType.getDocTypeId());
			getDocType(docType);
		}
	}
	
	@Listen("onClick = #btnHapus")
	public void hapus() {
		if (vLayoutList.isVisible() && listbox.getSelectedIndex() < 0) {
			showMessage(PILIH_DATA_DIHAPUS);
			return;
		}
		
		docType = listbox.getSelectedItem().getValue();
		confirmDelete(docType.getName(), evt -> {
			if (evt.getName().equals("onOK")) {
				try {
					docTypeRepository.delete(docType);
					clearObject();
				} catch (Exception e) {
					showMessage("Data sudah digunakan");
					return;
				}
			}
		});
	}
	
	@Listen("onChanging = #txtSearchName")
	public void searchName(InputEvent event) {
		loadData(event.getValue());
	}
	
	@Listen("onSelect = #listbox")
	public void pilih() {
		docType = listbox.getSelectedItem().getValue();

		btnFirst.setDisabled(getIndexSelected() == 0 ? true : getIndexSelected() == (listModel.size() - 1) ? false : false);
		btnPrev.setDisabled(getIndexSelected() == 0 ? true : getIndexSelected() == (listModel.size() - 1) ? false : false);
		btnNext.setDisabled(getIndexSelected() == (listModel.size() - 1) ? true : getIndexSelected() == 0 ? false : false);
		btnLast.setDisabled(getIndexSelected() == (listModel.size() - 1) ? true : getIndexSelected() == 0 ? false : false);
		
		getDocType(docType);
	}
	
	@Listen("onClick = #btnFirst")
	public void first() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(0);
		pilih();
	}
	
	@Listen("onClick = #btnPrev")
	public void prev() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(getIndexSelected() - 1);
		pilih();
	}
	
	@Listen("onClick = #btnNext")
	public void next() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(getIndexSelected() + 1);
		pilih();
	}
	
	@Listen("onClick = #btnLast")
	public void last() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(listModel.size() - 1);
		pilih();
	}
	
	@Listen("onSort = #listbox > listhead > listheader")
	public void sort() {
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
		listModel.clearSelection();
	}
	
	private void loadType() {
		for (TransactionType e : TransactionType.values()) {
			Comboitem ci = new Comboitem(TransactionType.getNameByCode(e.getValue()));
			ci.setValue(e.getValue());
			ci.setParent(cbType);
		}
	}
	
	private int getIndexSelected() {
		if (docType == null)
			return -1;
		
		return listModel.indexOf(docType);				
	}
	
	private void formView(boolean isForm) {
		gridForm.setVisible(isForm);
		vLayoutList.setVisible(!isForm);
		btnBatal.setTooltiptext(isForm ? TEXT_BATAL : TEXT_BARU);
		btnBatal.setImage(isForm ? IMG_BATAL_32 : IMG_BARU_32);
		btnBatal.setLabel(isForm ? TEXT_BATAL : TEXT_BARU);
		
		btnView.setTooltiptext(isForm ? TEXT_LIST : TEXT_FORM);
		btnView.setLabel(isForm ? TEXT_LIST : TEXT_FORM);
		btnView.setImage(isForm ? IMG_LIST_32 : IMG_FORM_32);
	}
	
	private void loadDataByParam(Integer id) {
		List<DocType> list = new ArrayList<>();
		list.add(docTypeRepository.findByDocTypeId(id));
		listModel = new ListModelList<>(list);
		listbox.setModel(listModel);
		renderItem();
		listbox.setSelectedIndex(0);
	}
	
	private void loadData(String name) {
		List<DocType> list;
		
		list = docTypeRepository.findByNameContainingAllIgnoreCaseOrderByNameAsc(name);
		
		listModel = new ListModelList<>(list);
		listbox.setModel(listModel);
		renderItem();
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
	}
	
	private void renderItem() {
		listbox.setItemRenderer((Listitem listitem, DocType docType, int row) -> {
			listitem.getChildren().clear();
			listitem.setValue(docType);
			
			listitem.addEventListener(Events.ON_DOUBLE_CLICK, evt -> {
				formView(true);
				getDocType(docType);
			});
			
			listitem.appendChild(lc(row + 1));
			listitem.appendChild(lc(docType.getName()));
			listitem.appendChild(lc(docType.getPrefix()));
			listitem.appendChild(lc(docType.getSequence()));
			listitem.appendChild(lc(TransactionType.getNameByCode(docType.getType())));
		});
	}
	
	private void getDocType(DocType docType) {
		if (docType == null)
			return;
		txtName.setValue(docType.getName());
		txtPrefix.setValue(docType.getPrefix());
		intSeq.setValue(docType.getSequence());
		cbType.setValue(TransactionType.getNameByCode(docType.getType()));
	}
	
	private void setDocType() {
		if (docType == null)
			docType = new DocType();
		docType.setName(txtName.getValue());
		docType.setPrefix(txtPrefix.getValue());
		docType.setSequence(intSeq.getValue());
		docType.setType(cbType.getSelectedItem().getValue());
	}
	
	private boolean validasi() {
		if (Strings.isBlank(txtName.getValue())) {
			showMessage("Nama masih kosong");
			return false;
		}
		
		return true;
	}
	
	private void clearObject() {
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
		clearForm(gridForm);
		loadData(txtSearchName.getValue());
		docType = new DocType();
	}
	
}
