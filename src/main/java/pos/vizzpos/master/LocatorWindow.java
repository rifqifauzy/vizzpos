package pos.vizzpos.master;

import java.util.ArrayList;
import java.util.List;

import org.springframework.orm.jpa.JpaSystemException;
import org.zkoss.lang.Strings;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import pos.vizzpos.CoreWindow;
import pos.vizzpos.entity.Locator;
import pos.vizzpos.repository.LocatorRepository;

@SuppressWarnings("serial")
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class LocatorWindow extends CoreWindow {
	
	@Wire private Textbox txtCode, txtName, txtAddress, txtDescription, txtSearchName, txtSearchCode;
	@Wire private Checkbox chkActive;
	@Wire private Listbox listbox;
	@Wire private Combobox cbStatus;
	@Wire private Grid gridForm;
	@Wire private Vlayout vLayoutList;
	
	@WireVariable private LocatorRepository locatorRepository;
	
	private Locator locator;
	
	private ListModelList<Locator> listModel;

	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		cbStatus.setSelectedIndex(0);
		locator = (Locator) getParameter(Locator.class);
		if (locator != null) {
			formView(true);
			getLocator(locator);
			loadDataByParam(locator.getLocatorId());
			removeParameter(Locator.class);
		} else {
			formView(false);
			loadData(txtSearchName.getValue(), txtSearchCode.getValue());
		}
	}
	
	@Listen("onClick = #chkActive")
	public void aktifForm() {
		disableForm(gridForm, !chkActive.isChecked());
	}
	
	@Listen("onClick = #btnSimpan")
	public void simpan() {
		if (vLayoutList.isVisible())
			return;
		
		if (validasi()) {
			setLocator();
			
			try {
				locatorRepository.save(locator);
			} catch (Exception e) {
				if (e instanceof JpaSystemException) {
					showMessage(txtName.getValue() + " sudah ada");
					return;
				}
			}
			
			formView(false);
			loadData(txtSearchName.getValue(), txtSearchCode.getValue());
			clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
			locator = new Locator();
			clearForm(gridForm);
		}
	}
	
	@Listen("onClick = #btnView")
	public void view() {
		formView(!gridForm.isVisible());
	}
	
	@Listen("onClick = #btnBatal")
	public void batal() {
		formView(!gridForm.isVisible());
		clearObject();
	}

	@Listen("onClick = #btnRefresh")
	public void refresh() {
		if (gridForm.isVisible() && (locator == null || locator.getLocatorId() == null))
			return;
		
		if (vLayoutList.isVisible()) {
			loadData(txtSearchName.getValue(), txtSearchCode.getValue());
		} else { 
			if (locator == null)
				return;
			
			locatorRepository.flush();
			locator = locatorRepository.findOne(locator.getLocatorId());
			getLocator(locator);
		}
	}
	
	@Listen("onClick = #btnHapus")
	public void hapus() {
		if (vLayoutList.isVisible() && listbox.getSelectedIndex() < 0) {
			showMessage(PILIH_DATA_DIHAPUS);
			return;
		}
		
		locator = listbox.getSelectedItem().getValue();
		confirmDelete(locator.getName(), evt -> {
			if (evt.getName().equals("onOK")) {
				try {
					locatorRepository.delete(locator);
					clearObject();
				} catch (Exception e) {
					showMessage("Data sudah digunakan");
					return;
				}
			}
		});
	}
	
	@Listen("onChanging = #txtSearchName")
	public void searchName(InputEvent event) {
		loadData(event.getValue(), txtSearchCode.getValue());
	}
	
	@Listen("onChanging = #txtSearchCode")
	public void searchCode(InputEvent event) {
		loadData(txtSearchName.getValue(), event.getValue());
	}
	
	@Listen("onSelect = #cbStatus")
	public void searchStatus() {
		loadData(txtSearchName.getValue(), txtSearchCode.getValue());
	}
	
	@Listen("onSelect = #listbox")
	public void pilih() {
		locator = listbox.getSelectedItem().getValue();

		btnFirst.setDisabled(getIndexSelected() == 0 ? true : getIndexSelected() == (listModel.size() - 1) ? false : false);
		btnPrev.setDisabled(getIndexSelected() == 0 ? true : getIndexSelected() == (listModel.size() - 1) ? false : false);
		btnNext.setDisabled(getIndexSelected() == (listModel.size() - 1) ? true : getIndexSelected() == 0 ? false : false);
		btnLast.setDisabled(getIndexSelected() == (listModel.size() - 1) ? true : getIndexSelected() == 0 ? false : false);
		
		getLocator(locator);
	}
	
	@Listen("onClick = #btnFirst")
	public void first() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(0);
		pilih();
	}
	
	@Listen("onClick = #btnPrev")
	public void prev() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(getIndexSelected() - 1);
		pilih();
	}
	
	@Listen("onClick = #btnNext")
	public void next() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(getIndexSelected() + 1);
		pilih();
	}
	
	@Listen("onClick = #btnLast")
	public void last() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(listModel.size() - 1);
		pilih();
	}
	
	@Listen("onSort = #listbox > listhead > listheader")
	public void sort() {
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
		listModel.clearSelection();
	}
	
	private int getIndexSelected() {
		if (locator == null)
			return -1;
		
		return listModel.indexOf(locator);				
	}
	
	private void formView(boolean isForm) {
		gridForm.setVisible(isForm);
		vLayoutList.setVisible(!isForm);
		btnBatal.setTooltiptext(isForm ? TEXT_BATAL : TEXT_BARU);
		btnBatal.setImage(isForm ? IMG_BATAL_32 : IMG_BARU_32);
		btnBatal.setLabel(isForm ? TEXT_BATAL : TEXT_BARU);
		
		btnView.setTooltiptext(isForm ? TEXT_LIST : TEXT_FORM);
		btnView.setLabel(isForm ? TEXT_LIST : TEXT_FORM);
		btnView.setImage(isForm ? IMG_LIST_32 : IMG_FORM_32);
	}
	
	private void loadDataByParam(Integer id) {
		List<Locator> list = new ArrayList<>();
		list.add(locatorRepository.findByLocatorId(id));
		listModel = new ListModelList<>(list);
		listbox.setModel(listModel);
		renderItem();
		listbox.setSelectedIndex(0);
	}
	
	private void loadData(String name, String code) {
		List<Locator> list;
		
		boolean active = cbStatus.getSelectedIndex() == 1;
		if (cbStatus.getSelectedIndex() == 0)
			list = locatorRepository.findByNameContainingAndCodeContainingAllIgnoreCaseOrderByNameAsc(name, code);
		else
			list = locatorRepository.findByNameContainingAndCodeContainingAndActiveAllIgnoreCaseOrderByNameAsc(name, code, active);
		
		listModel = new ListModelList<>(list);
		listbox.setModel(listModel);
		renderItem();
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
	}
	
	private void renderItem() {
		listbox.setItemRenderer((Listitem listitem, Locator locator, int row) -> {
			listitem.getChildren().clear();
			listitem.setValue(locator);
			
			listitem.addEventListener(Events.ON_DOUBLE_CLICK, evt -> {
				formView(true);
				getLocator(locator);
			});
			
			listitem.appendChild(lc(row + 1));
			listitem.appendChild(lc(locator.getCode()));
			listitem.appendChild(lc(locator.getName()));
			listitem.appendChild(lc(locator.getAddress()));
		});
	}
	
	private void getLocator(Locator locator) {
		if (locator == null)
			return;
		txtName.setValue(locator.getName());
		txtDescription.setValue(locator.getDescription());
		chkActive.setChecked(locator.isActive());
		txtAddress.setValue(locator.getAddress());
		txtCode.setValue(locator.getCode());
		
		disableForm(gridForm, !locator.isActive());
	}
	
	private void setLocator() {
		if (locator == null)
			locator = new Locator();
		locator.setName(txtName.getValue());
		locator.setDescription(txtDescription.getValue());
		locator.setActive(chkActive.isChecked());
		locator.setCode(txtCode.getValue());
		locator.setAddress(txtAddress.getValue());
	}
	
	private boolean validasi() {
		if (Strings.isBlank(txtCode.getValue())) {
			showMessage("Kode masih kosong");
			return false;
		}
		
		if (Strings.isBlank(txtName.getValue())) {
			showMessage("Nama masih kosong");
			return false;
		}
		
		return true;
	}
	
	private void clearObject() {
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
		clearForm(gridForm);
		loadData(txtSearchName.getValue(), txtSearchCode.getValue());
		locator = new Locator();
	}
	
}
