package pos.vizzpos.master;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.zkoss.lang.Strings;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.A;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Row;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timebox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import pos.vizzpos.CoreWindow;
import pos.vizzpos.entity.Promo;
import pos.vizzpos.entity.PromoManage;
import pos.vizzpos.entity.PromoManageLine;
import pos.vizzpos.repository.PromoManageLineRepository;
import pos.vizzpos.repository.PromoManageRepository;
import pos.vizzpos.repository.PromoRepository;
import pos.vizzpos.utils.PromoType;
import pos.vizzpos.utils.Utility;

@SuppressWarnings("serial")
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class PromoManageWindow extends CoreWindow {
	
	@Wire private Textbox txtName, txtDescription, txtSearchName;
	@Wire private Checkbox chkActive, chkByHour;
	@Wire private Datebox dtDateEffective;
	@Wire private Timebox tmFrom, tmTo;
	@Wire private Row rowHour;
	@Wire private Listbox listbox, listboxPromo;
	@Wire private Combobox cbStatus, cbPromo;
	@Wire private Grid gridForm;
	@Wire private Vlayout vLayoutList, vLayoutPromo;
	
	@WireVariable private PromoManageRepository promoManageRepository;
	@WireVariable private PromoManageLineRepository promoManageLineRepository;
	@WireVariable private PromoRepository promoRepository;
	
	private PromoManage promoManage;
	
	private List<PromoManageLine> lines = new ArrayList<>();
	private List<PromoManageLine> linesDelete = new ArrayList<>();
	
	private ListModelList<PromoManage> listModel;
	private ListModelList<PromoManageLine> modelLine;
	
	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		promoManage = (PromoManage) getParameter(PromoManage.class);
		loadPromo();
		cbStatus.setSelectedIndex(0);
		if (promoManage != null) {
			formView(true);
			getPromoManage(promoManage);
			loadDataByParam(promoManage.getPromoManageId());
			removeParameter(Promo.class);
		} else {
			formView(false);
			loadData(txtSearchName.getValue());
			clearObject();
		}
	}
	
	@Listen("onClick = #chkActive")
	public void aktifForm() {
		disableForm(gridForm, !chkActive.isChecked());
	}
	
	@Listen("onClick = #btnSimpan")
	public void simpan() {
		if (vLayoutList.isVisible())
			return;
		
		if (validasi()) {
			setPromoManage();
			
			promoManageRepository.save(promoManage);
			
			if (linesDelete.size() > 0)
				promoManageLineRepository.delete(linesDelete);
			
			lines.forEach(l -> l.setPromoManage(promoManage));
			promoManageLineRepository.save(lines);
			
			formView(false);
			loadData(txtSearchName.getValue());
			clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
			promoManage = new PromoManage();
			clearForm(gridForm);
		}
	}
	
	@Listen("onClick = #btnView")
	public void view() {
		formView(!gridForm.isVisible());
	}
	
	@Listen("onClick = #chkByHour")
	public void byHour() {
		rowHour.setVisible(chkByHour.isChecked());
	}
	
	@Listen("onClick = #btnBatal")
	public void batal() {
		formView(!gridForm.isVisible());
		clearObject();
	}

	@Listen("onClick = #btnRefresh")
	public void refresh() {
		if (gridForm.isVisible() && (promoManage == null || promoManage.getPromoManageId() == null))
			return;
		
		if (vLayoutList.isVisible()) {
			loadData(txtSearchName.getValue());
		} else { 
			if (promoManage == null)
				return;
			
			promoManageRepository.flush();
			promoManage = promoManageRepository.findOne(promoManage.getPromoManageId());
			getPromoManage(promoManage);
		}
	}
	
	@Listen("onClick = #btnHapus")
	public void hapus() {
		if (vLayoutList.isVisible() && listbox.getSelectedIndex() < 0) {
			showMessage(PILIH_DATA_DIHAPUS);
			return;
		}
		
		promoManage = listbox.getSelectedItem().getValue();
		confirmDelete(promoManage.getName(), evt -> {
			if (evt.getName().equals("onOK")) {
				try {
					promoManageRepository.delete(promoManage);
					clearObject();
				} catch (Exception e) {
					showMessage("Data sudah digunakan");
					return;
				}
			}
		});
	}
	
	@Listen("onChanging = #txtSearchName")
	public void searchName(InputEvent event) {
		loadData(event.getValue());
	}
	
	@Listen("onSelect = #cbStatus")
	public void searchStatus() {
		loadData(txtSearchName.getValue());
	}
	
	@Listen("onSelect = #listbox")
	public void pilih() {
		promoManage = listbox.getSelectedItem().getValue();

		btnFirst.setDisabled(getIndexSelected() == 0 ? true : getIndexSelected() == (listModel.size() - 1) ? false : false);
		btnPrev.setDisabled(getIndexSelected() == 0 ? true : getIndexSelected() == (listModel.size() - 1) ? false : false);
		btnNext.setDisabled(getIndexSelected() == (listModel.size() - 1) ? true : getIndexSelected() == 0 ? false : false);
		btnLast.setDisabled(getIndexSelected() == (listModel.size() - 1) ? true : getIndexSelected() == 0 ? false : false);
		
		getPromoManage(promoManage);
	}
	
	@Listen("onClick = #btnFirst")
	public void first() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(0);
		pilih();
	}
	
	@Listen("onClick = #btnPrev")
	public void prev() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(getIndexSelected() - 1);
		pilih();
	}
	
	@Listen("onClick = #btnNext")
	public void next() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(getIndexSelected() + 1);
		pilih();
	}
	
	@Listen("onClick = #btnLast")
	public void last() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(listModel.size() - 1);
		pilih();
	}
	
	@Listen("onSort = #listbox > listhead > listheader")
	public void sort() {
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
		listModel.clearSelection();
	}
	
	@Listen("onClick = #btnAddPromo")
	public void addPromo() {
		if (cbPromo.getSelectedIndex() < 0)
			return;
		
		Promo selected = cbPromo.getSelectedItem().getValue();
		if (lines.size() > 0) {
			Optional<PromoManageLine> optLine = lines.stream()
					.filter(e -> e.getPromo().getPromoId() == selected.getPromoId()).findFirst();
			if (optLine.isPresent())
				return;
		}
		
		PromoManageLine line = new PromoManageLine();
		line.setPromo(selected);
		lines.add(line);
		
		loadLines();
	}
	
	private void loadLines() {
		renderLines();
	}
	
	private void renderLines() {
		listboxPromo.setItemRenderer((Listitem listitem, PromoManageLine line, int row) -> {
			listitem.getChildren().clear();
			listitem.setValue(line);
			
			A aDelete = new A(EMPTY_STRING, "/images/icon_control/delete_16.png");
			aDelete.addEventListener(Events.ON_CLICK, evt -> {
				linesDelete.add(line);
				lines.remove(line);
				loadLines();
			});
			
			listitem.appendChild(lc(row + 1));
			listitem.appendChild(lc(line.getPromo().getName()));
			listitem.appendChild(lc(line.getPromo().getDescription()));
			listitem.appendChild(lc(PromoType.getNameByCode(line.getPromo().getType())));
			listitem.appendChild(lc(aDelete));
		});
		
		modelLine = new ListModelList<>(lines);
		listboxPromo.setModel(modelLine);
	}
	
	private void loadPromo() {
		List<Promo> promos = promoRepository.findAll();
		promos.stream().forEach(p -> {
			Comboitem it = new Comboitem(p.getName());
			it.setValue(p);
			it.setParent(cbPromo);
		});
	}
	
	private int getIndexSelected() {
		if (promoManage == null)
			return -1;
		
		return listModel.indexOf(promoManage);				
	}
	
	private void formView(boolean isForm) {
		gridForm.setVisible(isForm);
		vLayoutPromo.setVisible(isForm);
		vLayoutList.setVisible(!isForm);
		btnBatal.setTooltiptext(isForm ? TEXT_BATAL : TEXT_BARU);
		btnBatal.setImage(isForm ? IMG_BATAL_32 : IMG_BARU_32);
		btnBatal.setLabel(isForm ? TEXT_BATAL : TEXT_BARU);
		
		btnView.setTooltiptext(isForm ? TEXT_LIST : TEXT_FORM);
		btnView.setLabel(isForm ? TEXT_LIST : TEXT_FORM);
		btnView.setImage(isForm ? IMG_LIST_32 : IMG_FORM_32);
	}
	
	private void loadDataByParam(Integer id) {
		List<PromoManage> list = new ArrayList<>();
		list.add(promoManageRepository.findByPromoManageId(id));
		listModel = new ListModelList<>(list);
		listbox.setModel(listModel);
		renderItem();
		listbox.setSelectedIndex(0);
	}
	
	private void loadData(String name) {
		List<PromoManage> list;
		
		boolean active = cbStatus.getSelectedIndex() == 1;
		if (cbStatus.getSelectedIndex() == 0)
			list = promoManageRepository.findByNameContainingAllIgnoreCaseOrderByNameAsc(name);
		else
			list = promoManageRepository.findByNameContainingAndActiveAllIgnoreCaseOrderByNameAsc(name, active);
		
		listModel = new ListModelList<>(list);
		listbox.setModel(listModel);
		renderItem();
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
	}
	
	private void renderItem() {
		listbox.setItemRenderer((Listitem listitem, PromoManage promoManage, int row) -> {
			listitem.getChildren().clear();
			listitem.setValue(promoManage);
			
			listitem.addEventListener(Events.ON_DOUBLE_CLICK, evt -> {
				formView(true);
				getPromoManage(promoManage);
			});
			
			listitem.appendChild(lc(row + 1));
			listitem.appendChild(lc(promoManage.getName()));
			listitem.appendChild(lc(promoManage.getDateEffective()));
			listitem.appendChild(lc(promoManage.getTimeFrom() != null ? Utility.sdfHour.format(promoManage.getTimeFrom()) : EMPTY_STRING));
			listitem.appendChild(lc(promoManage.getTimeTo() != null ? Utility.sdfHour.format(promoManage.getTimeTo()) : EMPTY_STRING));
		});
	}
	
	private void getPromoManage(PromoManage promoManage) {
		if (promoManage == null)
			return;
		txtName.setValue(promoManage.getName());
		txtDescription.setValue(promoManage.getDescription());
		chkActive.setChecked(promoManage.isActive());
		chkByHour.setChecked(promoManage.isByHour());
		byHour();
		tmFrom.setValue(promoManage.getTimeFrom());
		tmTo.setValue(promoManage.getTimeTo());
		dtDateEffective.setValue(promoManage.getDateEffective());
		
		lines = promoManageLineRepository.findByPromoManage(promoManage.getPromoManageId());
		loadLines();
		
		disableForm(gridForm, !promoManage.isActive());
	}
	
	private void setPromoManage() {
		if (promoManage == null)
			promoManage = new PromoManage();
		promoManage.setName(txtName.getValue());
		promoManage.setDescription(txtDescription.getValue());
		promoManage.setActive(chkActive.isChecked());
		promoManage.setByHour(chkByHour.isChecked());
		promoManage.setTimeFrom(chkByHour.isChecked() ? tmFrom.getValue() : null);
		promoManage.setTimeTo(chkByHour.isChecked() ? tmTo.getValue() : null);
		promoManage.setDateEffective(dtDateEffective.getValue());
	}
	
	private boolean validasi() {
		if (Strings.isBlank(txtName.getValue())) {
			showMessage("Nama masih kosong");
			return false;
		}
		
		if (lines.size() == 0) {
			showMessage("Belum ada produk yang dimasukkan ke promo");
			return false;
		}
		
		return true;
	}
		
	private void clearObject() {
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
		clearForm(gridForm);
		loadData(txtSearchName.getValue());
		promoManage = new PromoManage();
		
		lines.clear();
		linesDelete.clear();
		cbPromo.setValue(null);
		loadLines();
	}
}
