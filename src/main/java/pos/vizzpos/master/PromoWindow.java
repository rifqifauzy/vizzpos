package pos.vizzpos.master;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.zkoss.lang.Strings;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.A;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Row;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import pos.vizzpos.CoreWindow;
import pos.vizzpos.bandbox.ProductBandbox;
import pos.vizzpos.entity.Product;
import pos.vizzpos.entity.Promo;
import pos.vizzpos.entity.PromoLine;
import pos.vizzpos.repository.PromoLineRepository;
import pos.vizzpos.repository.PromoRepository;
import pos.vizzpos.utils.PromoType;

@SuppressWarnings("serial")
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class PromoWindow extends CoreWindow {
	
	@Wire private Textbox txtName, txtDescription, txtSearchName;
	@Wire private Checkbox chkActive, chkMultiple, chkFree, chkProductPromo;
	@Wire private Radiogroup rdType;
	@Wire private Doublebox dbQty, dbAmount, dbQtyFree, dbQtyPay;
	@Wire private Bandbox bndProduct, bndProductFree;
	@Wire private Row rowQty, rowAmount, rowFree, rowPay;
	@Wire private Listbox listbox, listboxLine;
	@Wire private Combobox cbStatus;
	@Wire private Grid gridForm;
	@Wire private Vlayout vLayoutList, vLayoutLine;
	@Wire private Listheader lhType, lhPromo, lhFree;
	
	@WireVariable private PromoLineRepository promoLineRepository;
	@WireVariable private PromoRepository promoRepository;
	
	private Promo promo;
	private Product product, productFree;
	private ProductBandbox productBandbox, productBandboxFree;
	private boolean isEdit = false;
	
	private List<PromoLine> lines = new ArrayList<>();
	private List<PromoLine> linesDelete = new ArrayList<>();
	
	private ListModelList<Promo> listModel;
	private ListModelList<PromoLine> modelLine;

	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		productBandbox = (ProductBandbox) getController(bndProduct, ATTR_BANDBOX_PRODUCT);
		productBandbox.getListbox().addEventListener(Events.ON_SELECT, evt -> {
			product = productBandbox.getProduct(false);
			bndProduct.setText(product.getName());
			bndProduct.setOpen(false);
		});
		
		productBandboxFree = (ProductBandbox) getController(bndProductFree, ATTR_BANDBOX_PRODUCT);
		productBandboxFree.getListbox().addEventListener(Events.ON_SELECT, evt -> {
			productFree = productBandboxFree.getProduct(false);
			bndProductFree.setText(productFree.getName());
			bndProductFree.setOpen(false);
		});
		
		cbStatus.setSelectedIndex(0);
		promo = (Promo) getParameter(Promo.class);
		if (promo != null) {
			formView(true);
			getPromo(promo);
			loadDataByParam(promo.getPromoId());
			removeParameter(Promo.class);
		} else {
			formView(false);
			loadData(txtSearchName.getValue());
			rdType.setSelectedIndex(0);
			clearObject();
		}
	}
	
	@Listen("onOpen = #bndProduct")
	public void openBandbox() {
		productBandbox.getListbox().clearSelection();
	}
	
	@Listen("onCheck = #rdType")
	public void typePromo() {
		if (promo.getType() != null && lines.size() > 0) {
			showMessage("Tidak bisa ganti tipe promo karena sudah ada detail");
			rdType.setSelectedIndex(promo.getType().equalsIgnoreCase(PromoType.DISCOUNT.getValue()) ? 0 : 
				promo.getType().equalsIgnoreCase(PromoType.BUNDLE.getValue()) ? 1 : 
					promo.getType().equalsIgnoreCase(PromoType.BUY_FREE.getValue()) ? 2 : 3);
			return;
		}
		
		resetType();
	}

	@Listen("onClick = #chkActive")
	public void aktifForm() {
		disableForm(gridForm, !chkActive.isChecked());
	}
	
	@Listen("onClick = #btnSimpan")
	public void simpan() {
		if (vLayoutList.isVisible())
			return;
		
		if (validasi()) {
			setPromo();
			
			promoRepository.save(promo);
			
			if (linesDelete.size() > 0)
				promoLineRepository.delete(linesDelete);
			
			lines.forEach(l -> l.setPromo(promo));
			promoLineRepository.save(lines);
			
			formView(false);
			loadData(txtSearchName.getValue());
			clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
			promo = new Promo();
			clearForm(gridForm);
		}
	}
	
	@Listen("onClick = #btnView")
	public void view() {
		formView(!gridForm.isVisible());
	}
	
	@Listen("onClick = #btnBatal")
	public void batal() {
		formView(!gridForm.isVisible());
		clearObject();
	}

	@Listen("onClick = #btnRefresh")
	public void refresh() {
		if (gridForm.isVisible() && (promo == null || promo.getPromoId() == null))
			return;
		
		if (vLayoutList.isVisible()) {
			loadData(txtSearchName.getValue());
		} else { 
			if (promo == null)
				return;
			
			promoRepository.flush();
			promo = promoRepository.findOne(promo.getPromoId());
			getPromo(promo);
		}
	}
	
	@Listen("onClick = #btnHapus")
	public void hapus() {
		if (vLayoutList.isVisible() && listbox.getSelectedIndex() < 0) {
			showMessage(PILIH_DATA_DIHAPUS);
			return;
		}
		
		promo = listbox.getSelectedItem().getValue();
		confirmDelete(promo.getName(), evt -> {
			if (evt.getName().equals("onOK")) {
				try {
					promoRepository.delete(promo);
					clearObject();
				} catch (Exception e) {
					showMessage("Data sudah digunakan");
					return;
				}
			}
		});
	}
	
	@Listen("onChanging = #txtSearchName")
	public void searchName(InputEvent event) {
		loadData(event.getValue());
	}
	
	@Listen("onSelect = #cbStatus")
	public void searchStatus() {
		loadData(txtSearchName.getValue());
	}
	
	@Listen("onSelect = #listbox")
	public void pilih() {
		promo = listbox.getSelectedItem().getValue();

		btnFirst.setDisabled(getIndexSelected() == 0 ? true : getIndexSelected() == (listModel.size() - 1) ? false : false);
		btnPrev.setDisabled(getIndexSelected() == 0 ? true : getIndexSelected() == (listModel.size() - 1) ? false : false);
		btnNext.setDisabled(getIndexSelected() == (listModel.size() - 1) ? true : getIndexSelected() == 0 ? false : false);
		btnLast.setDisabled(getIndexSelected() == (listModel.size() - 1) ? true : getIndexSelected() == 0 ? false : false);
		
		getPromo(promo);
	}
	
	@Listen("onClick = #btnFirst")
	public void first() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(0);
		pilih();
	}
	
	@Listen("onClick = #btnPrev")
	public void prev() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(getIndexSelected() - 1);
		pilih();
	}
	
	@Listen("onClick = #btnNext")
	public void next() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(getIndexSelected() + 1);
		pilih();
	}
	
	@Listen("onClick = #btnLast")
	public void last() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(listModel.size() - 1);
		pilih();
	}
	
	@Listen("onSort = #listbox > listhead > listheader")
	public void sort() {
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
		listModel.clearSelection();
	}
	
	@Listen("onClick = #chkFree")
	public void free() {
		chkProductPromo.setChecked(chkFree.isChecked());
		chkProductPromo.setDisabled(chkFree.isChecked());
	}
	
	@Listen("onClick = #btnAddLine")
	public void simpanLine() {
		if (isEdit) {
			if (listboxLine.getSelectedIndex() < 0)
				return;
			PromoLine line = listboxLine.getSelectedItem().getValue();
			setPromoLine(product != null ? product : line.getProduct(), line);
			
			loadLines();
			clearLine();
		} else {
			if (validasiLine()) {
				setLine(product);
				clearLine();
			}
		}
		
		isEdit = false;
	}
	
	@Listen("onSelect = #listboxLine")
	public void selectLine() {
		isEdit = true;
		PromoLine line = listboxLine.getSelectedItem().getValue();
		getPromoLine(line);
	}
	
	private int getIndexSelected() {
		if (promo == null)
			return -1;
		
		return listModel.indexOf(promo);				
	}
	
	private void formView(boolean isForm) {
		gridForm.setVisible(isForm);
		vLayoutLine.setVisible(isForm);
		vLayoutList.setVisible(!isForm);
		btnBatal.setTooltiptext(isForm ? TEXT_BATAL : TEXT_BARU);
		btnBatal.setImage(isForm ? IMG_BATAL_32 : IMG_BARU_32);
		btnBatal.setLabel(isForm ? TEXT_BATAL : TEXT_BARU);
		
		btnView.setTooltiptext(isForm ? TEXT_LIST : TEXT_FORM);
		btnView.setLabel(isForm ? TEXT_LIST : TEXT_FORM);
		btnView.setImage(isForm ? IMG_LIST_32 : IMG_FORM_32);
	}
	
	private void loadDataByParam(Integer id) {
		List<Promo> list = new ArrayList<>();
		list.add(promoRepository.findByPromoId(id));
		listModel = new ListModelList<>(list);
		listbox.setModel(listModel);
		renderItem();
		listbox.setSelectedIndex(0);
	}
	
	private void loadData(String name) {
		List<Promo> list;
		
		boolean active = cbStatus.getSelectedIndex() == 1;
		if (cbStatus.getSelectedIndex() == 0)
			list = promoRepository.findByNameContainingAllIgnoreCaseOrderByNameAsc(name);
		else
			list = promoRepository.findByNameContainingAndActiveAllIgnoreCaseOrderByNameAsc(name, active);
		
		listModel = new ListModelList<>(list);
		listbox.setModel(listModel);
		renderItem();
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
	}
	
	private void renderItem() {
		listbox.setItemRenderer((Listitem listitem, Promo promo, int row) -> {
			listitem.getChildren().clear();
			listitem.setValue(promo);
			
			listitem.addEventListener(Events.ON_DOUBLE_CLICK, evt -> {
				formView(true);
				getPromo(promo);
			});
			
			listitem.appendChild(lc(row + 1));
			listitem.appendChild(lc(promo.getName()));
			listitem.appendChild(lc(promo.getDescription()));
			listitem.appendChild(lc(PromoType.getNameByCode(promo.getType())));
		});
	}
	
	private void getPromo(Promo promo) {
		if (promo == null)
			return;
		lines.clear();
		linesDelete.clear();
		
		txtName.setValue(promo.getName());
		txtDescription.setValue(promo.getDescription());
		chkActive.setChecked(promo.isActive());
		
		rdType.setSelectedIndex(promo.getType().equalsIgnoreCase(PromoType.DISCOUNT.getValue()) ? 0 : 
			promo.getType().equalsIgnoreCase(PromoType.BUNDLE.getValue()) ? 1 : 
				promo.getType().equalsIgnoreCase(PromoType.BUY_FREE.getValue()) ? 2 : 3);
		chkMultiple.setChecked(promo.isMultiple());
		resetType();
		
		lines = promoLineRepository.findByPromo(promo.getPromoId());
		renderLines();
		
		disableForm(gridForm, !promo.isActive());
	}
	
	private void setPromo() {
		if (promo == null)
			promo = new Promo();
		promo.setName(txtName.getValue());
		promo.setDescription(txtDescription.getValue());
		promo.setActive(chkActive.isChecked());
		promo.setMultiple(chkMultiple.isChecked());
		promo.setType(rdType.getSelectedIndex() == 0 ? PromoType.DISCOUNT.getValue() : 
			rdType.getSelectedIndex() == 1 ? PromoType.BUNDLE.getValue() : 
				rdType.getSelectedIndex() == 2 ? PromoType.BUY_FREE.getValue() : PromoType.BUY_PAY.getValue());
	}
	
	private void setLine(Product product) {
		if (promo == null)
			promo = new Promo();
		
		Optional<PromoLine> optLine = lines.stream()
				.filter(e -> e.getProduct().getProductId() == product.getProductId())
				.filter(e -> e.getPromo().getPromoId() == promo.getPromoId())
				.findFirst();
		
		PromoLine line;
		if (optLine.isPresent()) {
			showMessage(product.getName() + " sudah ada");
			return;
		} else {
			line = new PromoLine();
			line.setProduct(product);
			setPromoLine(product, line);
			lines.add(line);
		}
		
		loadLines();
	}

	private void setPromoLine(Product product, PromoLine line) {
		Optional<PromoLine> optLine = lines.stream()
				.filter(e -> e.getProduct().getProductId() != line.getProduct().getProductId())
				.filter(e -> e.getProduct().getProductId() == product.getProductId()).findFirst();
		if (optLine.isPresent()) {
			showMessage(product.getName() + " sudah ada");
			return;
		} 
		
		line.setProduct(product);
		line.setPromo(promo);
		line.setActive(true);
		line.setAmount(rowAmount.isVisible() ? dbAmount.getValue() : null);
		line.setFree(chkFree.isChecked());
		line.setProductPromo(chkProductPromo.isVisible() && chkProductPromo.isChecked());
		line.setQty(rowQty.isVisible() ? dbQty.getValue() : null);
		line.setQtyFree(rowFree.isVisible() ? dbQtyFree.getValue() : null);
		line.setProductFree(productFree != null && rowFree.isVisible() ? productFree : null);
		line.setQtyPay(rowPay.isVisible() ? dbQtyPay.getValue() : null);
	}
	
	private void getPromoLine(PromoLine line) {
		dbQty.setValue(line.getQty());
		chkFree.setChecked(line.isFree());
		bndProduct.setValue(line.getProduct().getName());
		dbAmount.setValue(line.getAmount());
		chkProductPromo.setChecked(line.isProductPromo());
		/*productBandbox.setProduct(line.getProduct());*/
	}
	
	private void renderLines() {
		listboxLine.setItemRenderer((Listitem listitem, PromoLine line, int row) -> {
			listitem.getChildren().clear();
			listitem.setValue(line);
			
			A aOK = new A(EMPTY_STRING, "/images/icon_control/ok_16.png");
			A aDelete = new A(EMPTY_STRING, "/images/icon_control/delete_16.png");
			aDelete.addEventListener(Events.ON_CLICK, evt -> {
				linesDelete.add(line);
				lines.remove(line);
				loadLines();
			});
			
			listitem.appendChild(lc(row + 1));
			listitem.appendChild(lc(line.getProduct().getName()));
			listitem.appendChild(lc(rdType.getSelectedIndex() == 0 ? line.getAmount() : line.getQty()));
			
			if (rdType.getSelectedIndex() <= 1) {
				listitem.appendChild(lc(line.isFree() ? aOK : EMPTY_STRING));
				listitem.appendChild(lc(line.isProductPromo() ? aOK : EMPTY_STRING));
			} else if (rdType.getSelectedIndex() == 2) {
				listitem.appendChild(lc(line.getProductFree().getName()));
				listitem.appendChild(lc(line.getQtyFree()));
			} else if (rdType.getSelectedIndex() == 3) {
				listitem.appendChild(lc(line.getQtyPay()));
				listitem.appendChild(lc(aDelete));
			}
			
			listitem.appendChild(lc(aDelete));
		});
		
		modelLine = new ListModelList<>(lines);
		listboxLine.setModel(modelLine);
	}
	
	private void loadLines() {
		renderLines();
	}
	
	private boolean validasi() {
		if (Strings.isBlank(txtName.getValue())) {
			showMessage("Nama masih kosong");
			return false;
		}
		
		if (rdType.getSelectedIndex() < 0) {
			showMessage("Tipe Promo belum dipilih");
			return false;
		}
		
		if (lines.size() == 0) {
			showMessage("Belum ada produk yang dimasukkan ke promo");
			return false;
		}
		
		return true;
	}
	
	private boolean validasiLine() {
		if (rdType.getSelectedIndex() == 0 && dbAmount.getValue() == null) {
			showMessage("Disc masih kosong");
			return false;
			
		}
		
		if (rdType.getSelectedIndex() != 0 && dbQty.getValue() == null) {
			showMessage("Qty masih kosong");
			return false;
		}
		
		if (chkFree.isChecked())
			chkProductPromo.setChecked(true);
		
		return true;
	}
	
	private void clearObject() {
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
		clearForm(gridForm);
		loadData(txtSearchName.getValue());
		promo = new Promo();
		rdType.setSelectedIndex(0);
		resetType();
		
		lines.clear();
		linesDelete.clear();
	}
	
	private void clearLine() {
		bndProduct.setValue(null);
		dbAmount.setValue(null);
		dbQty.setValue(null);
		chkFree.setChecked(false);
		chkProductPromo.setChecked(chkProductPromo.isDisabled());
		bndProductFree.setValue(null);
		dbQtyFree.setValue(null);
		dbQtyPay.setValue(null);
	}
	
	private void resetType() {
		lines.clear();
		loadLines();
		
		rowAmount.setVisible(rdType.getSelectedIndex() == 0);
		rowQty.setVisible(rdType.getSelectedIndex() != 0);
		rowFree.setVisible(rdType.getSelectedIndex() == 2);
		rowPay.setVisible(rdType.getSelectedIndex() == 3);
		
		lhType.setLabel(rdType.getSelectedIndex() == 0 ? "Disc (%)" : "Qty");
		chkFree.setVisible(rdType.getSelectedIndex() == 1);
		chkProductPromo.setChecked(rdType.getSelectedIndex() == 0);
		chkProductPromo.setDisabled(rdType.getSelectedIndex() == 0);
		chkProductPromo.setVisible(rdType.getSelectedIndex() <= 1);
		
		if (rdType.getSelectedIndex() == 2) {
			lhFree.setWidth("");
			lhFree.setLabel("Product Free");
			lhFree.setAlign("left");
			lhPromo.setWidth("140px");
			lhPromo.setLabel("Qty Free");
			lhPromo.setAlign("right");
		} else if (rdType.getSelectedIndex() == 3) {
			lhFree.setWidth("140px");
			lhFree.setLabel("Qty Pay");
			lhFree.setAlign("right");
			lhPromo.setVisible(false);
		} else {
			lhFree.setWidth("100px");
			lhFree.setLabel("Free");
			lhFree.setAlign("center");
			lhPromo.setWidth("140px");
			lhPromo.setLabel("Produk Promo");
			lhPromo.setAlign("center");
		}
	}
}
