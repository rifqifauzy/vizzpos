package pos.vizzpos.master;

import java.util.ArrayList;
import java.util.List;

import org.springframework.orm.jpa.JpaSystemException;
import org.zkoss.lang.Strings;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import pos.vizzpos.CoreWindow;
import pos.vizzpos.entity.Unit;
import pos.vizzpos.repository.UnitRepository;

@SuppressWarnings("serial")
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class UnitWindow extends CoreWindow {
	
	@Wire private Textbox txtName, txtDescription, txtSearchName, txtSearchDescription;
	@Wire private Checkbox chkActive;
	@Wire private Listbox listbox;
	@Wire private Combobox cbStatus;
	@Wire private Grid gridForm;
	@Wire private Vlayout vLayoutList;
	
	@WireVariable private UnitRepository unitRepository;
	
	private Unit unit;
	
	private ListModelList<Unit> listModel;

	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		cbStatus.setSelectedIndex(0);
		unit = (Unit) getParameter(Unit.class);
		if (unit != null) {
			formView(true);
			getUnit(unit);
			loadDataByParam(unit.getUnitId());
			removeParameter(Unit.class);
		} else {
			formView(false);
			loadData(txtSearchName.getValue(), txtSearchDescription.getValue());
		}
	}
	
	@Listen("onClick = #chkActive")
	public void aktifForm() {
		disableForm(gridForm, !chkActive.isChecked());
	}
	
	@Listen("onClick = #btnSimpan")
	public void simpan() {
		if (vLayoutList.isVisible())
			return;
		
		if (validasi()) {
			setUnit();
			
			try {
				unitRepository.save(unit);
			} catch (Exception e) {
				if (e instanceof JpaSystemException) {
					showMessage(txtName.getValue() + " sudah ada");
					return;
				}
			}
			
			formView(false);
			loadData(txtSearchName.getValue(), txtSearchDescription.getValue());
			clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
			unit = new Unit();
			clearForm(gridForm);
		}
	}
	
	@Listen("onClick = #btnView")
	public void view() {
		formView(!gridForm.isVisible());
	}
	
	@Listen("onClick = #btnBatal")
	public void batal() {
		formView(!gridForm.isVisible());
		clearObject();
	}

	@Listen("onClick = #btnRefresh")
	public void refresh() {
		if (gridForm.isVisible() && (unit == null || unit.getUnitId() == null))
			return;
		
		if (vLayoutList.isVisible()) {
			loadData(txtSearchName.getValue(), txtSearchDescription.getValue());
		} else { 
			if (unit == null)
				return;
			
			unitRepository.flush();
			unit = unitRepository.findOne(unit.getUnitId());
			getUnit(unit);
		}
	}
	
	@Listen("onClick = #btnHapus")
	public void hapus() {
		if (vLayoutList.isVisible() && listbox.getSelectedIndex() < 0) {
			showMessage(PILIH_DATA_DIHAPUS);
			return;
		}
		
		unit = listbox.getSelectedItem().getValue();
		confirmDelete(unit.getName(), evt -> {
			if (evt.getName().equals("onOK")) {
				try {
					unitRepository.delete(unit);
					clearObject();
				} catch (Exception e) {
					showMessage("Data sudah digunakan");
					return;
				}
			}
		});
	}
	
	@Listen("onChanging = #txtSearchName")
	public void searchName(InputEvent event) {
		loadData(event.getValue(), txtSearchDescription.getValue());
	}
	
	@Listen("onChanging = #txtSearchDescription")
	public void searchDescription(InputEvent event) {
		loadData(txtSearchName.getValue(), event.getValue());
	}
	
	@Listen("onSelect = #cbStatus")
	public void searchStatus() {
		loadData(txtSearchName.getValue(), txtSearchDescription.getValue());
	}
	
	@Listen("onSelect = #listbox")
	public void pilih() {
		unit = listbox.getSelectedItem().getValue();

		btnFirst.setDisabled(getIndexSelected() == 0 ? true : getIndexSelected() == (listModel.size() - 1) ? false : false);
		btnPrev.setDisabled(getIndexSelected() == 0 ? true : getIndexSelected() == (listModel.size() - 1) ? false : false);
		btnNext.setDisabled(getIndexSelected() == (listModel.size() - 1) ? true : getIndexSelected() == 0 ? false : false);
		btnLast.setDisabled(getIndexSelected() == (listModel.size() - 1) ? true : getIndexSelected() == 0 ? false : false);
		
		getUnit(unit);
	}
	
	@Listen("onClick = #btnFirst")
	public void first() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(0);
		pilih();
	}
	
	@Listen("onClick = #btnPrev")
	public void prev() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(getIndexSelected() - 1);
		pilih();
	}
	
	@Listen("onClick = #btnNext")
	public void next() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(getIndexSelected() + 1);
		pilih();
	}
	
	@Listen("onClick = #btnLast")
	public void last() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(listModel.size() - 1);
		pilih();
	}
	
	@Listen("onSort = #listbox > listhead > listheader")
	public void sort() {
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
		listModel.clearSelection();
	}
	
	private int getIndexSelected() {
		if (unit == null)
			return -1;
		
		return listModel.indexOf(unit);				
	}
	
	private void formView(boolean isForm) {
		gridForm.setVisible(isForm);
		vLayoutList.setVisible(!isForm);
		btnBatal.setTooltiptext(isForm ? TEXT_BATAL : TEXT_BARU);
		btnBatal.setImage(isForm ? IMG_BATAL_32 : IMG_BARU_32);
		btnBatal.setLabel(isForm ? TEXT_BATAL : TEXT_BARU);
		
		btnView.setTooltiptext(isForm ? TEXT_LIST : TEXT_FORM);
		btnView.setLabel(isForm ? TEXT_LIST : TEXT_FORM);
		btnView.setImage(isForm ? IMG_LIST_32 : IMG_FORM_32);
	}
	
	private void loadDataByParam(Integer id) {
		List<Unit> list = new ArrayList<>();
		list.add(unitRepository.findByUnitId(id));
		listModel = new ListModelList<>(list);
		listbox.setModel(listModel);
		renderItem();
		listbox.setSelectedIndex(0);
	}
	
	private void loadData(String name, String description) {
		List<Unit> list;
		
		boolean active = cbStatus.getSelectedIndex() == 1;
		if (cbStatus.getSelectedIndex() == 0)
			list = unitRepository.findByNameContainingAndDescriptionContainingAllIgnoreCaseOrderByNameAsc(name, description);
		else
			list = unitRepository.findByNameContainingAndDescriptionContainingAndActiveAllIgnoreCaseOrderByNameAsc(name, description, active);
		
		listModel = new ListModelList<>(list);
		listbox.setModel(listModel);
		renderItem();
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
	}
	
	private void renderItem() {
		listbox.setItemRenderer((Listitem listitem, Unit unit, int row) -> {
			listitem.getChildren().clear();
			listitem.setValue(unit);
			
			listitem.addEventListener(Events.ON_DOUBLE_CLICK, evt -> {
				formView(true);
				getUnit(unit);
			});
			
			listitem.appendChild(lc(row + 1));
			listitem.appendChild(lc(unit.getName()));
			listitem.appendChild(lc(unit.getDescription()));
		});
	}
	
	private void getUnit(Unit unit) {
		if (unit == null)
			return;
		txtName.setValue(unit.getName());
		txtDescription.setValue(unit.getDescription());
		chkActive.setChecked(unit.isActive());
		disableForm(gridForm, !unit.isActive());
	}
	
	private void setUnit() {
		if (unit == null)
			unit = new Unit();
		unit.setName(txtName.getValue());
		unit.setDescription(txtDescription.getValue());
		unit.setActive(chkActive.isChecked());
	}
	
	private boolean validasi() {
		if (Strings.isBlank(txtName.getValue())) {
			showMessage("Nama masih kosong");
			return false;
		}
		
		return true;
	}
	
	private void clearObject() {
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
		clearForm(gridForm);
		loadData(txtSearchName.getValue(), txtSearchDescription.getValue());
		unit = new Unit();
	}
	
}
