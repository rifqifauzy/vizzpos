package pos.vizzpos.master;

import java.util.ArrayList;
import java.util.List;

import org.springframework.orm.jpa.JpaSystemException;
import org.zkoss.lang.Strings;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import pos.vizzpos.CoreWindow;
import pos.vizzpos.entity.Product;
import pos.vizzpos.repository.ProductRepository;

@SuppressWarnings("serial")
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ProductWindow extends CoreWindow {
	
	@Wire private Textbox txtName, txtCode, txtSearchName, txtSearchCode;
	@Wire private Checkbox chkActive;
	@Wire private Listbox listbox;
	@Wire private Combobox cbStatus, cbCategory, cbUnit;
	@Wire private Doublebox dbPrice;
	@Wire private Intbox intGuarantee;	
	@Wire private Grid gridForm;
	@Wire private Vlayout vLayoutList;
	
	@WireVariable private ProductRepository productRepository;
	
	private Product product;
	
	private ListModelList<Product> listModel;

	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		cbStatus.setSelectedIndex(0);
		product = (Product) getParameter(Product.class);
		if (product != null) {
			formView(true);
			getProduct(product);
			loadDataByParam(product.getProductId());
			removeParameter(Product.class);
		} else {
			formView(false);
			loadData(txtSearchName.getValue(), txtSearchCode.getValue());
		}
	}
	
	@Listen("onClick = #chkActive")
	public void aktifForm() {
		disableForm(gridForm, !chkActive.isChecked());
	}
	
	@Listen("onClick = #btnSimpan")
	public void simpan() {
		if (vLayoutList.isVisible())
			return;
		
		if (validasi()) {
			setProduct();
			
			try {
				productRepository.save(product);
			} catch (Exception e) {
				if (e instanceof JpaSystemException) {
					showMessage(txtCode.getValue() + " sudah ada");
					return;
				}
			}
			
			formView(false);
			loadData(txtSearchName.getValue(), txtSearchCode.getValue());
			clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
			product = new Product();
			clearForm(gridForm);
		}
	}
	
	@Listen("onClick = #btnView")
	public void view() {
		formView(!gridForm.isVisible());
	}
	
	@Listen("onClick = #btnBatal")
	public void batal() {
		formView(!gridForm.isVisible());
		clearObject();
	}

	@Listen("onClick = #btnRefresh")
	public void refresh() {
		if (gridForm.isVisible() && (product == null || product.getProductId() == null))
			return;
		
		if (vLayoutList.isVisible()) {
			loadData(txtSearchName.getValue(), txtSearchCode.getValue());
		} else { 
			if (product == null)
				return;
			
			productRepository.flush();
			product = productRepository.findOne(product.getProductId());
			getProduct(product);
		}
	}
	
	@Listen("onClick = #btnHapus")
	public void hapus() {
		if (vLayoutList.isVisible() && listbox.getSelectedIndex() < 0) {
			showMessage(PILIH_DATA_DIHAPUS);
			return;
		}
		
		product = listbox.getSelectedItem().getValue();
		confirmDelete(product.getName(), evt -> {
			if (evt.getName().equals("onOK")) {
				try {
					productRepository.delete(product);
					clearObject();
				} catch (Exception e) {
					showMessage("Data sudah digunakan");
					return;
				}
			}
		});
	}
	
	@Listen("onChanging = #txtSearchName")
	public void searchName(InputEvent event) {
		loadData(event.getValue(), txtSearchCode.getValue());
	}
	
	@Listen("onChanging = #txtSearchCode")
	public void searchCode(InputEvent event) {
		loadData(txtSearchName.getValue(), event.getValue());
	}
	
	@Listen("onSelect = #cbStatus")
	public void searchStatus() {
		loadData(txtSearchName.getValue(), txtSearchCode.getValue());
	}
	
	@Listen("onSelect = #listbox")
	public void pilih() {
		product = listbox.getSelectedItem().getValue();

		btnFirst.setDisabled(getIndexSelected() == 0 ? true : getIndexSelected() == (listModel.size() - 1) ? false : false);
		btnPrev.setDisabled(getIndexSelected() == 0 ? true : getIndexSelected() == (listModel.size() - 1) ? false : false);
		btnNext.setDisabled(getIndexSelected() == (listModel.size() - 1) ? true : getIndexSelected() == 0 ? false : false);
		btnLast.setDisabled(getIndexSelected() == (listModel.size() - 1) ? true : getIndexSelected() == 0 ? false : false);
		
		getProduct(product);
	}
	
	@Listen("onClick = #btnFirst")
	public void first() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(0);
		pilih();
	}
	
	@Listen("onClick = #btnPrev")
	public void prev() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(getIndexSelected() - 1);
		pilih();
	}
	
	@Listen("onClick = #btnNext")
	public void next() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(getIndexSelected() + 1);
		pilih();
	}
	
	@Listen("onClick = #btnLast")
	public void last() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(listModel.size() - 1);
		pilih();
	}
	
	@Listen("onSort = #listbox > listhead > listheader")
	public void sort() {
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
		listModel.clearSelection();
	}
	
	private int getIndexSelected() {
		if (product == null)
			return -1;
		
		return listModel.indexOf(product);				
	}
	
	private void formView(boolean isForm) {
		gridForm.setVisible(isForm);
		vLayoutList.setVisible(!isForm);
		btnBatal.setTooltiptext(isForm ? TEXT_BATAL : TEXT_BARU);
		btnBatal.setImage(isForm ? IMG_BATAL_32 : IMG_BARU_32);
		btnBatal.setLabel(isForm ? TEXT_BATAL : TEXT_BARU);
		
		btnView.setTooltiptext(isForm ? TEXT_LIST : TEXT_FORM);
		btnView.setLabel(isForm ? TEXT_LIST : TEXT_FORM);
		btnView.setImage(isForm ? IMG_LIST_32 : IMG_FORM_32);
	}
	
	private void loadDataByParam(Integer id) {
		List<Product> list = new ArrayList<>();
		list.add(productRepository.findByProductId(id));
		listModel = new ListModelList<>(list);
		listbox.setModel(listModel);
		renderItem();
		listbox.setSelectedIndex(0);
	}
	
	private void loadData(String name, String code) {
		List<Product> list;
		
		boolean active = cbStatus.getSelectedIndex() == 1;
		if (cbStatus.getSelectedIndex() == 0)
			list = productRepository.findByNameContainingAndCodeContainingAllIgnoreCaseOrderByName(name, code);
		else
			list = productRepository.findByActiveAndNameContainingAndCodeContainingAllIgnoreCaseOrderByName(active, name, code);
		
		listModel = new ListModelList<>(list);
		listbox.setModel(listModel);
		renderItem();
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
	}
	
	private void renderItem() {
		listbox.setItemRenderer((Listitem listitem, Product product, int row) -> {
			listitem.getChildren().clear();
			listitem.setValue(product);
			
			listitem.addEventListener(Events.ON_DOUBLE_CLICK, evt -> {
				formView(true);
				getProduct(product);
			});
			
			listitem.appendChild(lc(row + 1));
			listitem.appendChild(lc(product.getCode()));
			listitem.appendChild(lc(product.getName()));
			listitem.appendChild(lc(product.getPrice()));
			listitem.appendChild(lc(product.getProductCategory() != null ? 
					product.getProductCategory().getName() : EMPTY_STRING));
		});
	}
	
	private void getProduct(Product product) {
		if (product == null)
			return;
		txtName.setValue(product.getName());
		txtCode.setValue(product.getCode());
		cbCategory.setValue(product.getProductCategory().getName());
		cbUnit.setValue(product.getUnit().getName());
		chkActive.setChecked(product.isActive());
		dbPrice.setValue(product.getPrice());
		intGuarantee.setValue(product.getGuarantee());
		disableForm(gridForm, !product.isActive());
	}
	
	private void setProduct() {
		if (product == null)
			product = new Product();
		product.setName(txtName.getValue());
		product.setCode(txtCode.getValue());
		product.setActive(chkActive.isChecked());
		product.setProductCategory(cbCategory.getSelectedItem().getValue());
		product.setUnit(cbUnit.getSelectedItem().getValue());
		product.setPrice(dbPrice.getValue());
		product.setGuarantee(intGuarantee.getValue() != null ? intGuarantee.getValue() : 0);
	}
	
	private boolean validasi() {
		if (Strings.isBlank(txtCode.getValue())) {
			showMessage("Kode masih kosong");
			return false;
		}
		
		if (cbCategory.getSelectedIndex() < 0) {
			showMessage("Kategori belum dipilih");
			return false;
		}
		
		if (Strings.isBlank(txtName.getValue())) {
			showMessage("Nama masih kosong");
			return false;
		}
		
		if (cbUnit.getSelectedIndex() < 0) {
			showMessage("Satuan belum dipilih");
			return false;
		}
		
		if (dbPrice.getValue() == null || dbPrice.getValue() <= 0d) {
			showMessage("Harga masih kosong");
			return false;
		}
		
		return true;
	}
	
	private void clearObject() {
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
		clearForm(gridForm);
		loadData(txtSearchName.getValue(), txtSearchCode.getValue());
		product = new Product();
	}
	
}
