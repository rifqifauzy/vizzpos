package pos.vizzpos.master;

import java.util.ArrayList;
import java.util.List;

import org.springframework.orm.jpa.JpaSystemException;
import org.zkoss.lang.Strings;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import pos.vizzpos.CoreWindow;
import pos.vizzpos.entity.ProductCategory;
import pos.vizzpos.repository.ProductCategoryRepository;

@SuppressWarnings("serial")
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ProductCategoryWindow extends CoreWindow {
	
	@Wire private Textbox txtName, txtDescription, txtSearchName, txtSearchDescription;
	@Wire private Checkbox chkActive;
	@Wire private Listbox listbox;
	@Wire private Combobox cbStatus;
	@Wire private Grid gridForm;
	@Wire private Vlayout vLayoutList;
	
	@WireVariable private ProductCategoryRepository productCategoryRepository;
	
	private ProductCategory productCategory;
	
	private ListModelList<ProductCategory> listModel;

	@Override
	public void doAfterCompose(Window comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		
		cbStatus.setSelectedIndex(0);
		productCategory = (ProductCategory) getParameter(ProductCategory.class);
		if (productCategory != null) {
			formView(true);
			getProductCategory(productCategory);
			loadDataByParam(productCategory.getProductCategoryId());
			removeParameter(ProductCategory.class);
		} else {
			formView(false);
			loadData(txtSearchName.getValue(), txtSearchDescription.getValue());
		}
	}
	
	@Listen("onClick = #chkActive")
	public void aktifForm() {
		disableForm(gridForm, !chkActive.isChecked());
	}
	
	@Listen("onClick = #btnSimpan")
	public void simpan() {
		if (vLayoutList.isVisible())
			return;
		
		if (validasi()) {
			setProductCategory();
			
			try {
				productCategoryRepository.save(productCategory);
			} catch (Exception e) {
				if (e instanceof JpaSystemException) {
					showMessage(txtName.getValue() + " sudah ada");
					return;
				}
			}
			
			formView(false);
			loadData(txtSearchName.getValue(), txtSearchDescription.getValue());
			clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
			productCategory = new ProductCategory();
			clearForm(gridForm);
		}
	}
	
	@Listen("onClick = #btnView")
	public void view() {
		formView(!gridForm.isVisible());
	}
	
	@Listen("onClick = #btnBatal")
	public void batal() {
		formView(!gridForm.isVisible());
		clearObject();
	}

	@Listen("onClick = #btnRefresh")
	public void refresh() {
		if (gridForm.isVisible() && (productCategory == null || productCategory.getProductCategoryId() == null))
			return;
		
		if (vLayoutList.isVisible()) {
			loadData(txtSearchName.getValue(), txtSearchDescription.getValue());
		} else { 
			if (productCategory == null)
				return;
			
			productCategoryRepository.flush();
			productCategory = productCategoryRepository.findOne(productCategory.getProductCategoryId());
			getProductCategory(productCategory);
		}
	}
	
	@Listen("onClick = #btnHapus")
	public void hapus() {
		if (vLayoutList.isVisible() && listbox.getSelectedIndex() < 0) {
			showMessage(PILIH_DATA_DIHAPUS);
			return;
		}
		
		productCategory = listbox.getSelectedItem().getValue();
		confirmDelete(productCategory.getName(), evt -> {
			if (evt.getName().equals("onOK")) {
				try {
					productCategoryRepository.delete(productCategory);
					clearObject();
				} catch (Exception e) {
					showMessage("Data sudah digunakan");
					return;
				}
			}
		});
	}
	
	@Listen("onChanging = #txtSearchName")
	public void searchName(InputEvent event) {
		loadData(event.getValue(), txtSearchDescription.getValue());
	}
	
	@Listen("onChanging = #txtSearchDescription")
	public void searchDescription(InputEvent event) {
		loadData(txtSearchName.getValue(), event.getValue());
	}
	
	@Listen("onSelect = #cbStatus")
	public void searchStatus() {
		loadData(txtSearchName.getValue(), txtSearchDescription.getValue());
	}
	
	@Listen("onSelect = #listbox")
	public void pilih() {
		productCategory = listbox.getSelectedItem().getValue();

		btnFirst.setDisabled(getIndexSelected() == 0 ? true : getIndexSelected() == (listModel.size() - 1) ? false : false);
		btnPrev.setDisabled(getIndexSelected() == 0 ? true : getIndexSelected() == (listModel.size() - 1) ? false : false);
		btnNext.setDisabled(getIndexSelected() == (listModel.size() - 1) ? true : getIndexSelected() == 0 ? false : false);
		btnLast.setDisabled(getIndexSelected() == (listModel.size() - 1) ? true : getIndexSelected() == 0 ? false : false);
		
		getProductCategory(productCategory);
	}
	
	@Listen("onClick = #btnFirst")
	public void first() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(0);
		pilih();
	}
	
	@Listen("onClick = #btnPrev")
	public void prev() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(getIndexSelected() - 1);
		pilih();
	}
	
	@Listen("onClick = #btnNext")
	public void next() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(getIndexSelected() + 1);
		pilih();
	}
	
	@Listen("onClick = #btnLast")
	public void last() {
		if (listbox.getSelectedIndex() < 0)
			return;
		listbox.setSelectedIndex(listModel.size() - 1);
		pilih();
	}
	
	@Listen("onSort = #listbox > listhead > listheader")
	public void sort() {
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
		listModel.clearSelection();
	}
	
	private int getIndexSelected() {
		if (productCategory == null)
			return -1;
		
		return listModel.indexOf(productCategory);				
	}
	
	private void formView(boolean isForm) {
		gridForm.setVisible(isForm);
		vLayoutList.setVisible(!isForm);
		btnBatal.setTooltiptext(isForm ? TEXT_BATAL : TEXT_BARU);
		btnBatal.setImage(isForm ? IMG_BATAL_32 : IMG_BARU_32);
		btnBatal.setLabel(isForm ? TEXT_BATAL : TEXT_BARU);
		
		btnView.setTooltiptext(isForm ? TEXT_LIST : TEXT_FORM);
		btnView.setLabel(isForm ? TEXT_LIST : TEXT_FORM);
		btnView.setImage(isForm ? IMG_LIST_32 : IMG_FORM_32);
	}
	
	private void loadDataByParam(Integer id) {
		List<ProductCategory> list = new ArrayList<>();
		list.add(productCategoryRepository.findByProductCategoryId(id));
		listModel = new ListModelList<>(list);
		listbox.setModel(listModel);
		renderItem();
		listbox.setSelectedIndex(0);
	}
	
	private void loadData(String name, String description) {
		List<ProductCategory> list;
		
		boolean active = cbStatus.getSelectedIndex() == 1;
		if (cbStatus.getSelectedIndex() == 0)
			list = productCategoryRepository
					.findByNameContainingAndDescriptionContainingAllIgnoreCaseOrderByNameAsc(name, description);
		else
			list = productCategoryRepository
					.findByNameContainingAndDescriptionContainingAndActiveAllIgnoreCaseOrderByNameAsc(name, description,
							active);

		listModel = new ListModelList<>(list);
		listbox.setModel(listModel);
		renderItem();
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
	}
	
	private void renderItem() {
		listbox.setItemRenderer((Listitem listitem, ProductCategory productCategory, int row) -> {
			listitem.getChildren().clear();
			listitem.setValue(productCategory);
			
			listitem.addEventListener(Events.ON_DOUBLE_CLICK, evt -> {
				formView(true);
				getProductCategory(productCategory);
			});
			
			listitem.appendChild(lc(row + 1));
			listitem.appendChild(lc(productCategory.getName()));
			listitem.appendChild(lc(productCategory.getDescription()));
		});
	}
	
	private void getProductCategory(ProductCategory productCategory) {
		if (productCategory == null)
			return;
		txtName.setValue(productCategory.getName());
		txtDescription.setValue(productCategory.getDescription());
		chkActive.setChecked(productCategory.isActive());
		disableForm(gridForm, !productCategory.isActive());
	}
	
	private void setProductCategory() {
		if (productCategory == null)
			productCategory = new ProductCategory();
		productCategory.setName(txtName.getValue());
		productCategory.setDescription(txtDescription.getValue());
		productCategory.setActive(chkActive.isChecked());
	}
	
	private boolean validasi() {
		if (Strings.isBlank(txtName.getValue())) {
			showMessage("Nama masih kosong");
			return false;
		}
		
		return true;
	}
	
	private void clearObject() {
		clearSelection(listbox, btnFirst, btnNext, btnPrev, btnLast);
		clearForm(gridForm);
		loadData(txtSearchName.getValue(), txtSearchDescription.getValue());
		productCategory = new ProductCategory();
	}
	
}
